For a complete overview visit the [wiki-changeLog site](https://github.com/FelixTaylor/GameMasterApp/wiki/ChangeLog)

## v0.14.0-alpha - 23-FEB-2018
**FUNCTION**
* BACKPACK: In the DetailActivity you can now view, add and remove items form you figures backpack!
* MoveToGroup Method: Now you can select one or more figures and move them from one group to another.
* New info in the SettingsActivity shows how many groups and figures have been created since the installation date of this version.

**BUGFIX**
* The app crashed when returning to the ListActivity from the DetailActivity if changes were made to a figure. Now it works like a charm.
* Fixed a bug where the app froze if the user tried to added more figures to a group then possible.
* Fixed a bug where the 'figureWasNotSaved' dialog in the DetailActivity was triggered even if the figure was just saved.
* Fixed a bug where the lore was not saved correctly and the app didn't warn the user.
* Fixed a bug where the figure was not calculated correctly if the level was locked.

**IMPROVEMENT**
* If a figure has only one attack the 'Base Attack Bonus' modifier value in the DetailActivity is now hidden.!
* I Improved the multi-figure-selection functionality in the ListActivity. You can now select and deselect multiple figures correctly.
* I reordered the items in the figure options dialog in the ListActivity.
* I reorganized the xml files, combined a few and deleted those not more in use.

**DESIGN**
* Moved the gold display in the DetailActivity from the properties tab to the backpack tab.
