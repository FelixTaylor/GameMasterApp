# GameMaster*App*

### GameMaster
GameMaster is an application which helps to generate and store playable Dungeon &amp; Dragons (ver. 3.5) figures. Those figures are supposed to be primarily used as NPC'S (Non-player character) but the values could by transferred to a 'Character Sheet'. This is a non-profit project I started - while attending school - to learn java and android programming. Keep in mind that his application does not obviate the need of the actual 'D&amp;D Rule-book' and 'Players-Handbook' to play the game! Help or constructive criticism are highly appreciated. 

## Installation
Go to the [releases page](https://github.com/FelixTaylor/GameMasterApp/releases) and download the newest android apk (GameMaster-X.X.X.apk) file to your phone. Next

1.	On your phone go to settings, scroll down to `Security` and tap it.
2.	Enable `unknown sources` **IMPORTANT: Make sure to disable it after the installation!**
3.	Navigate to the apk file.
4.	Install it.
5.  Disable `unknown sources`.

Now you are good to go!

## Prerequirements
- Android phone
- Minimum API: 19 (KitKat)

## Dependencies
- [ACRA/acra](https://github.com/ACRA/acra) (Error report system)
- [Figure-1.0.0.jar](https://github.com/FelixTaylor/Figure)

## Versioning
- I use [semver](https://semver.org/) as versioning scheme.
- I will reset the MINOR and PATCH version when increasing MAJOR version, for example: 0.9.0 -> 0.9.2 -> 1.0.0
- And finally I'll add '-alpha' to the version to indicate the apps alpha stage.

## Preview
#### 0.12.0 [ChangeLog](https://github.com/FelixTaylor/GameMasterApp/blob/master/CHANGELOG.md)
<table>
  <tr>
    <td>GameMaster: GroupActivity</td>
    <td>GameMaster: ListActivity</td>
    <td>GameMaster: DetailActivity</td>
  </tr>
  <tr>
    <td><img src="https://github.com/FelixTaylor/GameMasterApp/blob/master/img/0.10.0_group.png" width="250"></<td>
    <td><img src="https://github.com/FelixTaylor/GameMasterApp/blob/master/img/0.10.0_list.png" width="250"></td>
    <td><img src="https://github.com/FelixTaylor/GameMasterApp/blob/master/img/0.10.0_detail.png" width="250"></td>
  </tr>
</table>
