package felixtaylor.gamemaster.activities;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import felixtaylor.gamemaster.actions.DeleteFigureActivity;
import felixtaylor.gamemaster.actions.MoveFiguresToGroupActivity;
import felixtaylor.gamemaster.R;
import felixtaylor.gamemaster.StaticValues;
import felixtaylor.gamemaster.database.DBConstants;
import felixtaylor.gamemaster.database.DBHandler;
import felixtaylor.gamemaster.equipment.Item;
import felixtaylor.gamemaster.figure.Skill;
import felixtaylor.gamemaster.system.Group;
import felixtaylor.gamemaster.figure.Figure;
import felixtaylor.gamemaster.system.ShakeEventListener;
import felixtaylor.gamemaster.system.User;
import org.acra.*;
import org.acra.annotation.*;

@AcraCore(buildConfigClass = BuildConfig.class)
public class DetailActivity extends AppCompatActivity implements
        TabHost.OnTabChangeListener        ,
        View.OnClickListener               ,
        SeekBar.OnSeekBarChangeListener    ,
        AdapterView.OnItemSelectedListener ,
        CompoundButton.OnCheckedChangeListener {


    private static final String LOGTAG = DetailActivity.class.getSimpleName();
    private ShakeEventListener mSensorListener;
    private SensorManager      mSensorManager;

    private Resources resources;
    private Intent    intent;
    private DBHandler handler;
    private User      user;
    private Group     group;
    private Figure    figure;

    private Item      selectedItem;
    private boolean   figureWasSaved;

    // DIALOGS
    private Dialog dialogFigureName;
    private Dialog dialogHelp;
    private Dialog dialogUnsavedChanges;
    private Dialog dialogItem;

    private TabHost        tabHost;
    private StringBuffer   buffer;
    private LayoutInflater inflater;

    private SeekBar  sb_abilities[];
    private TextView tv_abilityValues[];
    private TextView tv_abilityMods[];
    private TextView usedAbilityPoints;
    private EditText et_name;
    private EditText et_newFigureName;
    private EditText et_lore;

    private RadioButton rb_female;
    private RadioButton rb_male;

    private Spinner sp_alignment;
    private Spinner sp_attitude;
    private Spinner sp_level;
    private Spinner sp_race;
    private Spinner sp_class;

    // 0. name, 1. race, 2. class, 3. level
    private ImageView iv_lockImages[];
    private boolean   locks[];


    private int skillPointsLeft;
    private TextView skillpoints;
    private TextView skillValue[];
    private SeekBar skillRank[];


    private class ItemClickListener implements View.OnClickListener {
        @Override public void onClick(View v) {
            showDialogAddItem(v);
            //return false;
        }
    }
    private class ItemListener implements View.OnClickListener {
        @Override public void onClick(View v) {
            switch (v.getId()) {

                case R.id.dialog_delete: {
                    deleteItem();
                    break;
                }

                case R.id.dialog_positive: {
                    EditText et_name     = dialogItem.findViewById(R.id.item_name);
                    EditText et_quantity = dialogItem.findViewById(R.id.item_quantity);
                    EditText et_value    = dialogItem.findViewById(R.id.item_value);
                    EditText et_weight   = dialogItem.findViewById(R.id.item_weight);

                    String name       = et_name.getText().toString().trim();
                    String text       = et_quantity.getText().toString().trim();
                    String valueText  = et_value.getText().toString().trim();
                    String weightText = et_weight.getText().toString().trim();

                    if(!name.isEmpty() && !text.isEmpty()) {
                        int quantity = Integer.valueOf(text);

                        if (quantity < 1) {
                            quantity = 1;
                        }

                        double value  = 0.0;
                        double weight = 0.0;


                        if (!valueText.isEmpty()) {
                            value = Double.valueOf(valueText);
                        }

                        if (!weightText.isEmpty()) {
                            weight = Double.valueOf(weightText);
                        }

                        if (selectedItem == null) { addItem(name,quantity,value,weight); }
                        else { updateItem(name,quantity,value,weight); }
                    }
                }

                break;
            }

            hideKeyboard();
            displayBackpack();
        }
    }
    private class SkillListener implements SeekBar.OnSeekBarChangeListener {
        @Override public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser && getSeekbarValueSum() <= figure.getSkillPoints()) {

                int id = seekBar.getId();
                figure.setSkillRank(seekBar.getId(), seekBar.getProgress());

                skillValue[id].setText(Integer.toString(figure.getSkillRanks()[id] +
                        figure.getAbilityMod(figure.getSkills()[id].getKeyAbility())));


                skillPointsLeft = figure.getSkillPoints() - figure.getSkillPointSum();
                setFigureWasSaved(false);
                displaySkills();

            }




        }
        @Override public void onStartTrackingTouch(SeekBar seekBar) { }
        @Override public void onStopTrackingTouch(SeekBar seekBar) { }
    }

    private void addItem(String name, int quantity, double value, double weight) {
        handler.open();
        Item addedItem = handler.insert(
                new Item(name,quantity, value, weight),
                figure.getId());

        figure.addItem(addedItem);
        handler.close();

        cancelItemDialog();
        Log.d(LOGTAG, "Added Item: " + name);
    }
    private void updateItem(String name, int quantity, double value, double weight) {
        selectedItem.setName(name);
        selectedItem.setQuantity(quantity);
        selectedItem.setValue(value);
        selectedItem.setWeight(weight);

        figure.updateItem(selectedItem);

        handler.open();
        handler.update(selectedItem, figure.getId());
        handler.close();

        Toast.makeText(getApplicationContext(),
                resources.getString(R.string.message_has_been_updated,
                        selectedItem.getName()), Toast.LENGTH_LONG).show();

        Log.d(LOGTAG, "Updated Item: " + selectedItem.toString());
        cancelItemDialog();
    }
    private void deleteItem() {
        figure.removeItem(selectedItem.getId());

        handler.open();
        handler.delete(selectedItem);
        handler.close();

        Toast.makeText(getApplicationContext(),
                resources.getString(R.string.message_has_been_deleted,
                        selectedItem.getName()), Toast.LENGTH_LONG).show();

        Log.d(LOGTAG, "Deleted Item: " + selectedItem.toString());
        cancelItemDialog();
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Log.d(LOGTAG, "ON CREATE");

        // GET IMPORTANT DATA
        resources  = getResources();
        intent     = getIntent();
        buffer     = new StringBuffer();
        handler    = new DBHandler(this);
        inflater   = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        group = (Group) intent.getSerializableExtra(StaticValues.INTENT_GROUP);
        user  = (User)  intent.getSerializableExtra(StaticValues.INTENT_USER);

        et_name           = findViewById(R.id.name);
        et_lore           = findViewById(R.id.lore_text);
        rb_female         = findViewById(R.id.rb_female);
        rb_male           = findViewById(R.id.rb_male);
        usedAbilityPoints = findViewById(R.id.tv_used_ability_points);

        sp_level     = initializeLevelSpinner();
        sp_race      = initializeTribeSpinner(R.id.spinner_race, resources.getStringArray(R.array.array_races));
        sp_class     = initializeTribeSpinner(R.id.spinner_class, resources.getStringArray(R.array.array_classes));
        sp_alignment = initializeDispositionSpinner(R.id.spinner_alignment, R.array.array_alignments);
        sp_attitude  = initializeDispositionSpinner(R.id.spinner_attitude,R.array.array_attitudes);

        mSensorManager  = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorListener = new ShakeEventListener();
        mSensorListener.setOnShakeListener(new ShakeEventListener.OnShakeListener() {
            @Override public void onShake() {
                showToast(new String[]{resources.getString(R.string.action_random_figure)});
                randomFigure();
            }
        });

        Button addItem = findViewById(R.id.addBackpackItem);
        addItem.setOnClickListener(DetailActivity.this);

        Toolbar toolbar      = findViewById(R.id.toolbar);
        ImageView backButton = toolbar.findViewById(R.id.back_button);
        backButton.setOnClickListener(DetailActivity.this);

        setSupportActionBar(toolbar);
        changeToolbarTitle(toolbar);

        String time;
        if (isExisting()) {
            figure = (Figure) intent.getSerializableExtra(StaticValues.INTENT_FIGURE);
            time = figure.dateToString(StaticValues.DATE_AND_TIME);
            setFigureWasSaved(true);

            handler.open();
            figure.setSkills(handler.selectAllSkills(figure.getId()));
            handler.close();
        }
        else {
            figure = new Figure();
            figure.setOwner(user.getName());
            figure.setGroupId(group.getId());
            time = figure.dateToString(StaticValues.DATE);
        }

        initializeTabHost();
        initializeLocks();
        initializeAbilitySlider();
        initializeSkills();

        // FAVOURITE HEART
        (findViewById(R.id.star_fav)).setOnClickListener(DetailActivity.this);
        ((TextView)findViewById(R.id.creationDate)).setText(resources.getString(R.string.tv_detail_footer, time, this.figure.getOwner()));
        displayFigure();

        et_name.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence arg0,int arg1,int arg2,int arg3) {}
            @Override public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
            @Override public void afterTextChanged(Editable s) {
                figure.setName(s.toString());
                figureWasSaved = false;
            }
        });
        et_lore.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence charSequence, int start, int count, int after) {}
            @Override public void afterTextChanged(Editable editable) {
                figure.setLore(et_lore.getText().toString());
                figureWasSaved = false;
            }
        });
    }
    @Override protected void onResume() {
        super.onResume();
        Log.d(LOGTAG, "ON RESUME");
        hideUi();

        mSensorManager.registerListener(mSensorListener,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_UI);
    }
    @Override protected void onPause() {
        mSensorManager.unregisterListener(mSensorListener);
        super.onPause();
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create, menu);
        return true;
    }
    @Override public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_random : {
                randomFigure();
                break;
            }

            case R.id.action_help: {
                showDialogHelp();
                break;
            }

            case R.id.action_delete : {
                deleteFigure(figure);
                break;
            }

            case R.id.action_move_to : {
                ArrayList<Figure> selectedFigure = new ArrayList<>();
                selectedFigure.add(figure);
                showMoveToGroupDialog(selectedFigure);
                break;
            }

        }
        return true;
    }
    @Override public void    onTabChanged(String tabId) {

        //unselected
        for(int i=0; i<tabHost.getTabWidget().getChildCount(); i++) {
            tabHost.getTabWidget().getChildAt(i).setBackgroundColor(
                    resources.getColor(R.color.colorSecondaryDark));
        }

        // selected
        tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab())
                .setBackgroundColor(resources.getColor(R.color.colorSecondaryVeryDark));
    }

    /* ABILITY Listener */
    @Override public void onStartTrackingTouch(SeekBar seekBar) {}
    @Override public void onStopTrackingTouch(SeekBar seekBar) {
        setFigureValues();
        displayFigure();
    }
    @Override public void onProgressChanged(SeekBar seekBar, int progress, boolean arg2) {
        figure.setAbility(seekBar.getId(), progress);
        displayAbilities();

        int total = 0;
        for (int val : figure.getAbilities()) {
            total += val;
        }

        usedAbilityPoints.setText(resources.getString(R.string.tv_total_ability_points, total));
    }

    /* LOCK, BUTTON Listener */
    @Override public void onClick(View view) {
        switch (view.getId()) {
            case R.id.name_lock:  switchLock(et_name,  0); break;
            case R.id.race_lock:  switchLock(sp_race,  1); break;
            case R.id.class_lock: switchLock(sp_class, 2); break;
            case R.id.level_lock: switchLock(sp_level, 3); break;
            case R.id.star_fav:   setFigureFavorite(view);     break;

            case R.id.btn_help_dismiss:       dialogHelp.dismiss();             break;
            case R.id.dialog_positive_button: figureNameDialogPositive();   break;
            case R.id.dialog_cancel_button:   figureNameDialogNegative();   break;

            case R.id.addBackpackItem: {
                if (figure.getId() != -1) {
                    showDialogAddItem(null);
                } else {
                    Toast.makeText(getApplicationContext(),
                            resources.getString(R.string.message_backpack_warning),
                            Toast.LENGTH_LONG).show();
                }
                break;
            }

            case R.id.dialog_positive: case R.id.dialog_dismiss: {
                dialogSaveFigureButtonAction(view);
                break;
            }

            case R.id.back_button: {

                if (!et_name.getText().toString().isEmpty()) {
                    if (!figureWasSaved) { showDialogSaveFigure(); }
                    else { onBackPressed(); }
                } else {
                    onBackPressed();
                }

                break;
            }
        }
    }
    private void switchLock(View view, int id) {

        if (locks[id]) { iv_lockImages[id].setImageResource(R.mipmap.ic_lock_open_black); }
        else { iv_lockImages[id].setImageResource(R.mipmap.ic_lock_outline_black); }

        locks[id] = !locks[id];
        view.setEnabled(!view.isEnabled());
    }
    private void setFigureFavorite(View view) {
        ImageView image = (ImageView) view;

        if (figure.isFavourite()) { image.setImageResource(R.mipmap.ic_favorite_border_black); }
        else { image.setImageResource(R.mipmap.ic_favorite_black); }

        figure.setFavourite(!figure.isFavourite());
        updateFigureInDatabase(figure);

        Log.d(LOGTAG, "Figure set to favourite=" + figure.isFavourite());
    }
    private void dialogSaveFigureButtonAction(View view) {
        if (view.getId() == R.id.dialog_positive) {
            saveFigure();
        }
        dialogUnsavedChanges.dismiss();
        startActivity(
                new Intent(getApplicationContext(),
                        ListActivity.class)
                        .putExtra(StaticValues.INTENT_GROUP, group)
                        .putExtra(StaticValues.INTENT_USER, user)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK)
        );
        finish();
    }
    private void figureNameDialogPositive() {
        String figureName = et_newFigureName.getText().toString();

        if (!figureName.isEmpty()) {
            et_name.setText(figureName);
            dialogFigureName.cancel();
            saveFigure();
        }
    }
    private void figureNameDialogNegative() {
        dialogFigureName.cancel();
    }

    /* GENDER Listener */
    @Override public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

        setFigureWasSaved(false);
        switch (compoundButton.getId()) {
            case R.id.rb_female: {
                rb_male.setChecked(false);
                rb_female.setChecked(true);
                figure.setGender("Female");
                break;
            }

            case R.id.rb_male: {
                rb_female.setChecked(false);
                rb_male.setChecked(true);
                figure.setGender("Male");
                break;
            }

        }
    }

    /* RACE, CLASS, LEVEL, DISPOSITION Listener */
    @Override public void onNothingSelected(AdapterView<?> parentView) {}
    @Override public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        switch(parentView.getId()) {
            case R.id.spinner_race:  setFigureRace();            break;
            case R.id.spinner_class: setFigureClass();           break;
            case R.id.spinner_level:
                setFigureLevel(position+1);
                for (SeekBar s : this.skillRank) {
                    s.setMax(figure.getLevel() + 3);
                }
                break;

            case R.id.spinner_alignment: case R.id.spinner_attitude: {
                figure.setDisposition(
                        sp_alignment.getSelectedItem().toString(),
                        sp_attitude.getSelectedItem().toString()
                );
                break;
            }
        }
    }
    private void setFigureRace() {

        /* If the value of the race spinner is NOT equal
           to the figures last race, proceed! */

        String selectedRace = getEnglishRaceName(sp_race.getSelectedItem().toString());
        Log.d(LOGTAG, "setFigureRace() selectedRace=" + selectedRace);

        if (!selectedRace.equals(figure.getRace())) {

            figure.removeRaceBonus();
            figure.setRace(selectedRace);

            /*  If the new selected race is NOT a human or halfelf
                proceed to add the new race bonus. */

            if (selectedRace.equals("Human") || selectedRace.equals("Halfelf")) {

                /*  If the selected race IS a human or halfelf
                    display a toast to notify the user. */

                showToast(new String[] {resources.getString(R.string.message_no_race_bonus,
                        getRaceName(figure.getRace()))});

            } else {

                figure.addRaceBonus();
                showToast(new String[]{resources.getString(R.string.message_race_bonus,
                        figure.getBonusAbilityName(0),
                        figure.getBonusAbilityValue(0),
                        figure.getBonusAbilityName(1),
                        figure.getBonusAbilityValue(1))});
            }

            setFigureValues();
            displayFigure();
        }

    }
    private void setFigureClass() {

        /* If the value of the class spinner is NOT equal to
         * the figures last class proceed! */

        String selectedClass = getEnglishClassName(sp_class.getSelectedItem().toString());
        if (!selectedClass.equals(figure.getFigureClass())) {

            figure.setClass(selectedClass);
            figure.distributeAbilities(figure.getFigureClass());

            setFigureValues();
            displayFigure();
        }
    }
    private void setFigureLevel(int newLevel) {

        String figureClass = figure.getFigureClass();
        int figureLevel    = figure.getLevel();

        if(newLevel != figureLevel) {
            Log.d(LOGTAG, "setFigureLevel @param=" + newLevel);

            figure.removeAbilityPoints(figureClass, figureLevel);
            figure.setLevel(newLevel);
            figure.addAbilityPoints(figureClass, newLevel);

            setFigureValues();
            displayFigure();
        }
    }

    /*  TODO Change Language setup
        I do not like this setup because I
        have to add other languages manually.

        I tried to use resources.getString() but I'm
        not allowed to use it in a switch
        statement. */

    @NonNull private String getRaceName(String race) {
        Log.d(LOGTAG, "getRaceName @param=" + race);
        /*  This method is used to display the
            language specific race names. It is needed
            because the Tribe class and Race class are both
            using the english identifiers. */

        switch (race.toLowerCase()) {
            case "dwarf":    return resources.getString(R.string.dwarf);
            case "elf":      return resources.getString(R.string.elf);
            case "gnome":    return resources.getString(R.string.gnome);
            case "halfelf":  return resources.getString(R.string.halfelf);
            case "halfling": return resources.getString(R.string.halfling);
            case "halfork":  return resources.getString(R.string.halfork);
            default:         return resources.getString(R.string.human);
        }
    }
    private String getEnglishRaceName(String race) {

        /*  This method is used to get the english identifier
            of a race so the Tribe class works as expected. */

        if (languageIsEnglish()) {
            return race;
        } else {
            Log.d(LOGTAG, "getEnglishRaceName switch needed: @param=" + race);

            switch (race.toLowerCase()) {
                case "dwarf":   case "zwerg":    return "Dwarf";
                case "elf":                      return "Elf";
                case "gnome":   case "gnom":     return "Gnome";
                case "halfelf": case "halbelf":  return "Halfelf";
                case "halfling":case "halbling": return "Halfling";
                case "halfork": case "halbork":  return "Halfork";
                default:                         return "Human";
            }
        }
    }
    private String getEnglishClassName(String fclass) {

        /*  This method is used to get the english identifier
            of a class so the Tribe class works as expected. */

        if (languageIsEnglish()) {
            return fclass;
        } else {
            Log.d(LOGTAG, "getEnglishClassName switch needed: @param=" + fclass);

            switch (fclass.toLowerCase()) {
                case "barbarian": case "barbar":     return "Barbarian";
                case "bard":      case "barde":      return "Bard";
                case "cleric":    case "kleriker":   return "Cleric";
                case "druid":     case "druide":     return "Druid";
                case "fighter":   case "kämpfer":    return "Fighter";
                case "mage":      case "magier":     return "Mage";
                case "monk":      case "mönch":      return "Monk";
                case "paladin":                      return "Paladin";
                case "ranger":    case "waldläufer": return "Ranger";
                case "rogue":     case "schurke":    return "Rogue";
                default:                             return "Warlock";
            }
        }

    }

    private boolean languageIsEnglish() {
        Log.d(LOGTAG, "LanguageIsEnglish() Language=" + user.getLanguage());
        return user.getLanguage().equals(StaticValues.LANGUAGE_EN);
    }
    public void actionButtonSaveFigureDialog(View view) {

        // OLD FIGURE NOT FAVOURITE
        if (!figure.isFavourite() && !figureWasSaved &&
            !et_name.getText().toString().isEmpty())  {
            saveFigure();
        }

        // OLD FIGURE AND FAVOURITE
        else if (figure.isFavourite() && !figureWasSaved) {
            showOverrideFigureDialog();
        }

        // NEW FIGURE
        else if (et_name.getText().toString().isEmpty()){
            showFigureNameDialog();
        }

        // FIGURE IS UP-TO-DATE
        else {
            Toast.makeText(getApplicationContext(),
                    resources.getString(R.string.message_nothing_to_save),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void    initializeTabHost() {
        tabHost = findViewById(R.id.tabHost);
        tabHost.setup();

        String tabText[] = {
                resources.getString(R.string.tab_title_properties),
                resources.getString(R.string.tab_title_skills),
                resources.getString(R.string.tab_title_lore),
                resources.getString(R.string.tab_title_backpack) };

        int layoutIds[] = {R.id.tab1, R.id.tab4, R.id.tab2, R.id.tab3};

        for (int i=0; i<tabText.length; i++) {
            tabHost.addTab(
                    makeTab(
                            tabText[i],
                            layoutIds[i]
                    )
            );

            ((TextView) tabHost.getTabWidget().getChildAt(i)
                    .findViewById(android.R.id.title))
                    .setTextColor(resources.getColor(R.color.font));
        }

        tabHost.setOnTabChangedListener(DetailActivity.this);
        tabHost.getTabWidget().getChildAt(0).setBackgroundColor(
                resources.getColor(R.color.colorSecondaryVeryDark)); //selected
    }
    private void    initializeAbilitySlider() {
        sb_abilities = new SeekBar[6];
        tv_abilityValues = new TextView[sb_abilities.length];
        tv_abilityMods = new TextView[sb_abilities.length];

        String[] ability_names = {resources.getString(R.string.tv_strength), resources.getString(R.string.tv_dexterity), resources.getString(R.string.tv_constitution), resources.getString(R.string.tv_intelligence), resources.getString(R.string.tv_wisdom), resources.getString(R.string.tv_charisma)};
        TableLayout tl_abilities = findViewById(R.id.detail_abilityTable);

        for (int i=0; i<sb_abilities.length; i++) {

            View row = inflater.inflate(R.layout.item_row_ability, null);
            ((TextView) row.findViewById(R.id.abilityIdentifier)).setText(ability_names[i]);

            sb_abilities[i] = row.findViewById(R.id.abilitySeekBar);
            sb_abilities[i].setId(i);
            sb_abilities[i].setMax(StaticValues.MAX_LEVEL);
            sb_abilities[i].setOnSeekBarChangeListener(DetailActivity.this);

            tv_abilityValues[i] = row.findViewById(R.id.abilityValue);
            tv_abilityValues[i].setText(Integer.toString(figure.getAbility(i)));
            tv_abilityValues[i].setId(i);

            tv_abilityMods[i] = row.findViewById(R.id.AbilityMod);
            tv_abilityMods[i].setText(Integer.toString(figure.getAbilityMod(i)));
            tv_abilityMods[i].setId(i);
            tl_abilities.addView(row);
        }
    }
    private void    initializeLocks() {
        this.locks = new boolean[4];
        this.iv_lockImages = new ImageView[]{
                findViewById(R.id.name_lock),
                findViewById(R.id.race_lock),
                findViewById(R.id.class_lock),
                findViewById(R.id.level_lock),
        };

        for (ImageView img : iv_lockImages) {
            img.setOnClickListener(DetailActivity.this);
        }
    }
    private Spinner initializeTribeSpinner(int spinnerId, String[] res) {
        Spinner spinner = findViewById(spinnerId);
        Arrays.sort(res);

        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(
                this, android.R.layout.simple_spinner_item, res);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(DetailActivity.this);
        return spinner;
    }
    private Spinner initializeLevelSpinner() {
        String[] levels = new String[StaticValues.MAX_LEVEL];

        for (int i=0; i<30; i++) {
            levels[i] = Integer.toString(i+1);
        }

        Spinner spinner = findViewById(R.id.spinner_level);
        ArrayAdapter<CharSequence> levelAdapter = new ArrayAdapter<CharSequence>(
                this, android.R.layout.simple_spinner_item, levels
        );

        levelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(DetailActivity.this);
        spinner.setAdapter(levelAdapter);
        return spinner;
    }
    private Spinner initializeDispositionSpinner(int spinnerId, int identifiers) {
        Spinner spinner = findViewById(spinnerId);
        spinner.setAdapter(makeAdapter(identifiers));
        spinner.setOnItemSelectedListener(DetailActivity.this);
        return spinner;
    }

    // DISPLAY FIGURE METHODS
    private void displayFigure() {
        sp_race.setOnItemSelectedListener(null);
        sp_class.setOnItemSelectedListener(null);
        sp_level.setOnItemSelectedListener(null);

        et_name.setText(figure.getName());
        et_lore.setText(figure.getLore());

        if (figure.isFavourite()) {
            ((ImageView) findViewById(R.id.star_fav)).setImageResource(R.mipmap.ic_favorite_black);
        }

        sp_level.setSelection(figure.getLevel()-1);
        sp_race.setSelection(selectRaceInSpinner());
        sp_class.setSelection(selectClassInSpinner());

        displayBaseAttackBonus();
        displayAbilities();
        displayDisposition();
        displayGender();
        displayBackpack();
        displaySkills();
        updateTable();

        sp_level.setOnItemSelectedListener(DetailActivity.this);
        sp_race.setOnItemSelectedListener(DetailActivity.this);
        sp_class.setOnItemSelectedListener(DetailActivity.this);
    }
    private int selectRaceInSpinner() {
        String figureRace = figure.getRace();
        int position = 0;

        for (int i=0; i<this.sp_race.getCount(); i++) {
            if (getEnglishRaceName(this.sp_race.getItemAtPosition(i).toString())
                    .equals(figureRace)) {
                position = i;
                break;
            }
        }
        return position;
    }
    private int selectClassInSpinner() {
        String figureClass = figure.getFigureClass();
        int position = 0;

        for (int i=0; i<this.sp_class.getCount(); i++) {
            if (getEnglishClassName(this.sp_class.getItemAtPosition(i).toString())
                    .equals(figureClass)) {
                position = i;
                break;
            }
        }
        return position;
    }

    private void displayDisposition() {
        this.sp_alignment.setSelection(getDispositionId(figure.getAlignment()));
        this.sp_attitude.setSelection(getDispositionId(figure.getAttitude()));
    }
    private void displayAbilities() {
        for(int i=0; i<sb_abilities.length; i++) {
            sb_abilities[i].setProgress(figure.getAbility(i));
            tv_abilityValues[i].setText(Integer.toString(figure.getAbility(i)));
            tv_abilityMods[i].setText(Integer.toString(figure.getAbilityMod(i)));
        }
    }
    private void displayGender() {
        rb_female.setOnCheckedChangeListener(null);
        rb_male.setOnCheckedChangeListener(null);

        switch (figure.getGender().toLowerCase()) {

            case "male": {
                rb_female.setChecked(false);
                rb_male.setChecked(true);
                break;
            }

            default:
                rb_male.setChecked(false);
                rb_female.setChecked(true);
                break;
        }
        rb_female.setOnCheckedChangeListener(DetailActivity.this);
        rb_male.setOnCheckedChangeListener(DetailActivity.this);
    }
    private void displayBaseAttackBonus() {
        ((TextView) findViewById(R.id.bab_base)).setText(resources.getString(R.string.tv_bab_placeholder,figure.getBaseAttackBonus()));
        ((TextView) findViewById(R.id.bab_attacks)).setText(Integer.toString(figure.getAttacks()));
        ((TextView) findViewById(R.id.bab_maxValue)).setText(resources.getString(R.string.tv_bab_melee_ranged,figure.getMaxMelee(),figure.getMaxRanged()));
        RelativeLayout modifierContainer = findViewById(R.id.bab_modifier_container);

        if (figure.getAttacks() == 1) {
            modifierContainer.setVisibility(View.GONE);
        } else {
            modifierContainer.setVisibility(View.VISIBLE);
            TextView modifier = findViewById(R.id.bab_modifier);
            modifier.setText(Integer.toString(figure.getAttackModifier()));
        }
    }
    private void displayBackpack() {

        TextView gold = findViewById(R.id.figure_gold_value);
        gold.setText(resources.getString(R.string.tv_gold,
                figure.getGold()));

        handler.open();
        TextView totalWeight = findViewById(R.id.figure_total_weight);
        totalWeight.setText(resources.getString(R.string.tv_total_weight,
                Double.toString(handler.getItemWeightSum(figure.getId()))));
        handler.close();

        TextView hide = findViewById(R.id.tv_total);
        hide.setVisibility(View.GONE);

        TableLayout backpack = findViewById(R.id.table);
        backpack.removeAllViews();

        ArrayList<Item> items = figure.getItems();
        Collections.sort(items, new Comparator<Item>() {
            public int compare(Item i1, Item i2) {
                return i1.getName().toLowerCase().compareTo(i2.getName().toLowerCase());
            }
        });

        ItemClickListener itemListener = new ItemClickListener();

        for (int i=0; i<items.size(); i++) {
            View row = inflater.inflate(R.layout.item_row_backpack, null);

            TextView name = row.findViewById(R.id.backpack_item_name);
            name.setText(items.get(i).getName());

            TextView value = row.findViewById(R.id.backpack_item_value);
            value.setText(resources.getString(R.string.tv_backpack_item_value,
                    String.format("%.02f", figure.getItems().get(i).getValue())));

            TextView weight = row.findViewById(R.id.backpack_item_weight);
            weight.setText(resources.getString(R.string.tv_backpack_item_weight,
                    String.format("%.02f", figure.getItems().get(i).getWeight())));

            TextView quantity = row.findViewById(R.id.backpack_item_quantity);
            quantity.setText(resources.getString(R.string.tv_backpack_item_quantity,
                    figure.getItems().get(i).getQuantity()));

            row.setId(Integer.valueOf(Long.toString(items.get(i).getId())));
            row.setOnClickListener(itemListener);

            if (i%2 == 0) {
                row.setBackgroundColor(resources.getColor(R.color.colorPrimaryDarkTable));
            }

            backpack.addView(row);
        }
    }

    private View setHeader(String text) {
        View group = inflater.inflate(R.layout.item_skill_header, null);
        TextView title = group.findViewById(R.id.skill_group);
        title.setText(text);
        return group;
    }

    private void initializeSkills() {
        String groups[] = resources.getStringArray(R.array.skillGroup);
        TableLayout skillTable = findViewById(R.id.skill_table);
        skillTable.removeAllViews();

        skillValue = new TextView[figure.getSkills().length];
        skillRank = new SeekBar[skillValue.length];
        SkillListener skillListener = new SkillListener();

        for (int i=0; i<figure.getSkills().length; i++) {
            if (i==0) {
                skillTable.addView(setHeader(groups[0]));
            } else if(i==3) {
                skillTable.addView(setHeader(groups[1]));
            } else if(i==12) {
                skillTable.addView(setHeader(groups[2]));
            } else if (i==13) {
                skillTable.addView(setHeader(groups[3]));
            } else if(i==21) {
                skillTable.addView(setHeader(groups[4]));
            } else if(i==26) {
                skillTable.addView(setHeader(groups[5]));
            }

            View row = inflater.inflate(R.layout.item_skill_row, null);
            TextView skillName = row.findViewById(R.id.skill_name);
            skillName.setText(resources.getStringArray(R.array.skills)[i]);

            skillValue[i] = row.findViewById(R.id.skill_value);
            skillValue[i].setText(Integer.toString(figure.getSkillRanks()[i] +
                    figure.getAbilityMod(figure.getSkills()[i].getKeyAbility())));

            skillRank[i] = row.findViewById(R.id.skill_rank);
            skillRank[i].setId(i);
            skillRank[i].setMax(figure.getLevel()+3);
            skillRank[i].setProgress(figure.getSkillRanks()[i]);
            skillRank[i].setOnSeekBarChangeListener(skillListener);

            if (i%2==0) { row.setBackgroundColor(resources.getColor(R.color.colorPrimary)); }
            skillTable.addView(row);
        }

        this.skillpoints = findViewById(R.id.skillpoints);
        skillPointsLeft = figure.getSkillPoints() - getSeekbarValueSum();
    }
    private void displaySkills() {
        Skill skills[] = this.figure.getSkills();
        int skillRanks[] = figure.getSkillRanks();

        for (int i=0; i<skills.length; i++) {
            skillRank[i].setMax(figure.getLevel()+3);
            skillRank[i].setProgress(skillRanks[i]);
            skillValue[i].setText(Integer.toString(skillRanks[i] +
                    figure.getAbilityMod(skills[i].getKeyAbility()) ));
        }

        skillpoints.setText(String.format( resources.getString(R.string.tv_bab_melee_ranged),
                skillPointsLeft, figure.getSkillPoints()));
    }

    // FIGURE METHODS
    private void deleteFigure(Figure figure) {
        Log.d(LOGTAG, "DELETE FIGURE: " + figure.toString());
        if (figure.getId() != -1) {

            if (!figure.isFavourite()) { showDialogDeleteFigure(); }
            else {
                showToast(new String[]{resources.getString(R.string.message_cannot_delete)});
            }

        } else {
            showToast(new String[]{resources.getString(R.string.message_nothing_to_delete)});
        }
    }
    private void randomFigure() {
        Log.d(LOGTAG, "Random figure.");

        String name   = "";
        String race   = "";
        String fclass = "";
        int level     = 1;

        if (locks[0]) { name   = figure.getName(); }
        if (locks[1]) { race   = figure.getRace(); }
        if (locks[2]) { fclass = figure.getFigureClass(); }
        if (locks[3]) { level  = figure.getLevel(); }

        figure = new Figure();
        figure.random();


        if (locks[3]) { figure.setLevel(level); }
        if (!name.trim().isEmpty()) { figure.setName(name); }

        if (!race.isEmpty()) {
            figure.removeRaceBonus();
            figure.setRace(race);
            figure.addRaceBonus();
        }

        if (!fclass.isEmpty()) {
            figure.setClass(fclass);
            figure.distributeAbilities(figure.getFigureClass());
        }

        figure.calculateFigure();
        setFigureWasSaved(false);
        displayFigure();

    }
    private void setFigureValues() {
        Log.d(LOGTAG, "SetFigureValues()");
        figure.setBaseAttackBonus();
        figure.calculateHitpoints();
        figure.calculateArmor();
        figure.calculateSkillPoints();
        figure.calculateTalentPoints();
        figure.calculateSavingThrows();
        figure.calculateGold();

        setFigureWasSaved(false);
    }
    private void saveFigure() {
        setFigureWasSaved(true);
        figure.setGroupId(group.getId());

        handler.open();
        if (isExisting()) {

            handler.update(figure);
            handler.update(figure.getId(), figure.getSkills());

            showToast(new String[]{resources.getString(R.string.message_has_been_updated, figure.getName())});
            Log.d(LOGTAG, "UPDATE Figure");

        } else {

            // Add next figure to counter
            user.addTotalCreatedFigure();
            handler.update(user);

            this.figure = handler.insert(figure);

            for (int i=0; i<figure.getSkills().length; i++) {
                handler.insert(figure.getSkills()[i], figure.getId());
            }

            showToast(new String[]{resources.getString(R.string.message_has_been_saved, figure.getName())});
            Log.d(LOGTAG, "INSERT Figure");
        }

        handler.close();
    }
    private boolean isExisting() {
        return intent != null && intent.getBooleanExtra(StaticValues.INTENT_IS_EXISTING, false);
    }

    // UPDATE METHODS
    private void setFigureWasSaved(boolean wasSaved) {
        Log.d(LOGTAG, "setFigureWasSaved @param=" + wasSaved);
        this.figureWasSaved = wasSaved;
    }
    private void updateFigureInDatabase(Figure figure) {
        Log.d(LOGTAG, "UpdateFigureInDatabase: " + figure.toString());
        handler.open();
        handler.update(figure);
        handler.close();
    }
    private void updateTable() {
        Log.d(LOGTAG, "UpdateTable()");
        TableLayout tl_values_table = findViewById(R.id.detail_generated_values_table);
        tl_values_table.removeAllViewsInLayout();

        String[] tableIdentifiers = resources.getStringArray(R.array.array_tableIdentifiers);
        int[] tableValues = {
                figure.getHitPoints(),
                figure.getArmor(),
                figure.getSkillPoints(),
                figure.getTalentPoints(),
                figure.getSavingThrowAt(0),
                figure.getSavingThrowAt(1),
                figure.getSavingThrowAt(2)
        };

        for (int i=0; i<tableValues.length; i++) {
            View row = inflater.inflate(R.layout.item_row_detail, null);
            ((TextView) row.findViewById(R.id.identifier)).setText(tableIdentifiers[i]);
            ((TextView) row.findViewById(R.id.value)).setText(String.valueOf(tableValues[i]));

            if (i%2==0) { row.setBackgroundColor(resources.getColor(R.color.colorPrimaryDarkTable)); }
            tl_values_table.addView(row);
        }
    }

    // DIALOG METHODS
    private void showDialogHelp() {
        if (dialogHelp == null) {

            dialogHelp = new Dialog(this);
            dialogHelp.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
            dialogHelp.setContentView(R.layout.dialog_simple_table);
            dialogHelp.setTitle(R.string.dialog_title_help);

            TextView title = dialogHelp.findViewById(R.id.dialog_title);

            title.setText(resources.getString(R.string.dialog_title_help));

            int color = resources.getColor(R.color.lightGreyBackground);
            TableLayout table = dialogHelp.findViewById(R.id.dialog_table);
            ArrayList<String[]> title_text = setHelpContent(
                    resources.getStringArray(R.array.dialog_titles_help),
                    resources.getStringArray(R.array.dialog_info_help)
            );

            Collections.sort(title_text, new Comparator<String[]>() {
                public int compare(String[] s1, String[] s2) {
                    return s1[0].compareTo(s2[0]);
                }
            });

            for (int i=0; i<title_text.size(); i++) {
                View row = inflater.inflate(R.layout.item_row_help, null);
                if (i%2 == 0) { row.setBackgroundColor(color); }

                ((TextView)row.findViewById(R.id.help_title)).setText(title_text.get(i)[0]);
                final TextView helpText = row.findViewById(R.id.help_text);
                helpText.setText(title_text.get(i)[1]);
                helpText.setVisibility(View.GONE);
                helpText.setId(i);

                row.setOnClickListener(new View.OnClickListener() {
                    @Override public void onClick(View view) {
                        if (helpText.getVisibility() == View.GONE) {
                            helpText.setVisibility(View.VISIBLE);
                        } else {
                            helpText.setVisibility(View.GONE);
                        }
                    }
                });

                table.addView(row);
            }

            (dialogHelp.findViewById(R.id.btn_help_dismiss)).setOnClickListener(DetailActivity.this);
        }
        dialogHelp.show();
    }
    private void showDialogSaveFigure() {
        if (dialogUnsavedChanges == null) {
            dialogUnsavedChanges = new Dialog(DetailActivity.this);

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                dialogUnsavedChanges.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
                dialogUnsavedChanges.setTitle(resources.getString(R.string.dialog_title_warning));
            }

            dialogUnsavedChanges.setContentView(R.layout.dialog_warning);
            ((TextView) dialogUnsavedChanges.findViewById(R.id.dialog_message))
                    .setText(resources.getString(R.string.dialog_info_save_figure));

            Button positive = dialogUnsavedChanges.findViewById(R.id.dialog_positive);
            positive.setOnClickListener(DetailActivity.this);
            positive.setText(resources.getString(R.string.action_save_figure));

            Button negative = dialogUnsavedChanges.findViewById(R.id.dialog_dismiss);
            negative.setText(resources.getString(R.string.action_without_saving));
            negative.setTextColor(resources.getColor(R.color.colorSecondaryVeryDark));
            negative.setOnClickListener(DetailActivity.this);
        }

        dialogUnsavedChanges.show();
    }
    private void showDialogDeleteFigure() {
        final Dialog deleteFigureDialog = new Dialog(DetailActivity.this);

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            deleteFigureDialog.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
            deleteFigureDialog.setTitle(resources.getString(R.string.dialog_title_warning));
        }

        deleteFigureDialog.setContentView(R.layout.dialog_warning);
        TextView title = deleteFigureDialog.findViewById(R.id.dialog_message);
        title.setText(resources.getQuantityString(R.plurals.message_delete_this_figure,1));

        Button positive = deleteFigureDialog.findViewById(R.id.dialog_positive);
        positive.setText(R.string.action_yes);
        positive.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ArrayList<Figure> figuresToDelete = new ArrayList<>();
                figuresToDelete.add(figure);

                startActivity(new Intent(getApplicationContext(), DeleteFigureActivity.class)
                        .putExtra(StaticValues.INTENT_FIGURES_TO_DELETE,figuresToDelete)
                        .putExtra(StaticValues.INTENT_GROUP, group)
                        .putExtra(StaticValues.INTENT_USER, user));

                deleteFigureDialog.cancel();
            }
        });


        Button negative = deleteFigureDialog.findViewById(R.id.dialog_dismiss);
        negative.setText(R.string.action_no);
        negative.setTextColor(resources.getColor(R.color.colorSecondaryVeryDark));
        negative.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                deleteFigureDialog.cancel();
            }
        });

        deleteFigureDialog.show();
    }
    private void showMoveToGroupDialog(final ArrayList<Figure> figures) {
        final Dialog dialog_move_figures = new Dialog(DetailActivity.this);
        dialog_move_figures.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
        dialog_move_figures.setContentView(R.layout.dialog_move_figures);
        dialog_move_figures.setTitle(resources.getString(R.string.action_move));

        int color = resources.getColor(R.color.lightGreyBackground);
        TableLayout table = dialog_move_figures.findViewById(R.id.dialog_move_to_group_table);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        handler.open();
        ArrayList<Group> groups = handler.selectAllGroups(DBConstants.SELECT_ALL_FROM_GROUP);

        for (int i=0; i<groups.size(); i++) {
            if (!groups.get(i).equals(group)) {
                View row = inflater.inflate(R.layout.item_row_move_to_group, null);

                TextView groupName = row.findViewById(R.id.group_name);
                groupName.setText(groups.get(i).getName());

                TextView groupSize = row.findViewById(R.id.group_size);
                groupSize.setText(resources.getString(R.string.tv_group_figure_amount,
                        handler.countFiguresInGroup(groups.get(i).getId())));

                if (i%2==0) {row.setBackgroundColor(color);}

                row.setId(Integer.valueOf(Long.toString(groups.get(i).getId())));
                row.setOnClickListener(new View.OnClickListener() {
                    @Override public void onClick(View view) {

                        dialog_move_figures.cancel();

                        final Dialog warning = new Dialog(DetailActivity.this);
                        warning.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
                        warning.setContentView(R.layout.dialog_warning);
                        warning.setTitle(resources.getString(R.string.dialog_title_warning));

                        handler.open();
                        final Group group = handler.selectGroup(DBConstants.SELECT_GROUP_WITH_ID + view.getId());
                        handler.close();

                        TextView dialog_message = warning.findViewById(R.id.dialog_message);
                        dialog_message.setText(resources.getString(R.string.message_move_to_group,
                                group.getName()));


                        Button dialog_dismiss = warning.findViewById(R.id.dialog_dismiss);
                        dialog_dismiss.setText(R.string.action_cancel);
                        dialog_dismiss.setOnClickListener(new View.OnClickListener() {
                            @Override public void onClick(View view) {
                                warning.cancel();
                            }
                        });

                        Button dialog_positive = warning.findViewById(R.id.dialog_positive);
                        dialog_positive.setText(R.string.action_move);
                        dialog_positive.setOnClickListener(new View.OnClickListener() {
                            @Override public void onClick(View view) {

                                startActivity(new Intent(getApplicationContext(), MoveFiguresToGroupActivity.class)
                                        .putExtra(StaticValues.INTENT_GROUP, group)
                                        .putExtra(StaticValues.INTENT_NEW_GROUP_ID, group.getId())
                                        .putExtra(StaticValues.INTENT_USER, user)
                                        .putExtra(StaticValues.INTENT_FIGURES_TO_MOVE, figures));
                            }
                        });

                        warning.show();

                    }
                });

                table.addView(row);
            }
        }
        handler.close();

        Button negative = dialog_move_figures.findViewById(R.id.dialog_negative);
        negative.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                dialog_move_figures.cancel();
            }
        });

        dialog_move_figures.show();
    }
    private void showDialogAddItem(View view) {

        dialogItem = new Dialog(this);
        dialogItem.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
        dialogItem.setContentView(R.layout.dialog_add_backpack_item);
        dialogItem.setTitle(R.string.action_add_item);
        dialogItem.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override public void onCancel(DialogInterface dialog) {
                cancelItemDialog();
            }
        });

        ItemListener il = new ItemListener();
        Button delete   = dialogItem.findViewById(R.id.dialog_delete);
        Button positive = dialogItem.findViewById(R.id.dialog_positive);
        positive.setOnClickListener(il);

        if (view == null) {
            delete.setVisibility(View.GONE);

        } else {

            selectedItem = figure.getItem(view.getId());
            EditText et_name = dialogItem.findViewById(R.id.item_name);
            et_name.setText(selectedItem.getName());

            EditText et_quantity = dialogItem.findViewById(R.id.item_quantity);
            EditText et_value    = dialogItem.findViewById(R.id.item_value);
            EditText et_weight   = dialogItem.findViewById(R.id.item_weight);

            et_quantity.setText(Integer.toString(selectedItem.getQuantity()));
            et_value.setText(Double.toString(selectedItem.getValue()));
            et_weight.setText(Double.toString(selectedItem.getWeight()));

            positive.setText(R.string.action_save_figure);
            delete.setOnClickListener(il);
        }

        Button negative = dialogItem.findViewById(R.id.dialog_negative);
        negative.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                cancelItemDialog();
            }
        });

        dialogItem.show();
    }
    private void showOverrideFigureDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_warning);
        dialog.setTitle(resources.getString(R.string.dialog_title_warning));

        Button negative = dialog.findViewById(R.id.dialog_dismiss);
        negative.setText(R.string.action_no);
        negative.setTextColor(resources.getColor(R.color.colorSecondaryVeryDark));
        negative.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                dialog.cancel();
            }
        });

        TextView message = dialog.findViewById(R.id.dialog_message);
        message.setText(resources.getString(R.string.message_override_figure));

        Button positive = dialog.findViewById(R.id.dialog_positive);
        positive.setText(R.string.action_yes);
        positive.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                saveFigure();
                dialog.cancel();
            }
        });
        dialog.show();
    }
    private void showFigureNameDialog() {
        dialogFigureName = new Dialog(this);
        dialogFigureName.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
        dialogFigureName.setContentView(R.layout.dialog_input);

        dialogFigureName.setTitle(resources.getString(R.string.dialog_title_figure_name));
        TextView title = dialogFigureName.findViewById(R.id.dialog_info_title);
        title.setVisibility(View.GONE);

        et_newFigureName = dialogFigureName.findViewById(R.id.dialog_input_field);
        (dialogFigureName.findViewById(R.id.dialog_cancel_button)).setOnClickListener(DetailActivity.this);
        (dialogFigureName.findViewById(R.id.dialog_positive_button)).setOnClickListener(DetailActivity.this);
        dialogFigureName.show();
    }

    // MISC METHODS
    private ArrayList<String[]> setHelpContent(String[] titles, String[]texts) {

        ArrayList<String[]> helpText = new ArrayList<>();
        for (int i=0; i<titles.length; i++) {
            helpText.add(new String[]{ titles[i], texts[i] });
        }

        return helpText;
    }
    private void showToast(String[] msg) {
        Toast.makeText(
                DetailActivity.this,
                makeString(msg),
                Toast.LENGTH_SHORT
        ).show();
    }
    private String makeString(String[] msg) {
        this.buffer.setLength(0);
        for (String s : msg) {
            this.buffer.append(s);
        }

        return this.buffer.toString();
    }
    private TabHost.TabSpec makeTab(String tabText, int contentId) {
        TabHost.TabSpec tab = tabHost.newTabSpec(tabText);
        tab.setIndicator(tab.getTag()).setContent(contentId);
        return tab;
    }
    private int getDispositionId(String disposition) {
        Log.d(LOGTAG, "getDispositionId @param=" + disposition);
        switch(disposition.toLowerCase()) {
            case "righteous":
            case "rechtschaffend":
            case "good":
            case "gut":
                return 0;

            case "chaotic":
            case "chaotisch":
            case "evil":
            case "böse":
                return 2;

            // Neutral alignment or attitude
            default:
                return 1;
        }
    }
    private ArrayAdapter makeAdapter(int res) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                DetailActivity.this, res,
                android.R.layout.simple_spinner_item
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }
    private void changeToolbarTitle(Toolbar toolbar) {
        TextView title = toolbar.findViewById(R.id.toolbar_title);

        if (isExisting()) { title.setText(resources.getString(R.string.activity_title_detail_edit)); }
        else { title.setText(resources.getString(R.string.activity_title_detail_create)); }
    }
    private void hideUi() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    private void cancelItemDialog() {
        hideKeyboard();
        dialogItem.cancel();
        selectedItem = null;
    }
    private int getSeekbarValueSum() {
        int sum = 0;
        for (SeekBar s : this.skillRank) {
            sum += s.getProgress();
        }
        return sum;
    }
}