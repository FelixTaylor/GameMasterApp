package felixtaylor.gamemaster.activities;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import felixtaylor.gamemaster.R;
import felixtaylor.gamemaster.StaticValues;
import felixtaylor.gamemaster.database.DBConstants;
import felixtaylor.gamemaster.database.DBHandler;
import felixtaylor.gamemaster.system.User;

public class SettingsActivity extends AppCompatActivity {

    private User user;
    private DBHandler dbHandler;
    private class LanguageListener implements AdapterView.OnItemSelectedListener {
        @Override public void onNothingSelected(AdapterView<?> parentView) {}
        @Override public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            if (
                    user.getLanguage().equals(StaticValues.LANGUAGE_EN) &&
                    !parentView.getSelectedItem().toString().equals(StaticValues.LANGUAGE_ENGLISH) ||

                    user.getLanguage().equals(StaticValues.LANGUAGE_DE) &&
                    !parentView.getSelectedItem().toString().equals(StaticValues.LANGUAGE_GERMAN) ||

                    user.getLanguage().equals(StaticValues.LANGUAGE_PL) &&
                    !parentView.getSelectedItem().toString().equals(StaticValues.LANGUAGE_POLISH)) {

                switch (parentView.getSelectedItem().toString()) {
                    case StaticValues.LANGUAGE_GERMAN:
                        setLocale(StaticValues.LANGUAGE_DE);
                        user.setLanguage(StaticValues.LANGUAGE_DE);
                        break;

                    case StaticValues.LANGUAGE_POLISH:
                        setLocale(StaticValues.LANGUAGE_PL);
                        user.setLanguage(StaticValues.LANGUAGE_PL);
                        break;

                    case StaticValues.LANGUAGE_ENGLISH:
                        setLocale(StaticValues.LANGUAGE_EN);
                        user.setLanguage(StaticValues.LANGUAGE_EN);
                        break;
                }

                updateUser(user);
                Toast.makeText(
                        getApplicationContext(),
                        getResources().getString(R.string.message_language_set_to, parentView.getSelectedItem().toString()),
                        Toast.LENGTH_SHORT
                ).show();
            }
        }
    }

    @Override protected void onResume() {
        super.onResume();
        hideUi();
    }
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = findViewById(R.id.toolbar);
        initializeSearchbar(toolbar);

        dbHandler = new DBHandler(getApplicationContext());
        dbHandler.open();
        user = dbHandler.getUser();
        dbHandler.close();

        ((TextView) findViewById(R.id.tv_versions))
                .setText(getResources().getString(R.string.tv_app_info,
                        getResources().getString(R.string.tv_version), DBConstants.DBVERSION));

        if (!this.user.getName().equals(getResources().getString(R.string.hint_username))) {
            ((EditText) findViewById(R.id.setting_username)).setText(user.getName());
        }

        ((EditText) findViewById(R.id.setting_username))
                .addTextChangedListener(new TextWatcher() {
                    @Override public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
                    @Override public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
                    @Override public void afterTextChanged(Editable s) {

                        if (!s.toString().trim().isEmpty()) {
                            user.setName(s.toString());
                            updateUser(user);
                        } else {
                            user.setName(getResources().getString(R.string.hint_username));
                            user.setShowUserNamePopUp(1);
                            updateUser(user);
                        }
                }
        });

        ArrayAdapter<CharSequence> languageAdapter =
                ArrayAdapter.createFromResource(
                        this,
                        R.array.array_languages,
                        android.R.layout.simple_spinner_item
                );

        languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner languageSpinner = findViewById(R.id.spinner_language);
        languageSpinner.setAdapter(languageAdapter);

        switch (user.getLanguage()) {
            case StaticValues.LANGUAGE_EN: languageSpinner.setSelection(0, true); break;
            case StaticValues.LANGUAGE_DE: languageSpinner.setSelection(1, true); break;
            case StaticValues.LANGUAGE_PL: languageSpinner.setSelection(2, true); break;
        }

        languageSpinner.setOnItemSelectedListener(new LanguageListener());
        TextView infotext = findViewById(R.id.settings_overall_figures);

        String groups = getResources().getQuantityString(
                R.plurals.tv_settings_user_groups,
                user.getTotalCreatedGroups(),
                user.getTotalCreatedGroups());

        String figures = getResources().getQuantityString(
                R.plurals.tv_settings_user_figures,
                user.getTotalCreatedFigures(),
                user.getTotalCreatedFigures());

        // TODO: How can I avoid concatenating these strings?
        infotext.setText(getResources().getString(R.string.tv_settings_user_date,
                dateToString(StaticValues.DATE)) + " " + groups + "" + figures);

    }

    private void updateUser(User user) {
        dbHandler.open();
        dbHandler.update(user);
        dbHandler.close();
    }
    public void setLocale(String lang) {

        Configuration conf = getResources().getConfiguration();
        conf.locale = new Locale(lang);
        getResources().updateConfiguration(conf, getResources().getDisplayMetrics());

        startActivity(
                new Intent(
                        this,
                        GroupActivity.class)
        );
        finish();
    }
    private void initializeSearchbar(Toolbar toolbar) {
        toolbar.setBackground(null);

        TextView title = toolbar.findViewById(R.id.toolbar_title);
        title.setText(getResources().getString(R.string.activity_title_settings));

        ImageView backButton = toolbar.findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    private void hideUi() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
    public String  dateToString(String displayPattern) {
        return new SimpleDateFormat(
                displayPattern,
                Locale.getDefault()
        ).format(new Date(user.getInitialUseTime()));
    }
}