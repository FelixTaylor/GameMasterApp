package felixtaylor.gamemaster.activities;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import felixtaylor.gamemaster.actions.DeleteGroupActivity;
import felixtaylor.gamemaster.R;
import felixtaylor.gamemaster.StaticValues;
import felixtaylor.gamemaster.database.DBConstants;
import felixtaylor.gamemaster.database.DBHandler;
import felixtaylor.gamemaster.figure.Figure;
import felixtaylor.gamemaster.system.Group;
import felixtaylor.gamemaster.system.User;

public class GroupActivity extends AppCompatActivity implements TextWatcher, View.OnClickListener,
    View.OnLongClickListener {
    private static final String LOGTAG = GroupActivity.class.getSimpleName();
    private ArrayList<Group> groups;

    private LayoutInflater inflater;
    private Resources resources;
    private User      user;
    private DBHandler handler;

    private Dialog dialog_user_name;
    private Dialog dialog_changelog;
    private Dialog dialog_group_name;
    private Dialog dialog_delete_figure;

    private EditText dialog_group_input;
    private EditText dialog_user_input;
    private EditText searchbar_input;

    private ImageView search_cancel_button;

    private class OnGroupClickListener implements View.OnClickListener {
        @Override public void onClick(final View view) {
            Group group = selectGroupFromDatabase(view.getId());
            Log.d(LOGTAG, "Open Group " + group.getName());

            startActivity(new Intent(
                    getApplicationContext(),
                    ListActivity.class)

                    .putExtra(StaticValues.INTENT_GROUP, group)
                    .putExtra(StaticValues.INTENT_USER, user));
        }
    }
    private class DialogUsernamePositiveListener implements View.OnClickListener {
        @Override public void onClick(View v) {
            String username = dialog_user_input.getText().toString().trim();

            if (!username.isEmpty()) {
                user.setName(username);
                user.setInitialUseTime(System.currentTimeMillis());

                handler.open();
                handler.update(user);
                handler.close();
                dialog_user_name.dismiss();
                dialog_user_name = null;
            }
        }
    }

    public void onResume() {
        super.onResume();
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (searchbar_input != null) {
            searchbar_input.setText("");
        }
    }
    public void onPause() {
        super.onPause();
        handler.close();
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);

        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView backButton = toolbar.findViewById(R.id.back_button);
        backButton.setVisibility(View.GONE);

        setSupportActionBar(toolbar);

        this.resources = getResources();
        this.groups = new ArrayList<>();

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        handler  = new DBHandler(GroupActivity.this);
        handler.open();
        this.user = handler.getUser();

        /*  If the app is installed sho a welcome message
            and ask for the username to be entered. Then
            close the dialog and don't show it again. */

        if (this.user.getShowUserNamePopUp() == 0) {
            this.user.setShowUserNamePopUp(1);
            handler.update(this.user);
            showUsernameDialog();
        }

        if (this.user.getInitialUseTime() == 0) {
            this.user.setInitialUseTime(System.currentTimeMillis());
            handler.update(this.user);
        }

        handler.close();

        /*  Set the the language to the language
            preferred by the user.  */

        resources.getConfiguration().locale = new Locale(user.getLanguage());
        resources.updateConfiguration(resources.getConfiguration(), resources.getDisplayMetrics());

        /* Set a clickListener on the 'Action Button' */
        findViewById(R.id.action_button).setOnClickListener(GroupActivity.this);
        initializeSearchbar();
    }
    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_group, menu);
        return true;
    }
    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_changelog:
                Log.d(LOGTAG, "MenuItem clicked: ACTION_CHANGELOG");
                showChangelogDialog();
                break;

            case R.id.action_setting:
                Log.d(LOGTAG, "MenuItem clicked: ACTION_SETTING");
                startActivity(
                        new Intent(getApplicationContext(),
                                SettingsActivity.class)
                );
                break;
        }
        return true;
    }

    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
    @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
        search_cancel_button.setAlpha(1F);
    }
    @Override public void afterTextChanged(Editable s) {
        if (s.toString().trim().isEmpty()) {

            Log.d(LOGTAG, "Searching Groups for: '" + s.toString().trim() + "'");
            search_cancel_button.setAlpha(0.2F);

            /*  TODO: Rework SQLite statements
                I want to save them as static variables or in the
                DBConstants class. Or could i write them with
                placeholders to the string.xml file? */

            displayGroups(
                    DBConstants.SELECT_ALL_FROM_GROUP +
                            " ORDER BY " + DBConstants.GROUP_NAME);

        } else {
            displayGroups(
                    DBConstants.SELECT_ALL_FROM_GROUP + " WHERE "
                            + DBConstants.GROUP_NAME + " LIKE \'%" + s + "%\' ORDER BY "
                            + DBConstants.GROUP_NAME);
        }
    }
    @Override public void onClick(View view) {
        switch (view.getId()) {

            /*  TODO: How can I change the ID's
                I want to be able to handle all button clicks
                in this method. But I have the same Button in
                different layout files so I can't select them
                as planed. */

            case R.id.action_button:
                Log.d(LOGTAG, "OnClick: ACTION_SHOW_NEW_GROUP_DIALOG");
                showNewGroupDialog();
                break;

            case R.id.dialog_positive_button:
                Log.d(LOGTAG, "OnClick: ACTION_CREATE_NEW_GROUP");
                createNewGroup();
                break;

            case R.id.cancel_search:
                Log.d(LOGTAG, "OnClick: ACTION_CANCEL_SEARCH");
                cancelSearch();
                break;

            case R.id.dialog_cancel_button:
                Log.d(LOGTAG, "OnClick: ACTION_GROUP_NAME_CANCEL");
                dialog_group_name.cancel();
                break;

            case R.id.btn_help_dismiss:
                Log.d(LOGTAG, "OnClick: ACTION_CHANGELOG_DISMISS");
                dialog_changelog.dismiss();
                break;
        }
    }
    @Override public boolean onLongClick(final View view) {
        final Group selectedGroup = selectGroupFromDatabase(view.getId());

        if (dialog_delete_figure == null) {
            dialog_delete_figure = new Dialog(GroupActivity.this);
            dialog_delete_figure.setContentView(R.layout.dialog_warning);
            dialog_delete_figure.setTitle(resources.getString(R.string.dialog_title_warning));

            Button negative = dialog_delete_figure.findViewById(R.id.dialog_dismiss);
            negative.setText(R.string.action_no);
            negative.setTextColor(resources.getColor(R.color.colorSecondaryVeryDark));
            negative.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    dialog_delete_figure.cancel();
                }
            });



            Button positive = dialog_delete_figure.findViewById(R.id.dialog_positive);
            positive.setText(R.string.action_yes);
            positive.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    handler.open();
                    ArrayList<Figure> figuresOfGroup =
                            handler.selectAllFigures(
                                    DBConstants.SELECT_ALL +
                                    " WHERE " + DBConstants.FIGURE_GROUP_ID +
                                    " = " + selectedGroup.getId());
                    handler.close();

                    boolean doNotDeleteGroup = false;
                    for (Figure f : figuresOfGroup) {
                        if (f.isFavourite()) {
                            doNotDeleteGroup = true;
                            break;
                        }
                    }

                    if (doNotDeleteGroup) {
                        Toast.makeText(
                                getApplicationContext(),
                                R.string.message_cannot_delete_group,
                                Toast.LENGTH_LONG
                        ).show();
                    } else {
                        startActivity(new Intent(getApplicationContext(), DeleteGroupActivity.class)
                                .putExtra(StaticValues.INTENT_GROUP, selectedGroup)
                                .putExtra(StaticValues.INTENT_FIGURES_OF_GROUP, figuresOfGroup));

                    }
                    dialog_delete_figure.cancel();
                }
            });
        }

        TextView title = dialog_delete_figure.findViewById(R.id.dialog_message);
        title.setText(resources.getString(R.string.message_delete, selectedGroup.getName()));
        dialog_delete_figure.show();

        return true;
    }

    // ACTIONS
    private void createNewGroup() {
        String groupName = dialog_group_input.getText().toString().trim();
        long timeInMill = System.currentTimeMillis();
        if (!groupName.isEmpty()) {

            handler.open();

            // TODO: OVERTHINK THIS PART OF CODE!

            long id = handler.selectMaxGroupId();
            Group group = new Group(id, timeInMill, groupName, "");

            // Add next group to counter
            user.addTotalCreateGroup();
            handler.update(user);

            handler.insert(group);
            handler.close();

            startActivity(
                    new Intent(getApplicationContext(),
                            ListActivity.class)
                            .putExtra(StaticValues.INTENT_USER, user)
                            .putExtra(StaticValues.INTENT_GROUP, group)
            );

            Log.d(LOGTAG, "Create new group (" + group.getId() + "," + group.getName() + ")");
            dialog_group_name.dismiss();
        }
    }
    private void cancelSearch() {
        if (!searchbar_input.getText().toString().trim().isEmpty()) {
            searchbar_input.setText("");

           // hideUi();
            View view = getCurrentFocus();
            if (view != null) {

            /*  Hide the Keyboard if the user cancels the search process. */
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    // DIALOGS
    private void showNewGroupDialog() {
        if (dialog_group_name == null) {

            dialog_group_name = new Dialog(GroupActivity.this);
            dialog_group_name.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
            dialog_group_name.setContentView(R.layout.dialog_input);

            TextView title = dialog_group_name.findViewById(R.id.dialog_info_title);

            if (deviceUsesApi21()) {
                title.setText(resources.getString(R.string.dialog_title_group));

            } else {

                title.setVisibility(View.GONE);
                dialog_group_name.setTitle(resources.getString(R.string.dialog_title_group));
            }

            dialog_group_input = dialog_group_name.findViewById(R.id.dialog_input_field);
            dialog_group_input.setHint(R.string.tv_name);

            TextView message = dialog_group_name.findViewById(R.id.dialog_info_text);
            message.setText(resources.getString(R.string.dialog_group_info));

            Button positive = dialog_group_name.findViewById(R.id.dialog_positive_button);
            positive.setText(resources.getString(R.string.action_create_group));
            positive.setOnClickListener(GroupActivity.this);

            Button negative = dialog_group_name.findViewById(R.id.dialog_cancel_button);
            negative.setOnClickListener(GroupActivity.this);
        }

        dialog_group_input.setText("");
        dialog_group_name.show();
    }
    private void showUsernameDialog() {
        dialog_user_name = new Dialog(GroupActivity.this);
        dialog_user_name.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
        dialog_user_name.setContentView(R.layout.dialog_input);

        TextView title = dialog_user_name.findViewById(R.id.dialog_info_title);
        if (deviceUsesApi21()) {
            title.setText(resources.getString(R.string.tv_username));
        } else {
            dialog_user_name.setTitle(resources.getString(R.string.tv_username));
            title.setVisibility(View.GONE);
        }

        dialog_user_input = dialog_user_name.findViewById(R.id.dialog_input_field);
        dialog_user_input.setHint(R.string.tv_name);

        TextView message = dialog_user_name.findViewById(R.id.dialog_info_text);
        message.setText(resources.getString(R.string.dialog_info_username));

        Button positive = dialog_user_name.findViewById(R.id.dialog_positive_button);
        positive.setOnClickListener(new DialogUsernamePositiveListener());
        positive.setText(resources.getString(R.string.action_save_figure));

        (dialog_user_name.findViewById(R.id.dialog_cancel_button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override public void onClick(View v) {
                        dialog_user_name.cancel();
                        dialog_user_name = null;
                    }
                });

        dialog_user_name.show();
    }
    private void showChangelogDialog() {
        if (dialog_changelog == null) {

            dialog_changelog = new Dialog(GroupActivity.this);
            dialog_changelog.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
            dialog_changelog.setContentView(R.layout.dialog_simple_table);
            TextView title = dialog_changelog.findViewById(R.id.dialog_title);

            if (!deviceUsesApi21()) {
                title.setVisibility(View.GONE);
                dialog_changelog.setTitle(resources.getString(
                        R.string.dialog_title_changelog,
                        resources.getString(R.string.tv_version)));

            } else {
                title.setText(resources.getString(
                        R.string.dialog_title_changelog,
                        resources.getString(R.string.tv_version)));
            }

            TableLayout table = dialog_changelog.findViewById(R.id.dialog_table);
            String[] titles = resources.getStringArray(R.array.array_titles_changelog);

            ArrayList<String[]> notes = new ArrayList<>();
            notes.add(resources.getStringArray(R.array.changelog_functions));
            notes.add(resources.getStringArray(R.array.changelog_bugfix));
            notes.add(resources.getStringArray(R.array.changelog_improvements));
            notes.add(resources.getStringArray(R.array.changelog_design));
            notes.add(resources.getStringArray(R.array.changelog_removed));

            for (int i=0; i<titles.length; i++) {
                if (notes.get(i).length > 0) {
                    showUpdates(table, titles[i], notes.get(i));
                }
            }

            dialog_changelog.findViewById(R.id.btn_help_dismiss).setOnClickListener(GroupActivity.this);
        }

        dialog_changelog.show();
    }

    // MISC
    private boolean deviceUsesApi21() {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT;
    }
    private void initializeSearchbar() {
        this.searchbar_input = findViewById(R.id.searchbar);
        this.searchbar_input.setHint(
                resources.getString(R.string.tv_search,
                resources.getString(R.string.tv_groups)));
        this.searchbar_input.addTextChangedListener(GroupActivity.this);
        this.search_cancel_button = findViewById(R.id.cancel_search);
        this.search_cancel_button.setOnClickListener(GroupActivity.this);
    }
    private void showUpdates(TableLayout table, String title, String[] notes) {
        TextView changeLog_title = (TextView) inflater.inflate(R.layout.item_list_title, null);

        changeLog_title.setText(title);
        table.addView(changeLog_title);

        for (String s : notes) {
            View row = inflater.inflate(R.layout.item_row_changlog, null);
            ((TextView) row.findViewById(R.id.update_text)).setText(s);
            table.addView(row);
        }

    }
    private void displayGroups(String statement) {

        Log.d(LOGTAG, "Display Groups.");

        int color = resources.getColor(R.color.colorPrimaryDarkTable);
        OnGroupClickListener groupClickListener = new OnGroupClickListener();
        TableLayout groupTable = findViewById(R.id.table);
        groupTable.removeAllViews();

        /*  Get the amount of figures
            in each group so the value
            can be displayed. */

        handler.open();
        groups = handler.selectAllGroups(statement);


        /* Displaying the each row. */
        for (int i=0; i<groups.size(); i++) {

            View row = inflater.inflate(R.layout.item_row_group, null);
            ((TextView) row.findViewById(R.id.tv_group_name))             .setText(groups.get(i).getName());
            ((TextView) row.findViewById(R.id.group_settings_description)).setText(groups.get(i).getDescription());
            ((TextView) row.findViewById(R.id.tv_group_date))             .setText(dateToString(groups.get(i)));

            ((TextView) row.findViewById(R.id.tv_group_amount_figures))
                    .setText(resources.getString(
                            R.string.tv_figures_in_group,
                            handler.countFiguresInGroup(groups.get(i).getId()))
                    );

            // TODO: Can I avoid to make a long a string and then an int?
            //row.setId(Integer.valueOf(groups.get(i).getId().toString()));
            row.setId(Integer.valueOf(Long.toString(groups.get(i).getId())));
            row.setOnClickListener(groupClickListener);
            row.setOnLongClickListener(GroupActivity.this);

            if (i%2==0) { row.setBackgroundColor(color); }
            groupTable.addView(row);
        }
        handler.close();
        TextView total = findViewById(R.id.tv_total);
        total.setText(showGroupInfo(groups.size(), searchbar_input.getText().toString().trim()));
    }
    private String showGroupInfo(int groupSize, String searchbarText) {
        /*  Only show the welcome text if no figures are in this
            group AND there is no search input from the user. */

        if (groupSize == 0 && searchbarText.trim().isEmpty()) {
            Log.d(LOGTAG, "showGroupInfo=true");
            return resources.getString(R.string.tv_welcome_group);
        } else {
            return resources.getString(R.string.tv_footer_total_groups, groups.size());
        }
    }
    private String dateToString(Group group) {
        return new SimpleDateFormat(StaticValues.DATE,
                Locale.getDefault()).format(new Date(group.getCreationDateInMillis()));
    }
    private Group selectGroupFromDatabase(long id) {
        Log.d(LOGTAG, "selectGroupFromDatabase: Find group with id=" + id);
        handler.open();
        Group group = handler.selectGroup(DBConstants.SELECT_GROUP_WITH_ID + id);
        handler.close();

        return group;
    }
}