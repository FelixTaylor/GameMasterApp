package felixtaylor.gamemaster.activities;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;

import felixtaylor.gamemaster.actions.AddMultipleFiguresActivity;
import felixtaylor.gamemaster.actions.DeleteFigureActivity;
import felixtaylor.gamemaster.actions.DeleteGroupActivity;
import felixtaylor.gamemaster.actions.MoveFiguresToGroupActivity;
import felixtaylor.gamemaster.R;
import felixtaylor.gamemaster.StaticValues;
import felixtaylor.gamemaster.database.DBConstants;
import felixtaylor.gamemaster.database.DBHandler;
import felixtaylor.gamemaster.figure.AverageFigure;
import felixtaylor.gamemaster.figure.Figure;
import felixtaylor.gamemaster.system.Group;
import felixtaylor.gamemaster.system.User;

public class ListActivity extends AppCompatActivity implements TextWatcher, View.OnClickListener, AdapterView.OnLongClickListener {
    private static final String LOGTAG = ListActivity.class.getSimpleName();
    private static final DecimalFormat DECFORMAT = new DecimalFormat("##.##");

    private ArrayList<Figure> allFiguresFromGroup;
    private ArrayList<Figure> selectedFiguresForAction;
    private ArrayList<String> randomAvailableRaces;
    private ArrayList<String> randomAvailableClasses;
    private boolean           userIsSelectingFigures;

    private LayoutInflater inflater;
    private StringBuffer   stringBuffer;

    private Resources resources;
    private DBHandler handler;
    private User      user;
    private Group     group;
    private Figure    temp;

    private FigureClickListener figureClickListener;
    private UndoListener undoListener;

    private Dialog dialog_database_info;
    private Dialog dialog_group_setting;
    private Dialog dialog_add_figure;
    private Dialog dialog_delete_figure;
    private Dialog dialog_move_figures;

    private TextView newGroupName;
    private EditText addFigureAmount;
    private EditText addFigureLevelMin;
    private EditText addFigureLevelMax;
    private EditText searchbar_input;
    private EditText groupDescription;

    private ImageView search_cancel_button;
    private TableLayout figureTable;

    private class AddFigureCheckRace implements CompoundButton.OnCheckedChangeListener {
        @Override public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            String buttonText = getEnglishRaceName(compoundButton.getText().toString());
            Log.d(LOGTAG,"AddFigureCheckRace @param=" +buttonText);

            if (b) {
                randomAvailableRaces.add(buttonText);
            } else {

                for (int i = 0; i<randomAvailableRaces.size(); i++) {
                    if (randomAvailableRaces.get(i).equals(buttonText)) {
                        randomAvailableRaces.remove(i);
                        break;
                    }
                }
            }
        }
    }
    private class AddFigureCheckClass implements CompoundButton.OnCheckedChangeListener {
        @Override public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            String buttonText = getEnglishClassName(compoundButton.getText().toString());
            Log.d(LOGTAG,"AddFigureCheckClass @param=" +buttonText);
            if (b) {
                randomAvailableClasses.add(buttonText);
            } else {

                for (int i=0; i<randomAvailableClasses.size(); i++) {
                    if (randomAvailableClasses.get(i).equals(buttonText)) {
                        randomAvailableClasses.remove(i);
                        break;
                    }
                }
            }
        }
    }
    private class FigureClickListener implements View.OnClickListener {
        @Override public void onClick(View view) {
            if (userIsSelectingFigures) {
                selectFigureForAction(view);

            } else {
                handler.open();
                startDetailActivity(handler.selectFigure(view.getId()));
                handler.close();
            }
        }
    }
    private class UndoListener implements View.OnClickListener {
        @Override public void onClick(View v) {

            handler.open();
            handler.insert(temp);
            handler.close();
            allFiguresFromGroup = selectFiguresFromDatabase(
                    DBConstants.SELECT_FIGURE_WITH_GUID + group.getId()
                            + " " + user.getOrderBy());
            displayRows();

            Toast.makeText(getApplicationContext(),
                    resources.getString(R.string.message_was_reinserted,temp.getName()),
                    Toast.LENGTH_SHORT).show();

            temp = null;
        }
    }

    public void onResume() {
        super.onResume();
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (searchbar_input != null) {
            searchbar_input.setText("");
        }
    }
    public void onPause() {
        super.onPause();
        handler.close();
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Log.d(LOGTAG, "OnCreate()");
        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView backButton = toolbar.findViewById(R.id.back_button);
        backButton.setOnClickListener(ListActivity.this);
        setSupportActionBar(toolbar);

        findViewById(R.id.action_button).setOnClickListener(ListActivity.this);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        stringBuffer             = new StringBuffer();
        allFiguresFromGroup      = new ArrayList<>();
        selectedFiguresForAction = new ArrayList<>();
        temp = null;

        this.figureClickListener = new FigureClickListener();
        this.undoListener        = new UndoListener();
        handler = new DBHandler(this);

        if (resources == null) {
            resources = getResources();
        }

        Intent intent = getIntent();
        if(intent != null) {
            this.user = (User) intent.getSerializableExtra(StaticValues.INTENT_USER);
            this.group = (Group) intent.getSerializableExtra(StaticValues.INTENT_GROUP);
        }

        initializeSearchbar();
        Log.d(LOGTAG, "END OnCreate()");
    }
    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }
    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            /*case R.id.action_about:
                Log.d(LOGTAG, "MenuItem onClick: ACTION_ABOUT");
                showDatabaseInformation();
                break;
*/
            case R.id.action_group_settings:
                Log.d(LOGTAG, "MenuItem onClick: ACTION_GROUP_SETTINGS");
                showGroupSettingDialog();
                break;

            case R.id.action_group_sort_by:
                Log.d(LOGTAG, "MenuItem onClick: ACTION_GROUP_SORT_LIST");
                showSortListDialog();
                break;

            case R.id.action_group_add_figures:
                Log.d(LOGTAG, "MenuItem onClick: ACTION_ADD_FIGURE");
                showAddFiguresDialog();
                break;

        }
        return true;
    }

    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
    @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
        search_cancel_button.setAlpha(1F);
    }
    @Override public void afterTextChanged(Editable s) {
        if (s.toString().trim().isEmpty()) {
            Log.d(LOGTAG, "Searching Groups for: '" + s.toString().trim() + "'");

            search_cancel_button.setAlpha(0.2F);
            allFiguresFromGroup = selectFiguresFromDatabase(
                    DBConstants.SELECT_FIGURE_WITH_GUID + group.getId()
                            + " " + user.getOrderBy());

        } else {

            allFiguresFromGroup = selectFiguresFromDatabase(
                    DBConstants.SELECT_ALL + " WHERE " + DBConstants.FIGURE_GROUP_ID
                            + " = " + group.getId()  + " AND " + DBConstants.FIGURE_NAME
                            + " LIKE \'%" + s + "%\' "

                            + " OR " + DBConstants.FIGURE_GROUP_ID + " = " + group.getId()
                            + " AND " + DBConstants.FIGURE_RACE + " LIKE \'%" + s + "%\' "

                            + " OR " + DBConstants.FIGURE_GROUP_ID + " = " + group.getId()
                            + " AND " + DBConstants.FIGURE_CLASS + " LIKE \'%" + s + "%\' "

                            + user.getOrderBy());
        }
        displayRows();
    }
    @Override public void onClick(View view) {
        switch (view.getId()) {
            case R.id.action_button:
                Log.d(LOGTAG, "OnClick: ACTION_CREATE_FIRST_FIGURE_IN_GROUP");
                if (groupCanHoldMoreFigures()) {
                    startActivity(new Intent(getApplicationContext(), DetailActivity.class)
                            .putExtra(StaticValues.INTENT_GROUP, group)
                            .putExtra(StaticValues.INTENT_USER, user)
                            .putExtra(StaticValues.INTENT_IS_EXISTING, false));

                } else {
                    Toast.makeText(getApplicationContext(),
                            resources.getString(R.string.message_group_has_max_figures),
                            Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.cancel_search:
                Log.d(LOGTAG, "OnClick: ACTION_CANCEL_SEARCH");
                cancelSearch();
                break;

            case R.id.btn_group_settings_dismiss:
                Log.d(LOGTAG, "OnClick: ACTION_UPDATE_GROUP");
                updateGroup();
                break;

            case R.id.btn_group_settings_delete:
                Log.d(LOGTAG, "OnClick: ACTION_DELETE_GROUP");
                dialog_group_setting.dismiss();
                showDeleteGroupDialog();
                break;

            case R.id.btn_group_settings_cancel:
                Log.d(LOGTAG, "OnClick: ACTION_DISMISS_GROUP_SETTING");
                dialog_group_setting.dismiss();
                break;

            case R.id.dialog_add_figures_positive:
                Log.d(LOGTAG, "OnClick: ACTION_ADD_FIGURE_TO_GROUP");
                addFiguresToGroup();
                break;

            case R.id.dialog_add_figures_negative:
                Log.d(LOGTAG, "OnClick: ACTION_DISMISS_ADD_FIGURE");
                dialog_add_figure.dismiss();
                break;

            case R.id.back_button:
                onBackPressed();
                break;
        }
    }
    @Override public boolean onLongClick (final View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ListActivity.this);

        handler.open();
        final Figure figure = handler.selectFigure(view.getId());
        handler.close();

        CharSequence menuItemText;
        if (figure.isFavourite()) { menuItemText = resources.getString(R.string.action_remove_favourite); }
        else { menuItemText = resources.getString(R.string.action_add_favourite); }

        if (userIsSelectingFigures) {

            builder.setTitle(resources.getString(R.string.dialog_title_selecting));
            builder.setItems(
                    new CharSequence[] {resources.getString(R.string.action_cancel), resources.getString(R.string.action_move), menuItemText, resources.getString(R.string.action_delete)},
                    new DialogInterface.OnClickListener() {
                        @Override public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    userIsSelectingFigures = false;
                                    onResume();
                                    break;

                                case 1: showMoveToGroupDialog(selectedFiguresForAction); break;
                                case 2: {
                                    for (Figure f : selectedFiguresForAction) {
                                        removeFigureFromFavourite(f);
                                    }

                                    userIsSelectingFigures = false;
                                    selectedFiguresForAction.clear();
                                    for (int i=0; i<figureTable.getChildCount(); i++) {
                                        figureTable.getChildAt(i).setAlpha(1f);
                                    }
                                    break;
                                }


                                case 3: {
                                    showDialogDeleteFigure();
                                    break;
                                }

                            }
                        }
                    }).show();

        } else {
            builder.setTitle(figure.getName());
            builder.setItems(
                    new CharSequence[] {resources.getString(R.string.action_select), resources.getString(R.string.action_move), menuItemText, resources.getString(R.string.action_delete)},
                    new DialogInterface.OnClickListener() {
                        @Override public void onClick(DialogInterface dialog, int which) {
                            switch (which) {

                                case 0: selectFigureForAction(view);
                                    for (int i=0; i<figureTable.getChildCount(); i++) {
                                        figureTable.getChildAt(i).setAlpha(.2f);
                                    }
                                    view.setAlpha(1F);
                                    break;

                                case 1:
                                    ArrayList<Figure> selectedFigure = new ArrayList<>();
                                    selectedFigure.add(figure);
                                    showMoveToGroupDialog(selectedFigure);
                                    break;

                                case 2: removeFigureFromFavourite(figure); break;

                                case 3:
                                    selectedFiguresForAction.clear();
                                    selectedFiguresForAction.add(figure);
                                    showDialogDeleteFigure();
                                    break;
                            }
                        }
                    }).show();
        }

        return true;
    }
    private void showDialogDeleteFigure() {
        final Dialog deleteFigureDialog = new Dialog(ListActivity.this);

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            deleteFigureDialog.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
            deleteFigureDialog.setTitle(resources.getString(R.string.dialog_title_warning));
        }

        deleteFigureDialog.setContentView(R.layout.dialog_warning);
        TextView title = deleteFigureDialog.findViewById(R.id.dialog_message);
        title.setText(resources.getQuantityString(
                R.plurals.message_delete_this_figure,
                selectedFiguresForAction.size()));

        Button positive = deleteFigureDialog.findViewById(R.id.dialog_positive);
        positive.setText(R.string.action_yes);
        positive.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

                deleteFigureDialog.cancel();
                startActivity(new Intent(getApplicationContext(), DeleteFigureActivity.class)
                        .putExtra(StaticValues.INTENT_FIGURES_TO_DELETE,selectedFiguresForAction)
                        .putExtra(StaticValues.INTENT_GROUP, group)
                        .putExtra(StaticValues.INTENT_USER, user));

                userIsSelectingFigures = false;
                selectedFiguresForAction.clear();
                for (int i=0; i<figureTable.getChildCount(); i++) {
                    figureTable.getChildAt(i).setAlpha(1f);
                }
            }
        });


        Button negative = deleteFigureDialog.findViewById(R.id.dialog_dismiss);
        negative.setText(R.string.action_no);
        negative.setTextColor(resources.getColor(R.color.colorSecondaryVeryDark));
        negative.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                deleteFigureDialog.cancel();

                userIsSelectingFigures = false;
                selectedFiguresForAction.clear();
                for (int i=0; i<figureTable.getChildCount(); i++) {
                    figureTable.getChildAt(i).setAlpha(1f);
                }
            }
        });

        deleteFigureDialog.show();
    }

    @NonNull private String switchRaceLanguage(String race) {

        /*  TODO: This should not happen
            User should never by null! */
/*
        if (this.user == null) {
            handler.open();
            this.user = handler.getUser();
            handler.close();
        }*/

        if (user.getLanguage().equals(StaticValues.LANGUAGE_EN)) {
            return race;
        }  else {

            Log.d(LOGTAG, "switchRaceLanguage(" + race + ")");
            switch (race.toLowerCase()) {
                case "dwarf":    return resources.getString(R.string.dwarf);
                case "elf":      return resources.getString(R.string.elf);
                case "gnome":    return resources.getString(R.string.gnome);
                case "halfelf":  return resources.getString(R.string.halfelf);
                case "halfling": return resources.getString(R.string.halfling);
                case "halfork":  return resources.getString(R.string.halfork);
                default:         return resources.getString(R.string.human);
            }
        }
    }
    @NonNull private String switchClassLanguage(String classname) {
        if (user.getLanguage().equals(StaticValues.LANGUAGE_EN)) {
            return classname;
        } else {

            Log.d(LOGTAG, "switchClassLanguage(" + classname + ")");
            switch (classname.toLowerCase()) {
                case "barbarian":   return resources.getString(R.string.barbarian);
                case "bard":        return resources.getString(R.string.bard);
                case "cleric":      return resources.getString(R.string.cleric);
                case "druid":       return resources.getString(R.string.druid);
                case "fighter":     return resources.getString(R.string.fighter);
                case "mage":        return resources.getString(R.string.mage);
                case "monk":        return resources.getString(R.string.monk);
                case "paladin":     return resources.getString(R.string.paladin);
                case "ranger":      return resources.getString(R.string.ranger);
                case "rogue":       return resources.getString(R.string.rogue);
                default:            return resources.getString(R.string.warlock);
            }
        }
    }
    @NonNull private String makeString(String[] inputs) {
        if(this.stringBuffer.length() != 0) {
            this.stringBuffer.setLength(0);
        }
        for (String s : inputs) {
            this.stringBuffer.append(s);
        }
        return this.stringBuffer.toString();
    }

    // ACTIONS
    private ArrayList<Figure> selectFiguresFromDatabase(String statement) {
        Log.d(LOGTAG, "selectFiguresFromDatabase(): STATEMENT = " + statement);

        handler.open();
        ArrayList<Figure> figureList = handler.selectAllFigures(statement);
        handler.close();
        return figureList;
    }
    private String showFigureInfo(int figureSize, String searchbarText) {
        /*  Only show the welcome text if no figures are in this
            group AND there is no search input from the user. */

        if (figureSize == 0 && searchbarText.trim().isEmpty()) {
            Log.d(LOGTAG, "showGroupInfo=true");
            return resources.getString(R.string.tv_welcome_list, group.getName());
        } else {
            return resources.getString(R.string.tv_footer_total_figures, allFiguresFromGroup.size());
        }
    }
    private void displayRows() {
        int color = resources.getColor(R.color.colorPrimaryDarkTable);
        figureTable = findViewById(R.id.table);
        figureTable.removeAllViewsInLayout();

        for (int i=0; i<allFiguresFromGroup.size(); i++) {
            View row = inflater.inflate(R.layout.item_row_list, null);
            Figure figure = allFiguresFromGroup.get(i);

            TextView tribe = row.findViewById(R.id.row_figure_tribe);
            tribe.setText(resources.getString(
                    R.string.tv_list_race_class,
                    switchRaceLanguage(figure.getRace()),
                    switchClassLanguage(figure.getFigureClass())));

            ((TextView) row.findViewById(R.id.row_figure_name)).setText(String.valueOf(figure.getName()));
            ((TextView) row.findViewById(R.id.row_figure_date)).setText(figure.dateToString(StaticValues.DATE));
            ((TextView) row.findViewById(R.id.row_figure_level)).setText(
                    makeString(new String[]{resources.getString(R.string.tv_level), ": ",
                            Integer.toString(figure.getLevel())}));

            if (figure.isFavourite()) {
                ImageView heart = row.findViewById(R.id.star_fav);
                heart.setImageDrawable(resources.getDrawable(R.mipmap.ic_favorite_white));
            }

            if (i%2 == 0) { row.setBackgroundColor(color); }

            row.setOnClickListener(figureClickListener);
            row.setOnLongClickListener(ListActivity.this);
            row.setId((int) figure.getId());
            figureTable.addView(row);
        }

        TextView message = findViewById(R.id.tv_total);
        message.setText(showFigureInfo(
                allFiguresFromGroup.size(),
                searchbar_input.getText().toString()));
    }
    private void cancelSearch() {
        searchbar_input.setText("");
        View view = getCurrentFocus();
        if (view != null) {
            /*  Hide the Keyboard if the user cancels the search process. */
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private boolean groupCanHoldMoreFigures() {
        boolean groupCanHoldMoreFigures = true;

        handler.open();
        int totalFiguresInGroup =  handler.countFiguresInGroup(group.getId());
        handler.close();

        if ((totalFiguresInGroup + 1) > StaticValues.MAX_FIGURES_IN_GROUP) {
            groupCanHoldMoreFigures = false;
        }
        Log.d(LOGTAG, "GroupCanHoldMoreFigures=" + groupCanHoldMoreFigures);
        return groupCanHoldMoreFigures;
    }
    private void addFiguresToGroup() {
        String figureAmount = addFigureAmount.getText().toString().trim();
        String levelMin = addFigureLevelMin.getText().toString().trim();
        String levelMax = addFigureLevelMax.getText().toString().trim();

        if (!figureAmount.isEmpty() && !levelMin.isEmpty() && !levelMax.isEmpty() &&
            Integer.valueOf(levelMin) < Integer.valueOf(levelMax) && Integer.valueOf(levelMax) <= StaticValues.MAX_LEVEL) {
            dialog_add_figure.dismiss();

            startActivity(new Intent(getApplicationContext(), AddMultipleFiguresActivity.class)
                    .putExtra(StaticValues.INTENT_GROUP, group)
                    .putExtra(StaticValues.INTENT_AVAILABLE_RACES,   randomAvailableRaces)
                    .putExtra(StaticValues.INTENT_AVAILABLE_CLASSES, randomAvailableClasses)
                    .putExtra(StaticValues.INTENT_FIGURE_AMOUNT, Integer.valueOf(figureAmount))
                    .putExtra(StaticValues.INTENT_LEVEL_MIN,     Integer.valueOf(levelMin))
                    .putExtra(StaticValues.INTENT_LEVEL_MAX,     Integer.valueOf(levelMax))
                    .putExtra(StaticValues.INTENT_USER,          user));
        } else {

            Toast.makeText(getApplicationContext(),
                    resources.getString(R.string.message_invalid_input),
                    Toast.LENGTH_SHORT).show();
        }
    }
    private void updateGroup() {
        String groupName = newGroupName.getText().toString().trim();
        if(!groupName.isEmpty()) {
            boolean savable = false;

            if (!groupName.equals(group.getName())) {
                group.setName(groupName);
                savable = true;
            }

            if (!group.getDescription().trim().toLowerCase()
                    .equals(groupDescription.getText().toString().trim().toLowerCase())) {
                group.setDescription(groupDescription.getText().toString().trim());
                savable = true;
            }

            if (savable) {
                handler.open();
                handler.update(group);
                handler.close();

                Toast.makeText(getApplicationContext(),
                        resources.getString(R.string.message_group_was_updated),
                        Toast.LENGTH_SHORT).show();
            }

            dialog_group_setting.dismiss();
        }
    }
    private void selectFigureForAction(View view) {
        this.userIsSelectingFigures = true;
        boolean figureIsInList = false;

        handler.open();
        Figure figureForAction = handler.selectFigure(view.getId());
        handler.close();

        for (int i=0; i < selectedFiguresForAction.size(); i++) {
            if (selectedFiguresForAction.get(i).equals(figureForAction)) {
                selectedFiguresForAction.remove(i);
                view.setAlpha(.2F);
                figureIsInList = true;
            }
        }

        if (!figureIsInList || selectedFiguresForAction.size()==0) {
            view.setAlpha(1F);
            selectedFiguresForAction.add(figureForAction);
        }
    }
    private void removeFigureFromFavourite(Figure figure) {
        figure.setFavourite(!figure.isFavourite());

        handler.open();
        handler.update(figure);
        handler.close();

        allFiguresFromGroup = selectFiguresFromDatabase(
                DBConstants.SELECT_FIGURE_WITH_GUID + group.getId() +
                          " " + user.getOrderBy());
        displayRows();
    }
    private void deleteFigure(Figure figure) {
        if (figure.isFavourite()) {

            Toast.makeText(
                    getApplicationContext(),
                    resources.getString(R.string.message_cannot_delete),
                    Toast.LENGTH_SHORT
            ).show();

        } else {

            if (!userIsSelectingFigures) {
                Snackbar.make(findViewById(R.id.table),
                        resources.getString(R.string.message_has_been_deleted, figure.getName()), Snackbar.LENGTH_LONG)
                        .setAction(resources.getString(R.string.action_undo), undoListener
                        ).setActionTextColor(resources.getColor(R.color.colorPrimaryLight)
                ).show();
                temp = figure;
            }

            handler.open();
            handler.delete(figure);
            handler.close();

            allFiguresFromGroup = selectFiguresFromDatabase(
                    DBConstants.SELECT_FIGURE_WITH_GUID + group.getId()
                            + " " + user.getOrderBy());
            displayRows();

        }

    }
    private void startDetailActivity(Figure figure) {
        startActivity(
                new Intent(getApplicationContext(),
                        DetailActivity.class)
                        .putExtra(StaticValues.INTENT_GROUP, group)
                        .putExtra(StaticValues.INTENT_FIGURE, figure)
                        .putExtra(StaticValues.INTENT_USER, user)
                        .putExtra(StaticValues.INTENT_IS_EXISTING, true)
        );

    }
    private void updateCalculatedValuesTable(AverageFigure figure, TableLayout table) {
        table.removeAllViewsInLayout();
        String[] tableIdentifiers = resources.getStringArray(R.array.array_tableIdentifiers);
        int textColor = resources.getColor(R.color.colorSecondaryVeryDark);
        int bgcolor = resources.getColor(R.color.lightGreyBackground);
        double[] tableValues = {
                figure.getHitpoints(),
                figure.getArmor(),
                figure.getSkillpoints(),
                figure.getTalentpoints(),
                figure.getSavingThrow(0),
                figure.getSavingThrow(1),
                figure.getSavingThrow(2)
        };

        for (int i=0; i<tableValues.length; i++) {
            View row = inflater.inflate(R.layout.item_row_detail, null);

            TextView identifier = row.findViewById(R.id.identifier);
            identifier.setTextColor(textColor);
            identifier.setText(tableIdentifiers[i]);

            TextView value = row.findViewById(R.id.value);
            value.setTextColor(textColor);
            value.setText(String.valueOf(DECFORMAT.format(tableValues[i])));

            if (i%2 == 0) { row.setBackgroundColor(bgcolor); }
            table.addView(row);
        }
    }
    private void getDatabaseContent() {
        /* TODO: UPDATE the getDatabaseContent Method */

        AverageFigure af = new AverageFigure(allFiguresFromGroup);
        View figureDetailView = dialog_database_info.findViewById(R.id.db_figure_detail);

        TextView allFigures = figureDetailView.findViewById(R.id.db_detail_tv_total_figures);
        allFigures.setText(Integer.toString(allFiguresFromGroup.size()));

        TextView level = figureDetailView.findViewById(R.id.db_level_textview);
        level.setText(makeString(new String[] {
                resources.getString(R.string.tv_level), " ", DECFORMAT.format(af.getLevel())}));

        TextView figureRace = figureDetailView.findViewById(R.id.db_race_textview);
        figureRace.setText(switchRaceLanguage(af.getRace()));

        TextView figureClass = figureDetailView.findViewById(R.id.db_class_textview);
        figureClass.setText(switchClassLanguage(af.getFigureClass()));

        TableLayout db_abilities = figureDetailView.findViewById(R.id.db_detail_abilityTable);
        db_abilities.removeAllViews();
        String[] ability_names = {resources.getString(R.string.tv_strength), resources.getString(R.string.tv_dexterity), resources.getString(R.string.tv_constitution), resources.getString(R.string.tv_intelligence), resources.getString(R.string.tv_wisdom), resources.getString(R.string.tv_charisma)};

        for (int i=0; i<6; i++) {
            View row = inflater.inflate(R.layout.item_row_ability, null);

            TextView identifier = row.findViewById(R.id.abilityIdentifier);
            identifier.setTextColor(resources.getColor(R.color.colorSecondaryVeryDark));
            identifier.setText(ability_names[i]);

            SeekBar seekBar = row.findViewById(R.id.abilitySeekBar);
            seekBar.setMax(af.getMaxAbility());
            seekBar.setProgress((int) af.getAbility(i));
            seekBar.setEnabled(false);

            TextView value = row.findViewById(R.id.abilityValue);
            value.setText(DECFORMAT.format(af.getAbility(i)));
            value.setTextColor(resources.getColor(R.color.colorSecondaryVeryDark));
            value.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);

            (row.findViewById(R.id.abilityValue)).setId(i);
            db_abilities.addView(row);
        }

        updateCalculatedValuesTable(af, (TableLayout) figureDetailView.findViewById(R.id.db_detail_generated_values_table));

        ((TextView) dialog_database_info.findViewById(R.id.bab_base))
                .setText(
                        makeString(new String[]{
                                resources.getString(R.string.tv_bab), " ",
                                DECFORMAT.format(af.getBaseAttackBonus()[3])
                        }));

        ((TextView) dialog_database_info.findViewById(R.id.bab_attacks))
                .setText(DECFORMAT.format(af.getBaseAttackBonus()[2]));

        ((TextView) dialog_database_info.findViewById(R.id.bab_maxValue))
                .setText(makeString(new String[]{
                        DECFORMAT.format(af.getBaseAttackBonus()[0]), " | ",
                        DECFORMAT.format(af.getBaseAttackBonus()[1])
                }));

        ((TextView) dialog_database_info.findViewById(R.id.bab_modifier))
                .setText(Integer.toString(-5));

        (dialog_database_info.findViewById(R.id.btn_help_dismiss))
                .setOnClickListener(new View.OnClickListener() {
                    @Override public void onClick(View v) {
                        dialog_database_info.dismiss();
                    }
                });

    }

    // DIALOGS
    private void showDeleteGroupDialog() {
        if (dialog_delete_figure == null) {
            dialog_delete_figure = new Dialog(ListActivity.this);
            dialog_delete_figure.setContentView(R.layout.dialog_warning);
            dialog_delete_figure.setTitle(resources.getString(R.string.dialog_title_warning));

            Button negative = dialog_delete_figure.findViewById(R.id.dialog_dismiss);
            negative.setText(R.string.action_no);
            negative.setTextColor(resources.getColor(R.color.colorSecondaryVeryDark));
            negative.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    dialog_delete_figure.cancel();
                }
            });

            Button positive = dialog_delete_figure.findViewById(R.id.dialog_positive);
            positive.setText(R.string.action_yes);
            positive.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    handler.open();
                    ArrayList<Figure> figuresOfGroup =
                            handler.selectAllFigures(
                                    DBConstants.SELECT_ALL +
                                            " WHERE " + DBConstants.FIGURE_GROUP_ID +
                                            " = " + group.getId());
                    handler.close();

                    boolean doNotDeleteGroup = false;
                    for (Figure f : figuresOfGroup) {
                        if (f.isFavourite()) {
                            doNotDeleteGroup = true;
                            break;
                        }
                    }

                    if (doNotDeleteGroup) {
                        Toast.makeText(
                                getApplicationContext(),
                                R.string.message_cannot_delete_group,
                                Toast.LENGTH_LONG
                        ).show();
                    } else {
                        startActivity(new Intent(getApplicationContext(), DeleteGroupActivity.class)
                                .putExtra(StaticValues.INTENT_GROUP, group)
                                .putExtra(StaticValues.INTENT_FIGURES_OF_GROUP, figuresOfGroup));
                    }
                    dialog_delete_figure.cancel();
                }
            });
        }

        TextView title = dialog_delete_figure.findViewById(R.id.dialog_message);
        title.setText(resources.getString(R.string.message_delete, group.getName()));
        dialog_delete_figure.show();
    }
    private void showGroupSettingDialog() {
        if (dialog_group_setting == null) {
            dialog_group_setting = new Dialog(ListActivity.this);
            dialog_group_setting.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
            dialog_group_setting.setContentView(R.layout.dialog_group_settings);
            dialog_group_setting.setTitle(R.string.action_group_settings);

            groupDescription = dialog_group_setting.findViewById(R.id.group_settings_description);
            newGroupName = dialog_group_setting.findViewById(R.id.group_settings_name);

            Button positive = dialog_group_setting.findViewById(R.id.btn_group_settings_dismiss);
            positive.setOnClickListener(ListActivity.this);

            Button delete = dialog_group_setting.findViewById(R.id.btn_group_settings_delete);
            delete.setOnClickListener(ListActivity.this);

            Button negative = dialog_group_setting.findViewById(R.id.btn_group_settings_cancel);
            negative.setOnClickListener(ListActivity.this);
        }

        newGroupName.setText(group.getName());
        groupDescription.setText(group.getDescription());
        dialog_group_setting.show();
    }
    private void showAddFiguresDialog() {
        if (dialog_add_figure == null) {
            dialog_add_figure = new Dialog(ListActivity.this);
            dialog_add_figure.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
            dialog_add_figure.setContentView(R.layout.dialog_add_figures);
            dialog_add_figure.setTitle(resources.getString(R.string.action_add_figures));

            addFigureAmount = dialog_add_figure.findViewById(R.id.add_figure_amount);

            TableLayout table = dialog_add_figure.findViewById(R.id.addFigure_table);
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            int color = resources.getColor(R.color.lightGreyBackground);

            addFigureLevelMin = dialog_add_figure.findViewById(R.id.add_figure_level_min);
            addFigureLevelMax = dialog_add_figure.findViewById(R.id.add_figure_level_max);

            randomAvailableRaces = new ArrayList<>();
            randomAvailableClasses = new ArrayList<>();

            AddFigureCheckRace raceListener = new AddFigureCheckRace();
            AddFigureCheckClass classListener = new AddFigureCheckClass();

            for (int i=0; i<11; i++) {
                View row = inflater.inflate(R.layout.item_row_add_figure_include, null);

                CheckBox cb_race = row.findViewById(R.id.checkbox_race);
                CheckBox cb_class = row.findViewById(R.id.checkbox_class);
                cb_class.setText(resources.getStringArray(R.array.array_classes)[i]);
                randomAvailableClasses.add(getEnglishClassName(cb_class.getText().toString()));
                cb_class.setOnCheckedChangeListener(classListener);

                if (i <= 6) {
                    cb_race.setText(resources.getStringArray(R.array.array_races)[i]);
                    randomAvailableRaces.add(getEnglishRaceName(cb_race.getText().toString()));
                    cb_race.setOnCheckedChangeListener(raceListener);
                } else {
                    cb_race.setVisibility(View.INVISIBLE);
                }

                if (i%2==0) { row.setBackgroundColor(color); }
                table.addView(row);
            }

            Button positive = dialog_add_figure.findViewById(R.id.dialog_add_figures_positive);
            positive.setOnClickListener(ListActivity.this);

            Button negative = dialog_add_figure.findViewById(R.id.dialog_add_figures_negative);
            negative.setOnClickListener(ListActivity.this);
        }

        addFigureAmount.setText("");
        dialog_add_figure.show();
    }
    private void showDatabaseInformation() {
        if (dialog_database_info == null) {

            dialog_database_info = new Dialog(this);
            dialog_database_info.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
            dialog_database_info.setContentView(R.layout.dialog_db_info);

            //if (!deviceUsesApi21()) {
            dialog_database_info.setTitle(resources.getString(R.string.dialog_title_group_information));
            //}
        }

        getDatabaseContent();
        dialog_database_info.show();
    }
    public void  showSortListDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ListActivity.this);

        builder.setTitle(resources.getString(R.string.action_list_sort_by));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override public void onDismiss(DialogInterface dialog) {
                onResume();
            }
        });
        builder.setItems(
                new CharSequence[]{
                        resources.getString(R.string.tv_name),
                        resources.getString(R.string.tv_race),
                        resources.getString(R.string.tv_class),
                        resources.getString(R.string.tv_level),
                        resources.getString(R.string.tv_creation_date),
                },

                new DialogInterface.OnClickListener() {
                    @Override public void onClick(DialogInterface dialog, int which) {
                        String toast_placeholder = resources.getString(R.string.tv_name_order);
                        String statement = StaticValues.ORDER_BY_NAME;

                        switch (which) {
                            case 1: {
                                toast_placeholder = resources.getString(R.string.tv_race_order);
                                statement = StaticValues.ORDER_BY_RACE;
                                break;
                            }

                            case 2: {
                                toast_placeholder = resources.getString(R.string.tv_class_order);
                                statement = StaticValues.ORDER_BY_CLASS;
                                break;
                            }

                            case 3: {
                                toast_placeholder = resources.getString(R.string.tv_level_order);
                                statement = StaticValues.ORDER_BY_LEVEL;
                                break;
                            }

                            case 4: {
                                toast_placeholder = resources.getString(R.string.tv_creation_date_order);
                                statement = StaticValues.ORDER_BY_DATE;
                                break;
                            }

                        }

                        user.setOrderBy(statement);
                        handler.open();
                        handler.update(user);
                        handler.close();

                        Toast.makeText(getApplicationContext(),
                                resources.getString(R.string.message_order_by, toast_placeholder),
                                Toast.LENGTH_LONG).show();

                        displayRows();
                    }
                }).show();
    }
    private void showMoveToGroupDialog(final ArrayList<Figure> figures) {
        dialog_move_figures = new Dialog(ListActivity.this);
        dialog_move_figures.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
        dialog_move_figures.setContentView(R.layout.dialog_move_figures);
        dialog_move_figures.setTitle(resources.getString(R.string.action_move));

        TableLayout table = dialog_move_figures.findViewById(R.id.dialog_move_to_group_table);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        handler.open();
        ArrayList<Group> groups = handler.selectAllGroups(DBConstants.SELECT_ALL_FROM_GROUP);


        for (int i=0; i<groups.size(); i++) {
            //if (!groups.get(i).getId().equals(getGroup().getId())) {
            if (!groups.get(i).equals(getGroup())) {
                View row = inflater.inflate(R.layout.item_row_move_to_group, null);

                TextView groupName = row.findViewById(R.id.group_name);
                groupName.setText(groups.get(i).getName());

                TextView groupSize = row.findViewById(R.id.group_size);
                groupSize.setText(resources.getString(
                        R.string.tv_group_figure_amount,
                        handler.countFiguresInGroup(groups.get(i).getId())
                ));

                row.setId(Integer.valueOf(Long.toString(groups.get(i).getId())));
                row.setOnClickListener(new View.OnClickListener() {
                    @Override public void onClick(View view) {

                        dialog_move_figures.dismiss();

                        final Dialog warning = new Dialog(ListActivity.this);
                        warning.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
                        warning.setContentView(R.layout.dialog_warning);
                        warning.setTitle(resources.getString(R.string.dialog_title_warning));

                        handler.open();
                        final Group group = handler.selectGroup(DBConstants.SELECT_GROUP_WITH_ID + view.getId());
                        handler.close();

                        TextView dialog_message = warning.findViewById(R.id.dialog_message);
                        dialog_message.setText(resources.getString(R.string.message_move_to_group,
                                group.getName()));

                        Button dialog_dismiss = warning.findViewById(R.id.dialog_dismiss);
                        dialog_dismiss.setText(R.string.action_cancel);
                        dialog_dismiss.setOnClickListener(new View.OnClickListener() {
                            @Override public void onClick(View view) {
                                warning.dismiss();
                            }
                        });

                        Button dialog_positive = warning.findViewById(R.id.dialog_positive);
                        dialog_positive.setText(R.string.action_move);
                        dialog_positive.setOnClickListener(new View.OnClickListener() {
                            @Override public void onClick(View view) {

                                warning.cancel();
                                startActivity(new Intent(getApplicationContext(), MoveFiguresToGroupActivity.class)
                                        .putExtra(StaticValues.INTENT_GROUP, getGroup())
                                        .putExtra(StaticValues.INTENT_NEW_GROUP_ID, group.getId())
                                        .putExtra(StaticValues.INTENT_USER, user)
                                        .putExtra(StaticValues.INTENT_FIGURES_TO_MOVE, figures));
                            }
                        });
                        warning.show();
                    }
                });
                table.addView(row);
            }
        }
        handler.close();

        Button negative = dialog_move_figures.findViewById(R.id.dialog_negative);
        negative.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                dialog_move_figures.cancel();
            }
        });

        dialog_move_figures.show();
    }

    // VALIDATIONS
    private boolean languageIsEnglish() {
        return user.getLanguage().equals(StaticValues.LANGUAGE_EN);
    }

    // MISC
    private void initializeSearchbar() {
        this.searchbar_input = findViewById(R.id.searchbar);
        this.searchbar_input.setHint(
                resources.getString(R.string.tv_search,
                resources.getString(R.string.tv_figures)));
        this.searchbar_input.addTextChangedListener(ListActivity.this);
        this.search_cancel_button = findViewById(R.id.cancel_search);
        this.search_cancel_button.setOnClickListener(ListActivity.this);
    }
    private String getEnglishRaceName(String race) {

        /*  This method is used to get the english identifier
            of a race so the Tribe class works as expected. */

        if (languageIsEnglish()) {
            return race;
        } else {

            Log.d(LOGTAG, "getEnglishRaceName switch needed: @param=" + race);
            switch (race.toLowerCase()) {
                case "dwarf":   case "zwerg":    return "Dwarf";
                case "elf":                      return "Elf";
                case "gnome":   case "gnom":     return "Gnome";
                case "halfelf": case "halbelf":  return "Halfelf";
                case "halfling":case "halbling": return "Halfling";
                case "halfork": case "halbork":  return "Halfork";
                default:                         return "Human";
            }
        }
    }
    private String getEnglishClassName(String fclass) {

        /*  This method is used to get the english identifier
            of a class so the Tribe class works as expected. */

        if (languageIsEnglish()) {
            return fclass;
        } else {

            Log.d(LOGTAG, "getEnglishClassName switch needed: @param=" + fclass);
            switch (fclass.toLowerCase()) {
                case "barbarian": case "barbar":     return "Barbarian";
                case "bard":      case "barde":      return "Bard";
                case "cleric":    case "kleriker":   return "Cleric";
                case "druid":     case "druide":     return "Druid";
                case "fighter":   case "kämpfer":    return "Fighter";
                case "mage":      case "magier":     return "Mage";
                case "monk":      case "mönch":      return "Monk";
                case "paladin":                      return "Paladin";
                case "ranger":    case "waldläufer": return "Ranger";
                case "rogue":     case "schurke":    return "Rogue";
                default:                             return "Warlock";
            }
        }

    }
    private Group getGroup() {
        return this.group;
    }
}