package felixtaylor.gamemaster;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import felixtaylor.gamemaster.activities.GroupActivity;
public class MainActivity extends AppCompatActivity {
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startActivity(new Intent(this, GroupActivity.class));
        finish();
    }
}