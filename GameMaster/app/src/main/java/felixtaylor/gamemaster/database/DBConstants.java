package felixtaylor.gamemaster.database;
public class DBConstants {
    public static final String DBNAME = "gamemaster.db";
    public static final int DBVERSION = 5;

     /* COLUMN NAMES
        These are column names which are
        used very often. */

    public static final String ID                    = "_id";
    public static final String NAME                  = "_name";


    /* TABLE NAMES
       These are the available
       tables in the application. */

    public static final String USER_TABLE_NAME       = "user";
    public static final String GROUP_TABLE_NAME      = "figureGroup";
    public static final String FIGURE_TABLE_NAME     = "figure";
    public static final String RACE_TABLE_NAME       = "race";
    public static final String CLASS_TABLE_NAME      = "class";
    public static final String ITEM_TABLE_NAME       = "item";
    public static final String SKILL_TABLE_NAME      = "skill";

    // USER TABLE
    public static final String USER_ID               = USER_TABLE_NAME + ID;
    public static final String USER_NAME             = USER_TABLE_NAME + NAME;
    public static final String USER_LANGUAGE         = USER_TABLE_NAME + "_language";
    public static final String USER_NAME_POPUP       = USER_TABLE_NAME + "_popup";
    public static final String USER_LAST_STATEMENT   = USER_TABLE_NAME + "_last_select_statement";
    public static final String USER_TOTAL_FIGURES    = USER_TABLE_NAME + "_total_figures";
    public static final String USER_TOTAL_GROUPS     = USER_TABLE_NAME + "_total_groups";
    public static final String USER_INITIAL_DATE     = USER_TABLE_NAME + "_initial_date";

    // GROUP TABLE
    public static final String GROUP_ID              = GROUP_TABLE_NAME + ID;
    public static final String GROUP_CREATION_TIME   = GROUP_TABLE_NAME + "_creationTime";
    public static final String GROUP_NAME            = GROUP_TABLE_NAME + NAME;
    public static final String GROUP_DESCRIPTION     = GROUP_TABLE_NAME + "_description";

    // FIGURE TABLE
    public static final String FIGURE_ID             = FIGURE_TABLE_NAME + ID;
    public static final String FIGURE_GROUP_ID       = FIGURE_TABLE_NAME + "_groupId";
    public static final String FIGURE_FAVOURITE      = FIGURE_TABLE_NAME + "_fav";
    public static final String FIGURE_NAME           = FIGURE_TABLE_NAME + NAME;
    public static final String FIGURE_RACE           = FIGURE_TABLE_NAME + "_race";
    public static final String FIGURE_CLASS          = FIGURE_TABLE_NAME + "_class";
    public static final String FIGURE_LEVEL          = FIGURE_TABLE_NAME + "_level";
    public static final String FIGURE_STR            = FIGURE_TABLE_NAME + "_str";
    public static final String FIGURE_DEX            = FIGURE_TABLE_NAME + "_dex";
    public static final String FIGURE_CON            = FIGURE_TABLE_NAME + "_con";
    public static final String FIGURE_INT            = FIGURE_TABLE_NAME + "_int";
    public static final String FIGURE_WIS            = FIGURE_TABLE_NAME + "_wis";
    public static final String FIGURE_CHA            = FIGURE_TABLE_NAME + "_cha";
    public static final String FIGURE_HITPOINTS      = FIGURE_TABLE_NAME + "_hitpoints";
    public static final String FIGURE_SKILLS_POINTS  = FIGURE_TABLE_NAME + "_skillpoints";
    public static final String FIGURE_TALENTS_POINTS = FIGURE_TABLE_NAME + "_talentpoints";
    public static final String FIGURE_FORTITUDE      = FIGURE_TABLE_NAME + "_fortitude";
    public static final String FIGURE_REFLEX         = FIGURE_TABLE_NAME + "_reflex";
    public static final String FIGURE_WILL           = FIGURE_TABLE_NAME + "_will";
    public static final String FIGURE_ARMOR          = FIGURE_TABLE_NAME + "_armor";
    public static final String FIGURE_GOLD           = FIGURE_TABLE_NAME + "_gold";
    public static final String FIGURE_GENDER         = FIGURE_TABLE_NAME + "_gender";
    public static final String FIGURE_ALIGNMENT      = FIGURE_TABLE_NAME + "_alignment";
    public static final String FIGURE_ATTITUDE       = FIGURE_TABLE_NAME + "_attitude";
    public static final String FIGURE_LORE           = FIGURE_TABLE_NAME + "_lore";
    public static final String FIGURE_CREATION_DATE  = FIGURE_TABLE_NAME + "_creation_date";
    public static final String FIGURE_OWNER          = FIGURE_TABLE_NAME + "_owner";

    // Skill TABLE
    public static final String SKILL_DB_ID           = SKILL_TABLE_NAME + ID;
    public static final String SKILL_SKILL_ID        = SKILL_TABLE_NAME + "_skill_id";
    public static final String SKILL_FIGURE_ID       = SKILL_TABLE_NAME + "_figure_id";
    public static final String SKILL_RANK            = SKILL_TABLE_NAME + "_rank";
    public static final String SKILL_KEYABILITY      = SKILL_TABLE_NAME + "_key_ability";

    // RACE TABLE
    public static final String RACE_ID               = RACE_TABLE_NAME + ID;
    public static final String RACE_NAME             = RACE_TABLE_NAME + NAME;

    // CLASS TABLE
    public static final String CLASS_ID              = CLASS_TABLE_NAME + ID;
    public static final String CLASS_NAME            = CLASS_TABLE_NAME + NAME;

    // ITEM TABLE
    public static final String ITEM_ID               = ITEM_TABLE_NAME + ID;
    public static final String ITEM_FIGURE_ID        = ITEM_TABLE_NAME + "_figure_id";
    public static final String ITEM_NAME             = ITEM_TABLE_NAME + NAME;
    public static final String ITEM_QUANTITY         = ITEM_TABLE_NAME + "_quantity";
    public static final String ITEM_VALUE            = ITEM_TABLE_NAME + "_value";
    public static final String ITEM_WEIGHT           = ITEM_TABLE_NAME + "_weight";


    // TRIBE INSERTS
    private static final String INSERT_INTO_RACE  = "INSERT INTO " + RACE_TABLE_NAME  + " (" + RACE_NAME  + ") VALUES ";
    private static final String INSERT_INTO_CLASS = "INSERT INTO " + CLASS_TABLE_NAME + " (" + CLASS_NAME + ") VALUES ";

    public static final String INSERT_INTO_RACE_DWARF      = INSERT_INTO_RACE + "('Dwarf')";
    public static final String INSERT_INTO_RACE_ELF        = INSERT_INTO_RACE + "('Elf')";
    public static final String INSERT_INTO_RACE_GNOME      = INSERT_INTO_RACE + "('Gnome')";
    public static final String INSERT_INTO_RACE_HALFELF    = INSERT_INTO_RACE + "('Halfelf')";
    public static final String INSERT_INTO_RACE_HALFLING   = INSERT_INTO_RACE + "('Halfling')";
    public static final String INSERT_INTO_RACE_HALFORK    = INSERT_INTO_RACE + "('Halfork')";
    public static final String INSERT_INTO_RACE_HUMAN      = INSERT_INTO_RACE + "('Human')";
    public static final String INSERT_INTO_CLASS_BARBARIAN = INSERT_INTO_CLASS + "('Barbarian')";
    public static final String INSERT_INTO_CLASS_BARD      = INSERT_INTO_CLASS + "('Bard')";
    public static final String INSERT_INTO_CLASS_CLERIC    = INSERT_INTO_CLASS + "('Cleric')";
    public static final String INSERT_INTO_CLASS_DRUID     = INSERT_INTO_CLASS + "('Druid')";
    public static final String INSERT_INTO_CLASS_FIGHTER   = INSERT_INTO_CLASS + "('Fighter')";
    public static final String INSERT_INTO_CLASS_MAGE      = INSERT_INTO_CLASS + "('Mage')";
    public static final String INSERT_INTO_CLASS_MONK      = INSERT_INTO_CLASS + "('Monk')";
    public static final String INSERT_INTO_CLASS_PALADIN   = INSERT_INTO_CLASS + "('Paladin')";
    public static final String INSERT_INTO_CLASS_RANGER    = INSERT_INTO_CLASS + "('Ranger')";
    public static final String INSERT_INTO_CLASS_ROGUE     = INSERT_INTO_CLASS + "('Rogue')";
    public static final String INSERT_INTO_CLASS_WARLOCK   = INSERT_INTO_CLASS + "('Warlock')";

    // ---------------------------------------------------------------------------------------------
    // SQLite CREATE STATEMENTS

    public static final String SQL_CREATE_SKILL = "CREATE TABLE IF NOT EXISTS " +
            SKILL_TABLE_NAME + "(" +
            SKILL_DB_ID      + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            SKILL_SKILL_ID   + " INTEGER, " +
            SKILL_FIGURE_ID  + " INTEGER, " +
            SKILL_RANK       + " INTEGER, " +
            SKILL_KEYABILITY + ")";

    public static final String SQL_CREATE_USER = "CREATE TABLE IF NOT EXISTS " +
            USER_TABLE_NAME     + "(" +
            USER_ID             + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            USER_NAME           + " TEXT, " +
            USER_LANGUAGE       + " TEXT, " +
            USER_LAST_STATEMENT + " TEXT, " +
            USER_TOTAL_FIGURES  + " INTEGER, " +
            USER_TOTAL_GROUPS   + " INTEGER, " +
            USER_INITIAL_DATE   + " INTEGER, " +
            USER_NAME_POPUP     + " INTEGER DEFAULT 0)";


    public static final String SQL_CREATE_GROUP = "CREATE TABLE IF NOT EXISTS " +
            GROUP_TABLE_NAME    + "(" +
            GROUP_ID            + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            GROUP_NAME          + " TEXT, " +
            GROUP_DESCRIPTION   + " TEXT, " +
            GROUP_CREATION_TIME + " TEXT)";


    public static final String SQL_CREATE_FIGURE = "CREATE TABLE IF NOT EXISTS " +
            FIGURE_TABLE_NAME     + "(" +
            FIGURE_ID             + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            FIGURE_GROUP_ID       + " INTEGER," +
            FIGURE_FAVOURITE      + " INTEGER, " +
            FIGURE_NAME           + " TEXT NOT NULL, " +
            FIGURE_RACE           + " INTEGER NOT NULL, " +  // FOREIGN KEY
            FIGURE_CLASS          + " INTEGER NOT NULL, " +  // FOREIGN KEY
            FIGURE_LEVEL          + " INTEGER NOT NULL, " +
            FIGURE_STR            + " INTEGER, " +
            FIGURE_DEX            + " INTEGER, " +
            FIGURE_CON            + " INTEGER, " +
            FIGURE_INT            + " INTEGER, " +
            FIGURE_WIS            + " INTEGER, " +
            FIGURE_CHA            + " INTEGER, " +
            FIGURE_HITPOINTS      + " INTEGER, " +
            FIGURE_SKILLS_POINTS  + " INTEGER, " +
            FIGURE_TALENTS_POINTS + " INTEGER, " +
            FIGURE_FORTITUDE      + " INTEGER, " +
            FIGURE_REFLEX         + " INTEGER, " +
            FIGURE_WILL           + " INTEGER, " +
            FIGURE_ARMOR          + " INTEGER, " +
            FIGURE_GOLD           + " INTEGER, " +
            FIGURE_GENDER         + " TEXT, " +
            FIGURE_ALIGNMENT      + " INTEGER, " +
            FIGURE_ATTITUDE       + " INTEGER, " +
            FIGURE_LORE           + " TEXT, " +
            FIGURE_OWNER          + " TEXT, " +
            FIGURE_CREATION_DATE  + " TEXT NOT NULL, " +

            "FOREIGN KEY (" + FIGURE_RACE + ")   REFERENCES " + RACE_TABLE_NAME +   "(" + RACE_ID + "), " +
            "FOREIGN KEY (" + FIGURE_CLASS + ")  REFERENCES " + CLASS_TABLE_NAME +  "(" + CLASS_ID + "))";


    public static final String SQL_CREATE_RACE = "CREATE TABLE IF NOT EXISTS " +
            RACE_TABLE_NAME + "(" +
            RACE_ID         + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            RACE_NAME       + " TEXT NOT NULL)";


    public static final String SQL_CREATE_CLASS = "CREATE TABLE IF NOT EXISTS " +
            CLASS_TABLE_NAME + "(" +
            CLASS_ID         + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            CLASS_NAME       + " TEXT NOT NULL)";


    public static final String SQL_CREATE_ITEM = "CREATE TABLE IF NOT EXISTS " +
            ITEM_TABLE_NAME + "(" +
            ITEM_ID        + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            ITEM_FIGURE_ID + " INTEGER, " +
            ITEM_NAME      + " TEXT, "    +
            ITEM_QUANTITY  + " INTEGER, " +
            ITEM_VALUE     + " REAL,"     +
            ITEM_WEIGHT    + " REAL)";


    // ---------------------------------------------------------------------------------------------
    // SQLite SELECT STATEMENTS


    public static final String SELECT_ALL                = "SELECT * FROM " + FIGURE_TABLE_NAME;
    public static final String SELECT_ALL_FROM_GROUP     = "SELECT * FROM " + GROUP_TABLE_NAME;
    public static final String SELECT_FIGURE_WITH_GUID   = SELECT_ALL + " WHERE " + FIGURE_GROUP_ID + " = ";
    public static final String SELECT_FIGURE_WITH_GUID_Q = SELECT_ALL + " WHERE " + FIGURE_GROUP_ID + " = ?";
    public static final String SELECT_USER               = "SELECT * FROM " + USER_TABLE_NAME  + " WHERE " + USER_ID        + "=1";
    public static final String SELECT_BACKPACK_ITEMS     = "SELECT * FROM " + ITEM_TABLE_NAME  + " WHERE " + ITEM_FIGURE_ID + "=";
    public static final String SELECT_GROUP_WITH_ID      = "SELECT * FROM " + GROUP_TABLE_NAME + " WHERE " + GROUP_ID       + "=";

    public static final String INSERT_DEFAULT_USER = "INSERT INTO " +
            USER_TABLE_NAME     + "(" +
            USER_NAME           + ", " +
            USER_LANGUAGE       + ", " +
            USER_LAST_STATEMENT + ", " +
            USER_TOTAL_FIGURES  + ", " +
            USER_TOTAL_GROUPS   + ", " +
            USER_INITIAL_DATE   +

            ") VALUES ('GameMaster','en','ORDER BY " +
            FIGURE_NAME  + ", " +
            FIGURE_RACE  + ", " +
            FIGURE_CLASS + ", " +
            FIGURE_LEVEL + "', '0', '0' ,'0')";


    // ---------------------------------------------------------------------------------------------
    // SQLite SELECT PARAMETERS

    public static final String PARAM_WHERE_USER_ID    = USER_ID   + "=?";
    public static final String PARAM_WHERE_GROUP_ID   = GROUP_ID  + "=?";
    public static final String PARAM_WHERE_FIGURE_ID  = FIGURE_ID + "=?";
    public static final String PARAM_WHERE_ITEM_ID    = ITEM_ID   + "=?";



    public static final String SELECT_ALL_FROM_SKILL    = "SELECT * FROM " + SKILL_TABLE_NAME + " WHERE ";



    public static final String PARAM_WHERE_SID_FID    = SKILL_SKILL_ID + "=? AND " + SKILL_FIGURE_ID + "=?";
    public static final String PARAM_WHERE_FID    = SKILL_FIGURE_ID + "=?";
    public static final String SELECT_ITEM = "SELECT * FROM " + ITEM_TABLE_NAME + " WHERE " + ITEM_FIGURE_ID + " = ?;";
}