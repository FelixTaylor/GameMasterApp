package felixtaylor.gamemaster.database;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBManager extends SQLiteOpenHelper {
    private static final String LOGTAG = "DBManager";

    public DBManager(Context context) {
        super(context, DBConstants.DBNAME, null, DBConstants.DBVERSION);
        Log.d(LOGTAG, "Created DB and opened:" + this.getDatabaseName());
    }
    public void onCreate(SQLiteDatabase db) {

        execute(db, new String[]{
                // USER
                DBConstants.SQL_CREATE_USER,
                DBConstants.INSERT_DEFAULT_USER,

                // Race
                DBConstants.SQL_CREATE_RACE,
                DBConstants.INSERT_INTO_RACE_DWARF,
                DBConstants.INSERT_INTO_RACE_ELF,
                DBConstants.INSERT_INTO_RACE_GNOME,
                DBConstants.INSERT_INTO_RACE_HALFELF,
                DBConstants.INSERT_INTO_RACE_HALFLING,
                DBConstants.INSERT_INTO_RACE_HALFORK,
                DBConstants.INSERT_INTO_RACE_HUMAN,

                // Class
                DBConstants.SQL_CREATE_CLASS,
                DBConstants.INSERT_INTO_CLASS_BARBARIAN,
                DBConstants.INSERT_INTO_CLASS_BARD,
                DBConstants.INSERT_INTO_CLASS_CLERIC,
                DBConstants.INSERT_INTO_CLASS_DRUID,
                DBConstants.INSERT_INTO_CLASS_FIGHTER,
                DBConstants.INSERT_INTO_CLASS_MAGE,
                DBConstants.INSERT_INTO_CLASS_MONK,
                DBConstants.INSERT_INTO_CLASS_PALADIN,
                DBConstants.INSERT_INTO_CLASS_RANGER,
                DBConstants.INSERT_INTO_CLASS_ROGUE,
                DBConstants.INSERT_INTO_CLASS_WARLOCK,

                // GROUP
                DBConstants.SQL_CREATE_GROUP,

                // Figure
                DBConstants.SQL_CREATE_FIGURE,

                // ITEM (BACKPACK)
                DBConstants.SQL_CREATE_ITEM,

                // SKILL
                DBConstants.SQL_CREATE_SKILL
        });
    }

    @Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
    private void execute(SQLiteDatabase db, String[] statements) {
        for (String s : statements) {
            db.execSQL(s);
            Log.d(LOGTAG, "EXECUTED: " + s);
        }
    }
}