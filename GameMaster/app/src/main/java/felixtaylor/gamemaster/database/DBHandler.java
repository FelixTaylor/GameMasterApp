package felixtaylor.gamemaster.database;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.util.ArrayList;

import felixtaylor.gamemaster.equipment.Item;
import felixtaylor.gamemaster.figure.Figure;
import felixtaylor.gamemaster.figure.Skill;
import felixtaylor.gamemaster.system.Group;
import felixtaylor.gamemaster.system.User;

public class DBHandler {

    private static final String LOGTAG = "DBHandler";
    private DBManager dbManager;
    private SQLiteDatabase db;
   // private Context context;

    public DBHandler(Context context) {
     //   this.context = context;
       // this.dbManager = new DBManager(this.context);
        this.dbManager = new DBManager(context);
        Log.d(LOGTAG, "FigureHandler ready");
    }
    public void open() {

        this.db = dbManager.getWritableDatabase();
        Log.d(LOGTAG, "The connection to database " + dbManager.getDatabaseName() + " is active.");
    }
    public void close() {
        dbManager.close();
        Log.d(LOGTAG, "Database connection has been closed.");
    }


    // SKILL METHODS
    public void update(long fid, Skill skills[]) {
        for (int i=0; i<skills.length; i++) {
            db.update(
                    DBConstants.SKILL_TABLE_NAME,
                    makeSkillValues(fid, skills[i]),
                    DBConstants.PARAM_WHERE_SID_FID,
                    new String[]{
                            String.valueOf(skills[i].getSkillId()),
                            String.valueOf(fid)
                    }
            );
        }
    }
    private ContentValues makeSkillValues(long fid, Skill s) {
        ContentValues values = new ContentValues();
        values.put(DBConstants.SKILL_SKILL_ID, s.getSkillId());
        values.put(DBConstants.SKILL_FIGURE_ID, fid);
        values.put(DBConstants.SKILL_RANK, s.getRank());
        values.put(DBConstants.SKILL_KEYABILITY, s.getKeyAbility());
        return values;
    }
    public ArrayList<Skill> selectAllSkills(long figureId) {
        ArrayList<Skill> allSkills = new ArrayList<>();
        Cursor cursor = db.rawQuery(DBConstants.SELECT_ALL_FROM_SKILL + DBConstants.PARAM_WHERE_FID,
                new String[]{Long.toString(figureId,0)});
        Log.d(LOGTAG, "STATEMENT: " + DBConstants.SELECT_ALL_FROM_SKILL + DBConstants.PARAM_WHERE_FID);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Skill s = this.cursor2Skill(cursor);
            Log.d(LOGTAG, "FOUND: " + s.toString());
            allSkills.add(s);
            cursor.moveToNext();
        }
        return allSkills;
    }

    private Skill cursor2Skill(Cursor c) {
        //long id = c.getLong(c.getColumnIndex(DBConstants.SKILL_DB_ID));
        long sid = c.getLong(c.getColumnIndex(DBConstants.SKILL_SKILL_ID));
        long fid = c.getLong(c.getColumnIndex(DBConstants.SKILL_FIGURE_ID));
        int rank = c.getInt(c.getColumnIndex(DBConstants.SKILL_RANK));
        int keyAbility = c.getInt(c.getColumnIndex(DBConstants.SKILL_KEYABILITY));
        return new Skill(sid, fid, rank, keyAbility);
    }
    public void insert(Skill s, long fid) {
        Long id = db.insert(DBConstants.SKILL_TABLE_NAME, null, makeSkillValues(fid, s));
    }



    // FIGURE METHODS
    public Figure insert(Figure f) {
        Figure figure = null;
        if (f != null) {
            Long id = db.insert(DBConstants.FIGURE_TABLE_NAME, null, makeFigureValues(f));

            int savingthrows[] = {f.getSavingThrowAt(0), f.getSavingThrowAt(1), f.getSavingThrowAt(2)};

            // TODO: Gender can just be a String!
            /*int gender;
            if (f.getGender().toLowerCase().equals("Male")) { gender = 1; }
            else { gender = 0;}
*/
            int alignment = 0;
            int attitude = 0;

            switch (f.getAlignment()) {
                case "0":case "Righteous" : alignment = 0; break;
                case "1":case "Neutral" : alignment = 1; break;
                case "2":case "Chaotic" : alignment = 2; break;
            }
            switch (f.getAttitude()) {
                case "0":case "Good" : attitude = 0; break;
                case "1":case "Neutral" : attitude = 1; break;
                case "2":case "Evil" : attitude = 2; break;
            }

            figure = new Figure (
                    id, f.getGroupId(), f.getCreationDate(), f.getName(),f.getRace(),f.getFigureClass(),
                    f.getGender(), f.getLevel(),
                    f.getHitPoints(),f.getAbilities() ,
                    alignment, attitude, savingthrows, f.getArmor(), f.getGold(),
                    f.getSkillPoints(), f.getTalentPoints(), f.isFavourite(), f.getLore(), f.getOwner()
            );
            Log.d(LOGTAG, "Inserting to database: " + figure.toString());
        }
        return figure;
    }
    public boolean update(Figure f) {
        int amount = 0;
        if (f != null) {
            amount = db.update(
                    DBConstants.FIGURE_TABLE_NAME,
                    makeFigureValues(f),
                    DBConstants.PARAM_WHERE_FIGURE_ID,
                    new String[]{String.valueOf(f.getId())}
            );

            Log.d(LOGTAG, "Figure with id " + f.getId() + " was updated.");
        }
        return amount>2;
    }
    public boolean delete(Figure figure) {
        int amount = db.delete(
                DBConstants.FIGURE_TABLE_NAME,
                DBConstants.PARAM_WHERE_FIGURE_ID,
                new String[]{String.valueOf(figure.getId())}
        );

        // TODO: DELETE ITEMS FROM FIGURE

        Log.d(LOGTAG, "Entry with ID " + figure.getId() + " was deleted from DB.");
        return amount>2;
    }
    public int countFiguresInGroup(long groupId) {
        Cursor c = db.rawQuery(
                DBConstants.SELECT_FIGURE_WITH_GUID_Q,
                new String[]{Long.toString(groupId)} );

        return c.getCount();
    }
    public ArrayList<Figure> selectAllFigures(String statement) {
        ArrayList<Figure> allFigures = new ArrayList<>();
        Cursor cursor = db.rawQuery(statement, null);
        Log.d(LOGTAG, "STATEMENT: " + statement);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Figure f = this.cursor2Figure(cursor);
            Log.d(LOGTAG, "FOUND: " + f.toString());
            allFigures.add(f);
            cursor.moveToNext();
        }
        return allFigures;
    }
    public Figure selectFigure(long id) {
        Figure f = null;
        Cursor cursor = db.rawQuery(
                DBConstants.SELECT_ALL + " WHERE " +
                    DBConstants.FIGURE_ID + " = " + id,
                null);

        if (cursor != null) {
            cursor.moveToFirst();
            f = this.cursor2Figure(cursor);
            Log.d(LOGTAG, "Found Figure with ID=" + f.getId());
        }

        cursor = db.rawQuery(
                DBConstants.SELECT_BACKPACK_ITEMS + id,null);
        if (cursor != null) {

            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                Item item = this.cursor2Item(cursor);
                Log.d(LOGTAG, "FOUND ITEM: " + item.toString());
                f.addItem(item);
                cursor.moveToNext();
            }
        }


        return f;
    }

    private Figure cursor2Figure(Cursor c) {
        long id = c.getLong(c.getColumnIndex(DBConstants.FIGURE_ID));
        long gid = c.getLong(c.getColumnIndex(DBConstants.FIGURE_GROUP_ID));
        boolean isFavourite = c.getInt(c.getColumnIndex(DBConstants.FIGURE_FAVOURITE)) == 1;

        String name = c.getString(c.getColumnIndex(DBConstants.FIGURE_NAME));
        String race = c.getString(c.getColumnIndex(DBConstants.FIGURE_RACE));
        String fclass = c.getString(c.getColumnIndex(DBConstants.FIGURE_CLASS));

        int level = c.getInt(c.getColumnIndex(DBConstants.FIGURE_LEVEL));
        int abilities[] = new int[]{
                c.getInt(c.getColumnIndex(DBConstants.FIGURE_STR)),
                c.getInt(c.getColumnIndex(DBConstants.FIGURE_DEX)),
                c.getInt(c.getColumnIndex(DBConstants.FIGURE_CON)),
                c.getInt(c.getColumnIndex(DBConstants.FIGURE_INT)),
                c.getInt(c.getColumnIndex(DBConstants.FIGURE_WIS)),
                c.getInt(c.getColumnIndex(DBConstants.FIGURE_CHA))
        };

        int hitpoints = c.getInt(c.getColumnIndex(DBConstants.FIGURE_HITPOINTS));
        int skillpoints = c.getInt(c.getColumnIndex(DBConstants.FIGURE_SKILLS_POINTS));
        int talentpoints = c.getInt(c.getColumnIndex(DBConstants.FIGURE_TALENTS_POINTS));
        int savingThrows[] = new int[] {
                c.getInt(c.getColumnIndex(DBConstants.FIGURE_FORTITUDE)),
                c.getInt(c.getColumnIndex(DBConstants.FIGURE_REFLEX)),
                c.getInt(c.getColumnIndex(DBConstants.FIGURE_WILL))
        };

        int armor = c.getInt(c.getColumnIndex(DBConstants.FIGURE_ARMOR));
        int gold = c.getInt(c.getColumnIndex(DBConstants.FIGURE_GOLD));

        String gender = c.getString(c.getColumnIndex(DBConstants.FIGURE_GENDER));

        int alignment = c.getInt(c.getColumnIndex(DBConstants.FIGURE_ALIGNMENT));
        int attitude = c.getInt(c.getColumnIndex(DBConstants.FIGURE_ATTITUDE));

        String lore = c.getString(c.getColumnIndex(DBConstants.FIGURE_LORE));
        long date = c.getLong(c.getColumnIndex(DBConstants.FIGURE_CREATION_DATE));
        String owner = c.getString(c.getColumnIndex(DBConstants.FIGURE_OWNER));

       /* Figure f =*/
       return new Figure(id, gid, date, name, race, fclass, gender, level, hitpoints, abilities,
                alignment, attitude, savingThrows, armor, gold, skillpoints,
                talentpoints, isFavourite, lore, owner);



       // return f;
    }
    private ContentValues makeFigureValues(Figure f) {
        ContentValues values = new ContentValues();
        values.put(DBConstants.FIGURE_GROUP_ID, f.getGroupId());

        if (f.isFavourite()) {
            values.put(DBConstants.FIGURE_FAVOURITE, 1);
        } else {
            values.put(DBConstants.FIGURE_FAVOURITE, 0);
        }

        values.put(DBConstants.FIGURE_NAME, f.getName());
        values.put(DBConstants.FIGURE_RACE, f.getRace());
        values.put(DBConstants.FIGURE_CLASS, f.getFigureClass());
        values.put(DBConstants.FIGURE_LEVEL, f.getLevel());

        values.put(DBConstants.FIGURE_STR, f.getAbility(0));
        values.put(DBConstants.FIGURE_DEX, f.getAbility(1));
        values.put(DBConstants.FIGURE_CON, f.getAbility(2));
        values.put(DBConstants.FIGURE_INT, f.getAbility(3));
        values.put(DBConstants.FIGURE_WIS, f.getAbility(4));
        values.put(DBConstants.FIGURE_CHA, f.getAbility(5));

        values.put(DBConstants.FIGURE_HITPOINTS, f.getHitPoints());
        values.put(DBConstants.FIGURE_SKILLS_POINTS, f.getSkillPoints());
        values.put(DBConstants.FIGURE_TALENTS_POINTS, f.getTalentPoints());

        values.put(DBConstants.FIGURE_FORTITUDE, f.getSavingThrowAt(0));
        values.put(DBConstants.FIGURE_REFLEX, f.getSavingThrowAt(1));
        values.put(DBConstants.FIGURE_WILL, f.getSavingThrowAt(2));

        values.put(DBConstants.FIGURE_ARMOR, f.getArmor());
        values.put(DBConstants.FIGURE_GOLD, f.getGold());

        /*if (f.getGender().toLowerCase().equals("Male")) { values.put(DBConstants.FIGURE_GENDER, 1); }
        else { values.put(DBConstants.FIGURE_GENDER, 0); }
*/
        values.put(DBConstants.FIGURE_GENDER, f.getGender());
        values.put(DBConstants.FIGURE_ALIGNMENT, f.getAlignment());
        values.put(DBConstants.FIGURE_ATTITUDE, f.getAttitude());

        values.put(DBConstants.FIGURE_LORE, f.getLore());
        values.put(DBConstants.FIGURE_CREATION_DATE, f.getCreationDate());
        values.put(DBConstants.FIGURE_OWNER, f.getOwner());


        return values;
    }

    public Item cursor2Item(Cursor c) {
        long id = c.getInt(c.getColumnIndex(DBConstants.ITEM_ID));
        String name = c.getString(c.getColumnIndex(DBConstants.ITEM_NAME));
        int quantity = c.getInt(c.getColumnIndex(DBConstants.ITEM_QUANTITY));

        double value = c.getDouble(c.getColumnIndex(DBConstants.ITEM_VALUE));
        double weight = c.getDouble(c.getColumnIndex(DBConstants.ITEM_WEIGHT));

        return new Item(id,name,quantity, value, weight);
    }

    // GROUP METHODS
    public Group insert(Group group) {
        Long id = db.insert(
                DBConstants.GROUP_TABLE_NAME,
                null,
                makeGroupValues(group)
        );

        Log.d(LOGTAG, "Inserted: Group with id: " + id);
        return new Group(id, group.getCreationDateInMillis(), group.getName(), group.getName());
    }
    public long update(Group group) {
        return db.update(
                DBConstants.GROUP_TABLE_NAME,
                makeGroupValues(group),
                DBConstants.PARAM_WHERE_GROUP_ID,
                new String[]{String.valueOf(group.getId())}
        );
    }
    public int delete(Group group) {
        return db.delete(
                DBConstants.GROUP_TABLE_NAME,
                DBConstants.PARAM_WHERE_GROUP_ID,
                new String[]{String.valueOf(group.getId())}
        );
    }

    public long selectMaxGroupId() {
        long id = 0;
        Cursor cursor = db.rawQuery(DBConstants.SELECT_ALL_FROM_GROUP, null);
        Log.d(LOGTAG, "STATEMENT: " + DBConstants.SELECT_ALL_FROM_GROUP);
        cursor.moveToFirst();

        Group group;
        while(!cursor.isAfterLast()) {
            group = this.cursor2Group(cursor);

            if (group.getId()>id) {
                id = group.getId();
            }
            Log.d(LOGTAG, "FOUND: " + group.toString());
            cursor.moveToNext();
        }
        return id+1;
    }
    public ArrayList<Group> selectAllGroups(String statement) {
        ArrayList<Group> groups = new ArrayList<>();
        Cursor cursor = db.rawQuery(statement, null);
        Log.d(LOGTAG, "STATEMENT: " + statement);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Group group = this.cursor2Group(cursor);
            Log.d(LOGTAG, "FOUND: " + group.toString());
            groups.add(group);
            cursor.moveToNext();
        }
        return groups;
    }
    public Group selectGroup(String statement) {
        Group group = null;
        Cursor cursor = db.rawQuery(statement, null);
        Log.d(LOGTAG, "STATEMENT: " + statement);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            group = this.cursor2Group(cursor);
            Log.d(LOGTAG, "FOUND: " + group.toString());
            cursor.moveToNext();
        }
        return group;
    }

    private Group cursor2Group(Cursor c) {
        long id = c.getInt(c.getColumnIndex(DBConstants.GROUP_ID));
        String name = c.getString(c.getColumnIndex(DBConstants.GROUP_NAME));
        String description = c.getString(c.getColumnIndex(DBConstants.GROUP_DESCRIPTION));
        String date = c.getString(c.getColumnIndex(DBConstants.GROUP_CREATION_TIME));

        return new Group(id,Long.valueOf(date),name, description);
    }
    private ContentValues makeGroupValues(Group group) {
        ContentValues cv = new ContentValues();

        // TODO: REMOVE ID FIELD
        cv.put(DBConstants.GROUP_ID, group.getId());
        cv.put(DBConstants.GROUP_NAME, group.getName());
        cv.put(DBConstants.GROUP_DESCRIPTION, group.getDescription());
        cv.put(DBConstants.GROUP_CREATION_TIME, Long.toString(group.getCreationDateInMillis()));
        return cv;
    }

    // USER METHODS
    public boolean update(User u) {
        int amount = db.update(
                DBConstants.USER_TABLE_NAME,
                makeUserValues(u),
                DBConstants.PARAM_WHERE_USER_ID,
                new String[]{String.valueOf(u.getId())}
        );

        Log.d(LOGTAG, "Updated user(" + u.getId() + ") Time=" + u.getInitialUseTime() + ", Groups=" + u.getTotalCreatedGroups() + ", Figures=" + u.getTotalCreatedFigures());
        return amount>0;
    }
    public User getUser() {
        Cursor cursor = db.rawQuery(DBConstants.SELECT_USER, null);
        cursor.moveToFirst();
        return cursor2User(cursor);
    }
    private User cursor2User(Cursor c) {
        long id = c.getLong(c.getColumnIndex(DBConstants.USER_ID));
        String name = c.getString(c.getColumnIndex(DBConstants.USER_NAME));
        String language = c.getString(c.getColumnIndex(DBConstants.USER_LANGUAGE));
        String statement = c.getString(c.getColumnIndex(DBConstants.USER_LAST_STATEMENT));
        int showPopUp = c.getInt(c.getColumnIndex(DBConstants.USER_NAME_POPUP));

        // New values since DB ver. 4
        int totalFigure = c.getInt(c.getColumnIndex(DBConstants.USER_TOTAL_FIGURES));
        int totalGroups = c.getInt(c.getColumnIndex(DBConstants.USER_TOTAL_GROUPS));
        long initTime   = c.getLong(c.getColumnIndex(DBConstants.USER_INITIAL_DATE));

        Log.d(LOGTAG, "FOUND: User (" + id + ") " + name);
        return new User(id,name,language, statement, showPopUp, totalFigure, totalGroups, initTime);
    }

    private ContentValues makeUserValues(User u) {
        ContentValues values = new ContentValues();
        values.put(DBConstants.USER_NAME, u.getName());
        values.put(DBConstants.USER_LANGUAGE, u.getLanguage());
        values.put(DBConstants.USER_LAST_STATEMENT, u.getOrderBy());
        values.put(DBConstants.USER_NAME_POPUP, u.getShowUserNamePopUp());

        values.put(DBConstants.USER_INITIAL_DATE, u.getInitialUseTime());
        values.put(DBConstants.USER_TOTAL_GROUPS, u.getTotalCreatedGroups());
        values.put(DBConstants.USER_TOTAL_FIGURES, u.getTotalCreatedFigures());

        return values;
    }


    // ITEM
    public Item insert(Item item, long figure_id) {
        Long id = db.insert(
                DBConstants.ITEM_TABLE_NAME,
                null,
                makeItemValues(item, figure_id)
        );

        return new Item(id,item.getName(),item.getQuantity(), item.getValue(), item.getWeight());
    }
    public long update(Item item, long figure_id) {
        return db.update(
                DBConstants.ITEM_TABLE_NAME,
                makeItemValues(item, figure_id),
                DBConstants.PARAM_WHERE_ITEM_ID,
                new String[]{String.valueOf(item.getId())}
        );
    }
    public int delete(Item item) {
        return db.delete(
                DBConstants.ITEM_TABLE_NAME,
                DBConstants.PARAM_WHERE_ITEM_ID,
                new String[]{String.valueOf(item.getId())}
        );
    }
    private ContentValues makeItemValues(Item item, long figure_id) {
        ContentValues cv = new ContentValues();
        cv.put(DBConstants.ITEM_NAME, item.getName());
        cv.put(DBConstants.ITEM_QUANTITY, item.getQuantity());
        cv.put(DBConstants.ITEM_VALUE, item.getValue());
        cv.put(DBConstants.ITEM_WEIGHT, item.getWeight());
        cv.put(DBConstants.ITEM_FIGURE_ID, figure_id);
        return cv;
    }


    public double getItemWeightSum(long fid) {
        double weight = 0.0;
        Cursor c = db.rawQuery(DBConstants.SELECT_ITEM, new String[] {Long.toString(fid)});
        c.moveToFirst();

        while (!c.isAfterLast()) {
            double times = (double) c.getInt(c.getColumnIndex(DBConstants.ITEM_QUANTITY));
            double w = c.getDouble(c.getColumnIndex(DBConstants.ITEM_WEIGHT));
            weight += (w * times);
            c.moveToNext();
        }

        //Log.d(LOGTAG, "getItemWeightSum: weight=" + weight);
        return weight;
    }

}
