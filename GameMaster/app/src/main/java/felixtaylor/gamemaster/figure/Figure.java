package felixtaylor.gamemaster.figure;
import android.util.Log;
import java.lang.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import felixtaylor.gamemaster.equipment.Equipment;
import felixtaylor.gamemaster.equipment.Item;
import felixtaylor.gamemaster.figure.tribe.Tribe;
import felixtaylor.gamemaster.system.Entity;

public class Figure extends Entity {
	private static final long serialVersionUID = 4L;
	private static final String LOGTAG = Figure.class.getSimpleName();
	private Random r;

	private long    groupId, creationDateInMillis;
	private String  owner,   lore, gender, alignment, attitude;
	private int     level,   hitPoints, skillPoints, talentPoints;
	private int[]   savingThrows = new int[3]; // Fortitude, Reflex, Will
	private boolean isFavourite;

	private Skill[] skills = new Skill[32];

	private Tribe tribe;
	private Abilities abilities;
	private BaseAttackBonus bab;
	private Equipment equipment;

	public Figure() {
		this.r = new Random();

		setGroupId(-1);
		setCreationDate(System.currentTimeMillis());
		setFavourite(false);

		this.owner 		  = "GameMaster";
		this.level        = 1;
		this.hitPoints    = 1;
		this.skillPoints  = 0;
		this.talentPoints = 0;
		this.lore		  = "";
		this.gender		  = "Female";
		this.alignment	  = "Neutral";
		this.attitude	  = "Neutral";

		this.savingThrows = new int[3];
		this.tribe		  = new Tribe();
		this.abilities 	  = new Abilities();
		this.equipment	  = new Equipment();
		this.bab = new BaseAttackBonus();

		setSkills(-1);
	}

	public void setSkills(long fid) {
		for (int i=0; i<skills.length; i++) {

			if (i<=2) {
				skills[i] = new Skill(i,fid,0,0);
			} else if(i<=11) {
				skills[i] = new Skill(i,fid,0,1);
			} else if(i<=12) {
				skills[i] = new Skill(i,fid,0,2);
			} else if (i<=20) {
				skills[i] = new Skill(i,fid,0,3);
			} else if(i<=25) {
				skills[i] = new Skill(i,fid,0,4);
			} else if(i<=32) {
				skills[i] = new Skill(i,fid,0,5);
			}

		}
	}


	public Figure(
			long id, 			long gid,         long date,
			String name,		String race,      String fclass,
			String gender, 		int level, 	      int hitPoints,
			int[] abilities, 	int alignment, 	  int attitude,
			int[] savingthrows, int armor, 		  int gold,
			int skillpoints, 	int talentpoints, boolean isFavourite,
			String lore, 		String owner/*, int ranks[]*/) {

		this();
		setOwner(owner);
		setId(id);
		setGroupId(gid);
		setCreationDate(date);
		setName(name);
		setRace(race);
		setClass(fclass);
		setLevel(level);
		setGender(gender);
		setLevel(level);
		setHitPoints(hitPoints);
		setAbilities(abilities);
		setBaseAttackBonus();
		setDisposition(Integer.toString(alignment), Integer.toString(attitude));
		setSavingThrows(savingthrows);
		setArmor(armor);
		setGold(gold);
		setSkillPoints(skillpoints);
		setTalentPoints(talentpoints);
		setFavourite(isFavourite);
		setLore(lore);

		//setSkillRanks(ranks);
	}

	public void calculateFigure() {
		randomAbilities();
		distributeAbilities(getFigureClass());

		// Add new ability points
		addAbilityPoints(getFigureClass(), getLevel());
		addRaceBonus();

		randomGender();
		randomName();
		randomDisposition();

		setBaseAttackBonus();
		calculateHitpoints();
		calculateSkillPoints();
		calculateTalentPoints();
		calculateSavingThrows();

		calculateArmor();
		calculateGold();
	}
	public String dateToString(String displayPattern) {
		return new SimpleDateFormat(
				displayPattern,
				Locale.getDefault()
		).format(new Date(this.creationDateInMillis));
	}
	@Override public boolean equals(Object object) {
		if (object instanceof Figure) {
			if (this == object) {
				return true;
			}
		}
		return false;
	}

	/* RANDOM Method */
	public void random() {
		// Remove previous ability points
		removeRaceBonus();
		removeAbilityPoints(getFigureClass(), getLevel());

		randomRace();
		randomClass();
		randomLevel();
		calculateFigure();
	}
	public void random(int[] levelRange, ArrayList<String> races, ArrayList<String> classes) {
		// Remove previous ability points
		removeRaceBonus();
		removeAbilityPoints(getFigureClass(), getLevel());

		setRace(races.get(r.nextInt(races.size())));
		setClass(classes.get(r.nextInt(classes.size())));
		randomLevelInRange(levelRange);
		calculateFigure();
	}

	private void randomRace() {
		this.tribe.random("Race");
	}
	private void randomClass() {
		this.tribe.random("Class");
	}
	private void randomAbilities() {
		this.abilities.random();

		for (int i=0; i<5; i++) {
			this.abilities.calculateModifier(i);
		}
	}
	private void randomGender() {
		this.gender = r.nextInt(2)==0 ? "Female" : "Male";
	}
	private void randomName() {
		setName(Name.random(
				this.tribe.toString("Race"),
				isFemale()
		));
	}
	private void randomDisposition() {
		String alignment[] = {"Righteous", "Neutral", "Chaotic"};
		String attitude[] = {"Good", "Neutral", "Evil"};
		int i = 0;
		int j = 0;
		//if (!getFigureClass().equals("")) {
			//do {
				switch (getFigureClass().toLowerCase()) {
					case "barbarian":case "bard": {
						i = r.nextInt(2)+1;
						j = r.nextInt(attitude.length);
						break;
					}
					case "mage": {
						//i = 0;
						j = r.nextInt(attitude.length);
						break;
					}
					case "monk": {
						i = 1; // TODO: Is this correct ?
						j = 1;
						break;
					}
					case "cleric": {
						int k = r.nextInt(5);
						switch (k) {
							case 0: i=1; j=0; break;
							case 1: i=1; j=2; break;
							case 2: i=1; j=1; break;
							case 3: i=0; j=1; break;
							case 4: i=2; j=1; break;
						}
						break;
					}
					default: {
						i = r.nextInt(alignment.length);
						j = r.nextInt(attitude.length);
						break;
					}
				}
			//} while (i<0 || j<0);
			this.alignment = alignment[i];
			this.attitude = attitude[j];
		/*} else {
			System.err.println("Disposition.random: Parameter is null or empty.");
		}*/
	}
	private void randomLevel() {
		this.level = Level.random();
	}
	private void randomLevelInRange(int[] range) {
		this.level = Level.randomInRange(range);
	}

	/* OWNER: The owner is the user
	   who created this figure. */

	public void   setOwner(String owner) {
		this.owner = owner;
	}
	public String getOwner() {
		return this.owner;
	}

	/* GROUP ID: The group id determines in which
	   group the figure was created and therefor in
	   which group it should be displayed. */

	public void  setGroupId(long id) {
		this.groupId = id;
	}
	public long  getGroupId() {
		return this.groupId;
	}

	/* CREATION DATE: The creationDate is the
	   date and time in which the figure was created
	   by its owner. */

	private void setCreationDate(long timeInMillis) {
		this.creationDateInMillis = timeInMillis;
	}
	public long  getCreationDate() {
		return this.creationDateInMillis;
	}

	/* FAVOURITE: Is a simple boolean to determine
	   whether the figure is a favourite one or not. */

	public void    setFavourite(boolean isFavourite) {
		this.isFavourite = isFavourite;
	}
	public boolean isFavourite() {
		return this.isFavourite;
	}

	/* LEVEL: The level holds an integer which
	   determines the overall strength of the
	   current figure. */

	public void setLevel(int level) {
		this.level = level>=1 ? level : 1;
	}
	public int  getLevel() {
		return this.level;
	}

	/* TRIBE: The tribe contains the race and class of the
	   current figure. It also determines the race specific
	   bonus. */

	public void setRace(String name) {
		this.tribe.setRace(name);
	}
	public void setRace(long id) {
		this.tribe.setRace(id);
	}
	public void setClass(String name) {
		this.tribe.setClass(name);
	}
	public void setClass(long id) {
		this.tribe.setClass(id);
	}

	public void addRaceBonus() {
		Log.d(LOGTAG, "addRaceBonus()");
		switch (tribe.getIndexAtTrue("Race")) {

			/* All races except humans and elf's
			are getting a bonus. */

			case 0:case 1:case 2:case 4:case 5: {
				abilities.set(
						tribe.getBonusAbility(0),
						abilities.getAbility(
								tribe.getBonusAbility(0)) +
								tribe.getRaceBonus(0)
				);

				abilities.set(
						tribe.getBonusAbility(1),
						abilities.getAbility(
								tribe.getBonusAbility(1)) +
								tribe.getRaceBonus(1)
				);
				break;
			}
		}
	}
	public void removeRaceBonus() {
		Log.d(LOGTAG, "removeRaceBonus()");
		switch (tribe.getIndexAtTrue("Race")) {

			case 0:case 1:case 2:case 4:case 5: {
				abilities.set(
						tribe.getBonusAbility(0),
						abilities.getAbility(
								tribe.getBonusAbility(0)) + -tribe.getRaceBonus(0)
				);
				abilities.set(
						tribe.getBonusAbility(1),
						abilities.getAbility(
								tribe.getBonusAbility(1)) + Math.abs(tribe.getRaceBonus(1))
				);

			}
			break;
		}
	}

	public String getRace() {
		return this.tribe.toString("Race");
	}
	public String getFigureClass() {
		return this.tribe.toString("Class");
	}

	/* ABILITIES: The abilities are str, dex, con, wis, int and wis.
	   They determine the base values of the current figure. */

	private void setAbilities(int abilities[]) {
		for (int i=0; i<abilities.length; i++) {
			this.abilities.set(i, abilities[i]);
			this.abilities.calculateModifier(i);
		}
	}
	public void  setAbility(int index, int value) {
		this.abilities.set(index, value);
		this.abilities.calculateModifier(index);
	}
	public int   getAbility(int position) {
		return this.abilities.getAbility(position);
	}
	public int   getAbilityMod(int index) {
		return this.abilities.getModifier(index);
	}
	public int[] getAbilities() {
		return new int[] {
				this.abilities.getAbility(0),
				this.abilities.getAbility(1),
				this.abilities.getAbility(2),
				this.abilities.getAbility(3),
				this.abilities.getAbility(4),
				this.abilities.getAbility(5),
		};
	}

	public void addAbilityPoints(String charCLass, int level) {
		this.abilities.addPoints(charCLass, level);
	}
	public void removeAbilityPoints(String charClass, int level) {
		this.abilities.removePoints(charClass, level);
	}
	public void distributeAbilities(String charClass) {
		this.abilities.distribute(charClass);
	}

	public String getBonusAbilityName(int position) {
		return this.tribe.getBonusAbility(position);
	}
	public int    getBonusAbilityValue(int position) {
		return this.tribe.getRaceBonus(position);
	}

	/* SKILLPOINTS */
	public void  calculateSkillPoints(){
		int skillPoints, modifier;

		switch (getFigureClass()) {
			case "Cleric": case "Fighter": case "Mage": case "Paladin": case "Warlock": {
				modifier = 2;
				break;
			}
			case "Barbarian": case "Druid": case "Monk": {
				modifier = 4;
				break;
			}
			case "Bard": case "Ranger": {
				modifier = 6;
				break;
			}
			default: {
				modifier = 8;
			}
		}

		// TODO: Skillpoints can be below 0!
		int intMod = this.abilities.getModifier("INT")>0 ? this.abilities.getModifier("INT") : 1;
		int points = modifier + this.abilities.getModifier("INT");
		skillPoints = getRace().equals("Human") ? points*4+4 : points*4;

		for (short i=1; i!=getLevel(); i++) {
			skillPoints += getRace().equals("Human") ? points + 1 : points;
		}

		this.skillPoints = skillPoints;
	}
	private void setSkillPoints(int skillPoints) {
		this.skillPoints = skillPoints;
	}
	public int   getSkillPoints() {
		return this.skillPoints;
	}

	/* TALENTPOINTS */
	public void  calculateTalentPoints() {
		int points = getRace().equals("Human") ? 2 : 1;
		for (int i=3; i<=getLevel(); i++) {
			if (i%3 == 0) {
				points += 1;
			}
		}
		this.talentPoints = points;
	}
	private void setTalentPoints(int talentPoints) {
		this.talentPoints = talentPoints;
	}
	public int   getTalentPoints() {
		return this.talentPoints;
	}

	/* HITPOINTS */
	private void setHitPoints(int hitPoints) {
		this.hitPoints = hitPoints;
	}
	public void  calculateHitpoints() {
		this.hitPoints = Hitpoints.calculate(
				this.tribe.toString("Class"), this.level,
				this.abilities.getModifier("CON")
		);
	}
	public int   getHitPoints() {
		return this.hitPoints;
	}

	/* DISPOSITION */
	public boolean validateDisposition(String alignment, String attitude) {
		boolean isValid = false;
		switch (getFigureClass().toLowerCase()) {
			case "barbarian": case "bard": {
				if (!alignment.equals("Righteous"))
					isValid = true;
				break;
			}
			case "cleric": {
				if (alignment.equals("Neutral") || attitude.equals("Neutral"))
					isValid = true;
				break;
			}
			case "mage": {
				if (alignment.equals("Righteous"))
					isValid = true;
				break;
			}
			case "monk": {
				if (alignment.equals("Righteous") && attitude.equals("Good"))
					isValid = true;
				break;
			}

			case "druid":
			case "fighter":
			case "paladin":
			case "ranger":
			case "rogue":
			case "warlock": {
				isValid = true;
				break;
			}
		}
		return isValid;
	}
	public void   setDisposition(String alignment, String attitude) {
		this.alignment = alignment;
		this.attitude = attitude;
	}
	public String getAlignment() {
		return this.alignment;
	}
	public String getAttitude() {
		return attitude;
	}

	/* GENDER */
	public void   setGender(String gender) {
		this.gender = gender;
		Log.d(LOGTAG, "Gender set to: " + gender);
	}
	public String getGender() {
		Log.d(LOGTAG, "Gender is: " + this.gender);
		return this.gender;
	}
	private boolean isFemale() {
		Log.d(LOGTAG, "Gender is Female: " + this.gender.equalsIgnoreCase("Female"));
		return this.gender.equalsIgnoreCase("Female");
	}

	/* LORE */
	public void   setLore(String lore) {
		this.lore = lore;
	}
	public String getLore() {
		return this.lore;
	}

	/* BASE ATTACK BONUS */
	public void setBaseAttackBonus() {
		this.bab = new BaseAttackBonus(
				getFigureClass(),
				getLevel(),
				getAbilityMod(1),
				getAbilityMod(0)
		);
	}
	public int  getAttacks() {
		return this.bab.getAttacks();
	}
	public int  getAttackModifier() {
		return this.bab.getAttackModifier();
	}
	public int  getBaseAttackBonus() {
		return this.bab.getBaseAttackBonus();
	}
	public int  getMaxMelee() {
		return this.bab.getMaxMelee();
	}
	public int  getMaxRanged() {
		return this.bab.getMaxRanged();
	}

	/* SAVING THROWS */
	private void setSavingThrows(int[] savingThrows) {
		this.savingThrows = savingThrows;
	}
	public void  calculateSavingThrows() {
		this.savingThrows = SavingThrows.calculate(
				this.tribe.toString("Class"), this.level,
				this.abilities.getModifier("DEX"),
				this.abilities.getModifier("CON"),
				this.abilities.getModifier("WIS")
		);
	}
	public int   getSavingThrowAt(int index) {
		return this.savingThrows[index];
	}

	/* EQUIPMENT */
	private void setArmor(int armor) {
		this.equipment.setArmor(armor);
	}
	private void setGold(int gold) {
		this.equipment.setGold(gold);
	}
	public void  calculateArmor() {
		this.equipment.calculateArmor(this.abilities.getModifier("DEX"));
	}
	public void  calculateGold() {
		this.equipment.calculateGold(this.tribe.getIndexAtTrue("Class"));
	}

	public int getArmor() {
		return this.equipment.getArmor();
	}
	public int getGold() {
		return this.equipment.getGold();
	}

	public void setSkillRank(int skillId, int rank) {
		this.skills[skillId].setRank(rank);
	}
	public void setSkills(ArrayList<Skill> skills) {
		for (int i=0; i<skills.size(); i++) {
			this.skills[i] = skills.get(i);
		}
	}
	public void setSkillRanks(int ranks[]) {
		for (int i=0; i<skills.length; i++) {

			if (i<=2) {
				skills[i] = new Skill(i,getId(),ranks[i],0);
			} else if(i<=11) {
				skills[i] = new Skill(i,getId(),ranks[i],1);
			} else if(i<=12) {
				skills[i] = new Skill(i,getId(),ranks[i],2);
			} else if (i<=20) {
				skills[i] = new Skill(i,getId(),ranks[i],3);
			} else if(i<=25) {
				skills[i] = new Skill(i,getId(),ranks[i],4);
			} else if(i<=32) {
				skills[i] = new Skill(i,getId(),ranks[i],5);
			}

		}
	}
	public int getSkillPointSum() {
		int res = 0;
		for (Skill s : this.skills) {
			res += s.getRank();
		}
		return res;
	}
	public int[] getSkillRanks() {
		int ranks[] = new int[skills.length];
		for (int i=0; i<ranks.length; i++) {
			ranks[i] = skills[i].getRank();
		}
		return ranks;
	}
	public Skill[] getSkills() {
		return this.skills;
	}

	public void addItem(Item item) {
		this.equipment.addItem(item);
	}
	public ArrayList<Item> getItems() {
		return this.equipment.getItems();
	}
	public void updateItem(Item item) {
		this.equipment.updateItem(item);
	}
	public void removeItem(long id) {
		this.equipment.removeItem(id);
	}
	public Item getItem(long id) {
		return this.equipment.getItem(id);
	}
}