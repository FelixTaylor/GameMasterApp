package felixtaylor.gamemaster.figure;
public class SavingThrows {
	private static int savingThrowBase;
	private static void badSavingThrowBase(int level) {
		savingThrowBase = 0;
		for (int i=3; i<=level; i++) {
			if (i%3 == 0)
				savingThrowBase += 1;
		}
	}
	private static void goodSavingThrowBase(int level) {
		savingThrowBase = 2;
		for (int i=2; i<=level; i++) {
			if (i%2 == 0)
				savingThrowBase += 1;
		}
	}

	public static int[] calculate(String charClass, int level, int dexterityMod, int constitutionMod, int wisdomMod) {
		int points[] = new int[3];
		savingThrowBase = 0;

		// Fortitude
		switch (charClass) {
			case "Barbarian": case "Cleric": case "Druid": case "Fighter": case "Monk":
			case "Paladin": case "Ranger": {
				goodSavingThrowBase(level);
				break;
			}
			default: {
				badSavingThrowBase(level);
			}
		}

		// Reflex
		switch (charClass) {
			case "Bard": case "Monk": case "Ranger": case "Rogue": {
				goodSavingThrowBase(level);
				break;
			}
			default: {
				badSavingThrowBase(level);
			}
		}

		// Will
		switch (charClass) {
			case "Bard": case "Cleric": case "Druid": case "Mage": case "Monk": case "Warlock": {
				goodSavingThrowBase(level);
				break;
			}
			default: {
				badSavingThrowBase(level);
			}
		}

		points[0] = constitutionMod + savingThrowBase;
		points[1] = dexterityMod + savingThrowBase;
		points[2] = wisdomMod + savingThrowBase;

		return points;
	}
}