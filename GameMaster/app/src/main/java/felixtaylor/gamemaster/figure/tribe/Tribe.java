package felixtaylor.gamemaster.figure.tribe;
import java.io.Serializable;
import java.util.Random;
import felixtaylor.gamemaster.figure.misc.Token;

public class Tribe implements Serializable {
    private static final long serialVersionUID = 13L;

    /*  The Tribe class contains the Race and
        Class and combines them to the
        tribe class. There are a total number of 7
        races and 11 classes to choose from. */

    private Race[]  races;
    private Token[] classes;
    private Random  random;

    public Tribe() {
        final String RACENAME[] = {"Dwarf", "Elf", "Gnome", "Halfelf",
                "Halfling", "Halfork", "Human" };

        this.races = new Race[RACENAME.length];
        for (int i=0; i<this.races.length; i++) {
            this.races[i] = new Race();
            this.races[i].setName(RACENAME[i]);
        }


        // DWARF
        this.races[0].setBonus(0, "CON", 2);
        this.races[0].setBonus(1, "CHA", -2);

        // ELF
        this.races[1].setBonus(0, "DEX", 2);
        this.races[1].setBonus(1, "CON", -2);

        // GNOME
        this.races[2].setBonus(0, "CON", 2);
        this.races[2].setBonus(1, "STR", -2);

        //HALFLING
        this.races[4].setBonus(0, "DEX", 2);
        this.races[4].setBonus(1, "STR", -2);

        //HALF ORK
        this.races[5].setBonus(0, "STR", 2);
        this.races[5].setBonus(1, "CHA", -2);



        final String CLASS[] = {"Barbarian", "Bard", "Cleric",
                "Druid", "Fighter", "Mage", "Monk", "Paladin", "Ranger",
                "Rogue", "Warlock" };

        this.classes = new Token[CLASS.length];
        for (int i=0; i<this.classes.length; i++) {
            this.classes[i] = new Token();
            this.classes[i].setName(CLASS[i]);
        }

        this.random = new Random();
        set("Human", "Fighter");
    }

    /*  The reset method resets the selected
        race or class of the figure. */

    public void reset(String entity) {
        switch (entity.toLowerCase()) {
            case "race": {
                for (Race r : this.races) {
                    if (r.isSelected()) {
                        r.setSelected(false);
                    }
                }
                break;
            }
            case "class": {
                for (Token t : this.classes) {
                    if (t.isSelected()) {
                        t.setSelected(false);
                    }
                }
                break;
            }
        }
    }

    /*  The random methods generate a random tribe
        or explicit only the class or race. */

    public void random() {
        random("Race");
        random("Class");
    }
    public void random(String entity) {
        switch (entity) {
            case "Race":case "race": {
                reset("Race");
                this.races[this.random.nextInt(this.races.length)].setSelected(true);
            }
            break;

            case "Class":case "class": {
                reset("Class");
                this.classes[this.random.nextInt(this.classes.length)].setSelected(true);
            }
            break;
        }
    }

    /*  The set method sets both the
        race and class of a figure. */

    public void set(String raceName, String className) {
        reset("Race");
        reset("Class");
        setRace(raceName);
        setClass(className);
    }
    public void setRace(String name) {
        reset("Race");
        for(Race r : this.races) {
            if (r.getName().toLowerCase().equals(name.toLowerCase())) {
                r.setSelected(true);
                break;
            }
        }
    }
    public void setRace(long id) {
        reset("Race");
        this.races[(int)id].setSelected(true);
    }
    public void setClass(String name) {
        reset("Class");
        for(Token t : this.classes) {
            if (t.getName().equals(name)) {
                t.setSelected(true);
                break;
            }
        }
    }
    public void setClass(long id) {
        reset("Class");
        this.classes[(int)id].setSelected(true);
    }

    /* GETTER */
    public int getRaceBonus(int position) {
        return this.races[getIndexAtTrue("Race")].getBonus(position);
    }
    public String getBonusAbility(int position) {
        return this.races[getIndexAtTrue("Race")].getBonusAbility(position);
    }
    public int getIndexAtTrue(String entity) {
        int output = 0;
        switch (entity.toLowerCase()) {
            case "race": {
                for (Race r : this.races) {
                    if (r.isSelected()) { break; }
                    else { output++; }
                }
                break;
            }
            case "class": {
                for (Token t : this.classes) {
                    if (t.isSelected()) { break; }
                    else { output++; }
                }
                break;
            }
        }
        return output;
    }

    public String toString(String entity) {
        String output = "";
        switch (entity.toLowerCase()) {
            case "race": {
                for (Race r : this.races)
                    if (r.isSelected())
                        output = r.getName();
                break;
            }
            case "class": {
                for (Token t : this.classes)
                    if (t.isSelected())
                        output = t.getName();
                break;
            }
        }
        return output;
    }
}