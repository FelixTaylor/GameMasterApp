package felixtaylor.gamemaster.figure;
import java.util.Random;

public class Hitpoints {
    public static int calculate(String charClass, int level, int dexMod) {
        Random random = new Random();
        int hitPoints = 0;
        int modifier;

        switch (charClass) {
            case "Mage": case "Warlock": {
                modifier = 4;
                break;
            }
            case "Bard": case "Rogue": {
                modifier = 6;
                break;
            }
            case "Druid": case "Cleric": case "Monk": case "Range": {
                modifier = 8;
                break;
            }
            case "Fighter": case "Paladin": {
                modifier = 10;
                break;
            }
            default: {
                modifier = 12;
            }
        }

        hitPoints += modifier + dexMod;
        for (short i=2; i<=level; i++) {
            hitPoints += random.nextInt(modifier) + 1;
            hitPoints += dexMod;
        }

        return hitPoints<1 ? 1 : hitPoints;
    }
}