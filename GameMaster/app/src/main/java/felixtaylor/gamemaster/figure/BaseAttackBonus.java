package felixtaylor.gamemaster.figure;
import android.util.Log;
import java.io.Serializable;

class BaseAttackBonus implements Serializable {

    private static final long serialVersionUID = 29L;
    private static String LOGTAG = BaseAttackBonus.class.getSimpleName();

    private int attacks;
    private int baseAttackBonus;
    private int maxMelee;
    private int maxRanged;

    public BaseAttackBonus() {
        this.baseAttackBonus = 1;
        this.maxMelee = 1;
        this.maxRanged = 1;
    }
    public BaseAttackBonus(String figureClass, int level, int dexMod, int strMod) {
        this();
        Log.d(LOGTAG, "Input parameter: " + level + ", " + dexMod + ", " + strMod);
        setBaseAttack(figureClass, level, dexMod, strMod);
    }

    private void setBaseAttack(String figureClass, int level, int dexMod, int strMod) {
        setAttacks(figureClass, level);
        setBaseAttackBonus(figureClass, level);

        setMaxMelee(strMod);
        setMaxRanged(dexMod);
    }

    private void setAttacks(String figureClass, int level) {
        int interval;
        switch (figureClass.toLowerCase()) {
            case "barbarian":
            case "fighter":
            case "paladin":
            case "ranger":

                interval = 5;
                break;

            case "bard":
            case "cleric":
            case "druid":
            case "monk":
            case "rogue":

                interval = 7;
                break;

            default:

                interval = 11;
                break;
        }

        this.attacks = calculateAttacks(level, interval);
        //Log.d(LOGTAG, "Attacks = " + getAttacks());

    }
    private void setBaseAttackBonus(String figureClass, int level) {
        this.baseAttackBonus = calculateBaseAttackBonus(figureClass, level);
        //Log.d(LOGTAG, "Bab set to " + getBaseAttackBonus());
    }
    private void setMaxMelee(int strMod) {
        this.maxMelee = getBaseAttackBonus() + strMod;
        //Log.d(LOGTAG, "Set maxMelee to " + getMaxMelee());
    }
    private void setMaxRanged(int dexMod) {
        this.maxRanged = getBaseAttackBonus() + dexMod;
        //Log.d(LOGTAG, "Set maxRange to " + getMaxRanged());
    }

    private int calculateAttacks(int level, int interval) {
        int temp  = 0;
        int value = 0;

        for (int i=0; i<=level; i++) {

            temp++;
            if (temp == interval) {
                temp = 0;
                value++;
            }
        }

        if (value < 1) {
            value = 1;
        }

        return value;
    }
    private int calculateBaseAttackBonus (String figureClass, int level) {
        int bab = 0;
        switch (figureClass.toLowerCase()) {
            case "barbarian":
            case "fighter":
            case "paladin":
            case "ranger":

                bab = level;
                break;

            case "bard":
            case "cleric":
            case "druid":
            case "monk":
            case "rogue":

                for (int i=0; i<level; i++) {
                    if (i%4 != 0) {
                        bab ++;
                    }
                }

                break;

            default:

                for (int i=1; i<=level; i++) {
                    if (i%2 == 0) {
                        bab ++;
                    }
                }

                break;
        }
        return bab;
    }

    public int getBaseAttackBonus() {
        return this.baseAttackBonus;
    }
    public int getAttacks() {
        return this.attacks;
    }
    public int getAttackModifier() {
        return -5;
    }
    public int getMaxMelee() {
        return this.maxMelee;
    }
    public int getMaxRanged() {
        return this.maxRanged;
    }

    public String toString() {
        return new StringBuffer()
                .append("BAB: [")
                .append(getAttacks())
                .append("] [")
                .append(getMaxMelee())
                .append(",")
                .append(getMaxRanged())
                .append("] [")
                .append(getAttackModifier())
                .append("]")
                .toString();
    }
}