package felixtaylor.gamemaster.figure;
import java.util.Random;

public class Name {
	private static Random random = new Random();
	public static String random(String race, boolean isFemale) {
		switch (race) {
			case "Dwarf":
				return dwarf(isFemale);
			case "Elf":
				return elf(isFemale);
			case "Gnome":
				return gnome(isFemale);
			case "Halfelf":
				return halfelf(isFemale);
			case "Halfling":
				return halfling(isFemale);
			case "Halfork":
				return halfork(isFemale);
			default:
				return human(isFemale);
		}
	}

	private static String dwarf(boolean isFemale) {
		if (isFemale) {
			String[] femaleName = {"Bimpnotti", "Caramip", "Duvamil", "Ellywick", "Ellyjobell", "Tupfschleife", "Mardnab", "Roywyn", "Shamil", "Wegwocket"};
			return femaleName[random.nextInt(femaleName.length)];

		} else {
			String[] maleName = {"Boddynock", "Dimbel", "Fonkin", "Glim", "Gerbo", "Jebeddo", "Namfudel", "Rundar", "Siebo", "Zuck"};
			return maleName[random.nextInt(maleName.length)];
		}
	}
    private static String elf(boolean isFemale) {
		String[] surname = {"Amastacia", "Amakiir", "Galanodel", "Holimion", "Liadon", "Meliamne", "Nailo", "Siannodel", "Ilphukiir", "Xiloszient"};

		if (isFemale) {
			String[] femaleName = {"Anastrianna", "Antinua", "Drusilia", "Felosial", "Ielelnia", "Lia", "Qillathe", "Silaqui", "Valathe", "Xanaphia"};
			return femaleName[random.nextInt(femaleName.length)] + " " + surname[random.nextInt(surname.length)];

		} else {
			String[] maleName = {"Aramil", "Aust", "Enialis", "Heian", "Himo", "Ivellios", "Laucian", "Quarion", "Thamior", "Thraviol"};
			return maleName[random.nextInt(maleName.length)] + " " + surname[random.nextInt(surname.length)];
		}
	}
	private static String gnome(boolean isFemale) {
		if (isFemale) {
			String[] femaleName = {"Bimpnotti", "Caramip", "Duvamil", "Ellywick", "Ellyjobell", "Tupfschleife", "Mardnab", "Roywyn", "Shamil", "Wegwocket"};
			return femaleName[random.nextInt(femaleName.length)];

		} else {
			String[] maleName = {"Boddynock", "Dimbel", "Fonkin", "Glim", "Gerbo", "Jebeddo", "Namfudel", "Rundar", "Siebo", "Zuck"};
			return maleName[random.nextInt(maleName.length)];
		}
	}
	private static String halfelf(boolean isFemale) {
		if (isFemale) {
			String[] femaleName = {"Olvyre", "Iropisys", "Unaseris", "Howaris", "Jilviel", "Loradove", "Ileaerys", "Armythe", "Qizira", "Quetheris"};
			return femaleName[random.nextInt(femaleName.length)];

		} else {
			String[] maleName = {"Rilumin", "Cravoril", "Quogretor", "Kevnan", "Orisariph", "Zanhomin", "Zanhomin", "Wilynor", "Halcoril", "Davreak"};
			return maleName[random.nextInt(maleName.length)];
		}
	}
	private static String halfling(boolean isFemale) {
		if (isFemale) {
			String[] femaleName = {"Amaryllis", "Charmine", "Cora", "Euphemia", "Jilian", "Lavinia", "Merla", "Portia", "Seraphina", "Verna"};
			return femaleName[random.nextInt(femaleName.length)];

		} else {
			String[] maleName = {"Alton", "Beau", "Kayd", "Eldon", "Garret", "Leyl", "Milo", "Osborn", "Rosco", "Wellby"};
			return maleName[random.nextInt(maleName.length)];
		}
	}
	private static String halfork(boolean isFemale) {
		if (isFemale) {
			String[] femaleName = {"Baggi", "Emen", "Engong", "Myev", "Neega", "Ovak", "Onka", "Schautha", "Vola", "Volen"};
			return femaleName[random.nextInt(femaleName.length)];

		} else {
			String[] maleName = {"Dentsch", "Feng", "Gell", "Henk", "Holg", "Imsh", "Keth", "Ront", "Shump", "Thokk"};
			return maleName[random.nextInt(maleName.length)];
		}
	}
	private static String human(boolean isFemale) {
		String[] surname = {"Alfaran", "Darben", "Bierwirth", "Termoil", "Bodiak", "Perensen", "Arsteener", "Zandor"};

		if (isFemale) {
			String[] femaleName = {"Allia", "Celien", "Sabthatrida", "Vertia", "Linlia", "Ilwyn", "Lintha"};
			return femaleName[random.nextInt(femaleName.length)] + " " + surname[random.nextInt(surname.length)];

		} else {
			String[] maleName = {"Erich", "Ron", "Nius", "Xanbert", "Kucas", "Linus", "Alco", "Torbrecht", "Sigman", "Norron"};
			return maleName[random.nextInt(maleName.length)] + " " + surname[random.nextInt(surname.length)];
		}

	}
}