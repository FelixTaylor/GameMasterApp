package felixtaylor.gamemaster.figure;
import java.io.Serializable;

public class Skill implements Serializable {
    private static final long serialVersionUID = 1L;
    private long skillId, figureId;
    private int rank, keyAbility;

    private Skill() {
        this.skillId  = -1;
        this.figureId = -1;
        this.rank     = 0;
    }
    public Skill(long sid, int rank) {
        this();
        setSkillId(sid);
        setRank(rank);

    }
    public Skill(long sid, long fid, int rank, int keyAbility) {
        this(sid,rank);
        setFigureId(fid);
        setKeyAbility(keyAbility);
    }

    /*public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }*/

    public void setSkillId(long skillId) {
        this.skillId = skillId;
    }
    public long getSkillId() {
        return skillId;
    }

    public void setFigureId(long figureId) {
        this.figureId = figureId;
    }
    public long getFigureId() {
        return figureId;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
    public int getRank() {
        return rank;
    }

    public void setKeyAbility(int keyAbility) {
        this.keyAbility = keyAbility;
    }
    public int getKeyAbility() {
        return keyAbility;
    }

    @Override public String toString() {
        return this.getClass().getSimpleName() + " [sid=" + getSkillId() + ", fid=" + getFigureId() + ", rank=" +getRank() + "]";
    }

}
