package felixtaylor.gamemaster.figure;
import java.util.Random;

public class Level {
	public static int random() {
		return new Random().nextInt(30)+1;
	}
	public static int randomInRange(int[] range) {
		Random random = new Random();
		int level;
		do {
			level = random.nextInt(range[1] + 1);
		} while (level<range[0] || level>range[1]);
		return level;
	}
}