package felixtaylor.gamemaster.figure.misc;
import java.io.Serializable;

public class Token implements Serializable {
    private static final long serialVersionUID = 24L;
    private boolean selected;
    private String name;

    public Token() {
        this.selected = false;
        this.name = "TOKEN";
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSelected(boolean isSelected) {
        this.selected = isSelected;
    }
    public String getName() {
        return this.name;
    }
    public boolean isSelected() {
        return this.selected;
    }
}