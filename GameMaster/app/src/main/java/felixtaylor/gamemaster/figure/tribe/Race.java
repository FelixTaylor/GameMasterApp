package felixtaylor.gamemaster.figure.tribe;
import java.io.Serializable;
import felixtaylor.gamemaster.figure.misc.Token;

class Race extends Token {
    private class Bonus implements Serializable {

        /*  A Bonus is defined by the name of
            the Ability which gets the bonus and
            a modifier value. */

        private static final long serialVersionUID = 21L;
        private String identifier;
        private int modifier;

        private Bonus() {
            setIdentifier("BONUS");
            setModifier(0);
        }
        private Bonus(String identifier, int value) {
            this();
            setIdentifier(identifier);
            setModifier(value);
        }

        /* SETTER */
        private void setIdentifier(String identifier) {
            if (!identifier.trim().isEmpty()) {
                this.identifier = identifier;
            }
        }
        private void setModifier(int value) {
            this.modifier = value;
        }

        /* GETTER */
        private String getIdentifier() {
            return this.identifier;
        }
        private Integer getModifier() {
            return this.modifier;
        }

        public String toString() {
            return getName() + ": " + getModifier();
        }
    }
    private Bonus[] boni;

    public Race() {
        this.boni = new Bonus[2];
    }
    public void setBonus(int id, String name, int modifier) {
        boni[id] = new Bonus(name.toUpperCase(), modifier);
    }
    public int getBonus(int position) {
        return this.boni[position].getModifier();
    }
    public String getBonusAbility(int position) {
        return this.boni[position].getIdentifier();
    }
}