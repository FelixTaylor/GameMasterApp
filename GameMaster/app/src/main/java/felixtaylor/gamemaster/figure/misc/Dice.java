package felixtaylor.gamemaster.figure.misc;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
public class Dice implements Serializable {
	private static final long serialVersionUID = 1L;

	/* The results contains all the
	 * results of the rolled dice's. */ 

	private ArrayList<Integer> results;
	
	/* The value of amount determines how
	 * many dice's will be rolled.
	 * 
	 * The value of range defines how
	 * many sites the dice has. At the 
	 * moment any value greater then 1
	 * is allowed */
	
	private int amount;
	private int range;
	
	private Random dice;

	public Dice() {
		
		/* The standard of this class is
		 * that there is one dice with a
		 * range of 6! */
		
		setAmount(1);
		setRange(6);
		
		this.results = new ArrayList<>();
		this.dice = new Random();
	}
	
	
	public Dice(int amount, int range) {
		this();
		setAmount(amount);
		setRange(range);
	}

	/* setAmount sets the amount of dice's.
	 * It has to be at least 1 dice! I build
	 * this method to ignore negative numbers.
	 * So if the input is -2 there will be (+)2
	 * dice's set */
	
	public void setAmount(int var) {
		if (var >= 1)
			this.amount = var;
		else {
			
			/* If the value of amount is invalid it
			 * will be set to 1. */
			
			this.amount = 1;
			System.err.println("Dice.setAmount: Changed amount to 1.");
		}
	}
	
	/* setRange is used to set the range for EACH dice.
	 * The minimum range of this dice has to be 2 (then
	 * it would be a 'true'/ 'false' dice). Until now
	 * there is no maximum range for the dice. */
	
	private void setRange(int var) {
		if (var >= 2)
			this.range = var;
		else 
			
			/* An error will be displayed if the range of the
			 * dice is smaller then 2. */
			
			System.err.println("Dice.setRange: var is < 2");
	}
	
	/* the diceSum method adds
	 * all the rolled dice to one
	 * value and returns it. */
	
	public int diceSum(){
		int sum = 0;
		for (int i=0; i<getResults().size(); i++)
			sum += getResultAtPosition(i);
		return sum;
	}

	/* GETTER
	 * 
	 * getArmor returns the value of
	 * the characters armor */
	
	public int getAmount() {
		return this.amount;
	}
	
	/* getRange returns how many
	 * dices are in use. */
	
	public int getRange() {
		return this.range;
	}
	
	/* getResults returns the
	 * current array with all
	 * values. */
	
	public ArrayList<Integer> getResults() {
		return this.results;
	}
	
	/* getResultAtPosition returns
	 * a specific value at 'i'
	 *  in the results. */
	
	private int getResultAtPosition(int index){
		return getResults().get(index);
	}
	
	public int size() {
		return this.results.size();
	}
	
	/* roleDice is used to actual
	 * roll the created dice. */

	public void roleDice() {
		for (int i=0; i<getAmount(); i++)
			this.results.add(this.dice.nextInt(getRange())+1);
	}
	
	/* sortDice sorts the values
	 * in the results. */
	
	public void sortDice() {
		 Collections.sort(this.results);
	}
	
	/* eraseSmallestDice will first
	 * sort the array and then changes
	 * the smallest value to 0. */
	
	public void eraseSmallestDice() {
		sortDice();
		this.results.set(0, 0);
	}
	
	public void clear() {
		this.results.clear();
	}

	@Override
	public String toString(){
		return "[" + getAmount() + "D" + getRange() + "]";
	}
}