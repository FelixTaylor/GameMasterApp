package felixtaylor.gamemaster.figure;
import java.io.Serializable;
import java.util.Arrays;

import felixtaylor.gamemaster.figure.misc.Dice;

public class Abilities implements Serializable {
	private static final long serialVersionUID = 15L;
    private class Ability implements Serializable {
        private static final long serialVersionUID = 1L;

        /*  An Ability is defined by a name, a value
            and a modifier. A Figure has the abilities:
            STR, DEX, CON, INT, WIS, CHA */

        private String name;
        private int value;
        private int mod;

        /*  The ability constructor initializes a
            single ability by giving it a name and
            a value. */

        private Ability() {
            this.name = "TESTNAME";
            this.value = 9999;
        }
        private Ability(String name, int value) {
            this();
            setName(name);
            setValue(value);
        }

        /*  The calculateMod method calculates the
            value of the modifier of the ability */

        private void calculateMod() {
            int startValue;
            int mod;

            if (getValue()<10) {
                mod = -5;
                startValue = 0;
            } else {
                startValue = 10;
                mod = 0;
            }

            for (int i=startValue; i<=getValue(); i++) {
                if (i % 2 == 0)
                    mod++;
            }

            setMod(mod-1);
        }

        /* SETTER */
        public void setName(String name) {
            this.name = name;
        }
        public void setValue(int value) {
            this.value = value;
        }
        private void setMod(int value) {
            this.mod = value;
        }

        /* GETTER */
        public String getName() {
            return this.name;
        }
        public int getValue() {
            return this.value;
        }
        private int getMod() {
            return this.mod;
        }
    }

    /*  The abilities class contains all seven main
        abilities: STR, DEX, CON, INT, WIS, CHA.
        These abilities are very important for the
        figure creation. */

    private Ability[] abilities;

    /*  The dice set is used to calculate the
        value of a single Ability. For each
        Ability the dices et roles 4 dices with
        a range of 6. Then the smallest dice is
        being erased and the remaining 3 dices
        are added to the final sum. */

	private Dice diceSet;

	/*  The abilities constructor initializes
	    the array with a name and the value 0;
	 */
	
	public Abilities() {
        String[] abilityName = {"STR", "DEX", "CON", "INT", "WIS", "CHA"};
        this.abilities = new Ability[abilityName.length];
        this.diceSet   = new Dice(4, 6);

        for (int i=0; i<abilityName.length; i++)
            this.abilities[i] = new Ability(abilityName[i], 0);
	}

	/*  The method distribute decides by
        reference to the charClass value how
	    important each ability for the character is.

	    e.g. It is for a fighter more important to
	    be strong as for a mage. */

    public void distribute(String charClass) {
        int[] values = new int[this.abilities.length];
        for (int i=0; i<this.abilities.length; i++)
            values[i] = getAbility(i);

        Arrays.sort(values);

        switch(charClass) {
            case "Barbarian":
                set("DEX", values[5]);
                set("STR", values[4]);
                set("CON", values[3]);
                set("INT", values[2]);
                set("WIS", values[1]);
                set("CHA", values[0]);
                break;
            case "Bard":
                set("CHA", values[5]);
                set("CON", values[4]);
                set("DEX", values[3]);
                set("INT", values[2]);
                set("WIS", values[1]);
                set("STR", values[0]);
                break;
            case "Cleric":
                set("WIS", values[5]);
                set("CON", values[4]);
                set("STR", values[3]);
                set("DEX", values[2]);
                set("INT", values[1]);
                set("CHA", values[0]);
                break;
            case "Druid":
                set("WIS", values[5]);
                set("DEX", values[4]);
                set("CON", values[3]);
                set("STR", values[2]);
                set("INT", values[1]);
                set("CHA", values[0]);
                break;
            case "Fighter":
                set("STR", values[5]);
                set("CON", values[4]);
                set("DEX", values[3]);
                set("INT", values[2]);
                set("WIS", values[1]);
                set("CHA", values[0]);
                break;
            case "Mage":
                set("INT", values[5]);
                set("CON", values[4]);
                set("WIS", values[3]);
                set("DEX", values[2]);
                set("CHA", values[1]);
                set("STR", values[0]);
                break;
            case "Monk":
                set("WIS", values[5]);
                set("DEX", values[4]);
                set("STR", values[3]);
                set("INT", values[2]);
                set("CON", values[1]);
                set("CHA", values[0]);
                break;
            case "Paladin":
                set("CHA", values[5]);
                set("WIS", values[4]);
                set("STR", values[3]);
                set("CON", values[2]);
                set("DEX", values[1]);
                set("INT", values[0]);
                break;
            case "Ranger":
                set("DEX", values[5]);
                set("WIS", values[4]);
                set("STR", values[3]);
                set("CON", values[2]);
                set("INT", values[1]);
                set("CHA", values[0]);
                break;
            case "Rogue":
                set("DEX", values[5]);
                set("INT", values[4]);
                set("CON", values[3]);
                set("WIS", values[2]);
                set("STR", values[1]);
                set("CHA", values[0]);
                break;
            case "Warlock":
                set("CHA", values[5]);
                set("CON", values[4]);
                set("INT", values[3]);
                set("WIS", values[2]);
                set("DEX", values[1]);
                set("STR", values[0]);
                break;
        }
    }
	public void calculateModifier(int index) {
        if (index <= 6 && index >= 0) {
            this.abilities[index].calculateMod();
        } else {
            System.err.println("Abilities.calculateModifier: Index out of range!");
        }
    }
    public void addPoints(String charClass, int level) {
        boolean isFirst = true;

        for (int i=1; i<=level; i++) {
            if (i%4 == 0) {
                if (isFirst) {

                    switch(charClass) {
                        case "Fighter":
                            set("STR", getAbility("STR") + 1);
                            break;

                        case "Barbarian": case "Ranger":case "Rogue":
                            set("DEX", getAbility("DEX") + 1);
                            break;

                        case "Bard": case "Paladin": case "Warlock":
                            set("CHA", getAbility("CHA") + 1);
                            break;

                        case "Mage": set("INT", getAbility("INT") + 1); break;

                        case "Cleric": case "Druid": case "Monk":
                            set("WIS", getAbility("WIS") + 1);
                            break;

                    }

                    isFirst = false;
                } else {

                    switch(charClass) {
                        case "Barbarian": set("STR", getAbility("STR") + 1); break;

                        case "Druid": case "Monk":
                            set("DEX", getAbility("DEX") + 1);
                            break;

                        case "Bard": case "Cleric": case "Fighter": case "Mage":  case "Warlock":
                            set("CON", getAbility("CON") + 1);
                            break;

                        case "Rogue": set("INT", getAbility("INT") + 1); break;

                        case "Paladin": case "Ranger":
                            set("WIS", getAbility("WIS") + 1);
                            break;

                    }

                    isFirst = true;
                }
            }

        }
    }
    public void removePoints(String charClass, int level) {
        boolean isFirst = true;
        for (int i=1; i<=level; i++) {

            if (i % 4 == 0) {
                if (isFirst) {

                    switch(charClass) {
                        case "Fighter":
                            set("STR", getAbility("STR") - 1);
                            break;

                        case "Barbarian": case "Ranger":case "Rogue":
                            set("DEX", getAbility("DEX") - 1);
                            break;

                        case "Bard": case "Paladin": case "Warlock":
                            set("CHA", getAbility("CHA") - 1);
                            break;

                        case "Mage": set("INT", getAbility("INT") - 1);
                            break;

                        case "Cleric": case "Druid": case "Monk":
                            set("WIS", getAbility("WIS") - 1);
                            break;

                    }

                    isFirst = false;
                } else {

                    switch(charClass) {
                        case "Barbarian": set("STR", getAbility("STR") - 1);
                            break;

                        case "Druid": case "Monk":
                            set("DEX", getAbility("DEX") - 1);
                            break;

                        case "Bard": case "Cleric": case "Fighter": case "Mage":  case "Warlock":
                            set("CON", getAbility("CON") - 1);
                            break;

                        case "Rogue": set("INT", getAbility("INT") - 1);
                            break;

                        case "Paladin": case "Ranger":
                            set("WIS", getAbility("WIS") - 1);
                            break;

                    }

                    isFirst = true;
                }
            }


        }
    }

    /* SETTER */

	public void set(String name, int value) {
        if (value >= 0)
            for (Ability a : this.abilities)
                if (a.getName().equals(name)) {
                    a.setValue(value);
                    a.calculateMod();
                }
    }
    public void set(int index, int value) {
        this.abilities[index].setValue(value);
    }

    /*  The random is used to
	    set all of the six abilities to a
	    random value. */

	public void random() {
        for (Ability a : this.abilities) {
            this.diceSet.roleDice();
            this.diceSet.eraseSmallestDice();
            a.setValue(this.diceSet.diceSum());
            this.diceSet.clear();
        }
	}
	
	/* GETTER */
	
	/* the method getAbility returns
	 * a single ability positioned in
	 * the abilities array at "position". */
	
	public int getAbility(int index) {
       /* int output = 0;
        if (index <= 6 && index >= 0) {
            output = this.abilities[index].getValue();
        } else {
            System.err.println("Abilities.getAbility: Index out of range!");
        }
		return output;*/

        return this.abilities[index].getValue();
	}
	public int getAbility(String name) {
        int output = 0;
        if (name != null && !name.equals("")) {
            for (Ability a : this.abilities) {
                if (a.getName().equals(name)) {
                    output = a.getValue();
                }
            }
        }
        return output;
    }

	/*  The getModifier Method returns the
	    modifier value of an ability. */

	public int getModifier(String name) {
        int output = 0;
        if (name != null && !name.equals("")) {
            for (Ability a : this.abilities) {
                if (a.getName().equals(name)) {
                    output = a.getMod();
                    break;
                }
            }
        }
        return output;
    }
    public int getModifier(int index) {
        int output = 0;
        if (index >= 0 && index <=6) {
            output = this.abilities[index].getMod();
        }
        return output;
    }

    /* TO STRING */
    public String toString(int position) {
		return Integer.toString(getAbility(position));
	}
	public String toString() {
        StringBuffer output = new StringBuffer();
        for (int i=0; i<this.abilities.length; i++) {
            output.append(getAbility(i));
            output.append(" ");
        }
        return output.toString();
    }
}