package felixtaylor.gamemaster.figure;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

// TODO: I dont like the AverageFigure class.
/* In future i want to only use sqlite statements. */

public class AverageFigure {
    private ArrayList<Figure> allFigures;
    private int values[];


    private AverageFigure() {
        this.allFigures = new ArrayList<>();
    }
    public AverageFigure(ArrayList<Figure> figures) {
        this();
        this.allFigures = figures;
        this.values = new int[this.allFigures.size()];
    }
    private double getAverage(int[] values) {
        double average = 0;

        for (int v : values) {
            average += v;
        }
        average /= values.length;

        return average;
    }

    public double getLevel() {

        for (int i=0; i<this.allFigures.size(); i++) {
            this.values[i] = this.allFigures.get(i).getLevel();
        }

        return getAverage(values);
    }
    public double getAbility(int position) {

        for (int i=0; i<this.allFigures.size(); i++) {
            this.values[i] = this.allFigures.get(i).getAbility(position);
        }

        return getAverage(values);
    }
    public double getHitpoints() {

        for (int i=0; i<this.allFigures.size(); i++) {
            this.values[i] = this.allFigures.get(i).getHitPoints();
        }

        return getAverage(values);
    }
    public double getArmor() {

        for (int i=0; i<this.allFigures.size(); i++) {
            this.values[i] = this.allFigures.get(i).getArmor();
        }

        return getAverage(values);
    }
    public double getSkillpoints() {

        for (int i=0; i<this.allFigures.size(); i++) {
            this.values[i] = this.allFigures.get(i).getSkillPoints();
        }

        return getAverage(values);
    }
    public double getTalentpoints() {

        for (int i=0; i<this.allFigures.size(); i++) {
            this.values[i] = this.allFigures.get(i).getTalentPoints();
        }

        return getAverage(values);
    }
    public double getSavingThrow(int position) {

        for (int i=0; i<this.allFigures.size(); i++) {
            this.values[i] = this.allFigures.get(i).getSavingThrowAt(position);
        }

        return getAverage(values);
    }
    public double getGold() {

        for (int i=0; i<this.allFigures.size(); i++) {
            this.values[i] = this.allFigures.get(i).getGold();
        }

        return getAverage(values);
    }
    public int getMaxAbility() {

        Integer[] abilities = new Integer[6];
        for (int i=0; i<abilities.length; i++) {
            abilities[i] = (int) getAbility(i);
        }

        Arrays.sort(abilities, Collections.reverseOrder());
        return abilities[0];

    }

    public String getRace() {
       HashMap map = new HashMap();

       map.put("dwarf", 0);
       map.put("elf", 0);
       map.put("gnome", 0);
       map.put("halfelf", 0);
       map.put("halfork", 0);
       map.put("human", 0);

       for (int i=0; i<allFigures.size(); i++) {
           switch (allFigures.get(i).getRace().toLowerCase()) {

               case "dwarf" :   map.put("dwarf",   (int) map.get("dwarf") + 1);   break;
               case "elf" :     map.put("elf",     (int) map.get("elf") + 1);     break;
               case "gnome" :   map.put("gnome",   (int) map.get("gnome") + 1);   break;
               case "halfelf" : map.put("halfelf", (int) map.get("halfelf") + 1); break;
               case "halfork" : map.put("halfork", (int) map.get("halfork") + 1); break;
               case "human":    map.put("human",   (int) map.get("human") + 1);   break;
           }

       }

       int maxVal = 0;
       String name = "";
       Iterator i = map.entrySet().iterator();

       while(i.hasNext()) {
           Map.Entry entry = (Map.Entry)i.next();
           if ((int) entry.getValue() >= maxVal) {
               maxVal = (int) entry.getValue();
               name = (String) entry.getKey();
           }
       }

        return name;
    }
    public String getFigureClass() {
        HashMap map = new HashMap();

        map.put("barbarian", 0);
        map.put("bard", 0);
        map.put("cleric", 0);
        map.put("druid", 0);
        map.put("fighter", 0);
        map.put("mage", 0);
        map.put("monk", 0);
        map.put("paladin", 0);
        map.put("ranger", 0);
        map.put("rogue", 0);
        map.put("warlock", 0);

        for (int i=0; i<allFigures.size(); i++) {
            switch (allFigures.get(i).getFigureClass().toLowerCase()) {

                case "barbarian" : map.put("barbarian",   (int) map.get("barbarian") + 1);   break;
                case "bard" :    map.put("bard",     (int) map.get("bard") + 1);     break;
                case "cleric" :  map.put("cleric",   (int) map.get("cleric") + 1);   break;
                case "druid" :   map.put("druid", (int) map.get("druid") + 1); break;
                case "fighter" : map.put("fighter", (int) map.get("fighter") + 1); break;
                case "mage" :    map.put("mage", (int) map.get("mage") + 1); break;
                case "monk" :    map.put("monk", (int) map.get("monk") + 1); break;
                case "paladin" : map.put("paladin", (int) map.get("paladin") + 1); break;
                case "ranger" :  map.put("ranger", (int) map.get("ranger") + 1); break;
                case "rogue" :   map.put("rogue", (int) map.get("rogue") + 1); break;
                case "warlock":  map.put("warlock",   (int) map.get("warlock") + 1);   break;
            }

        }

        int maxVal = 0;
        String name = "";
        Iterator i = map.entrySet().iterator();

        while(i.hasNext()) {
            Map.Entry entry = (Map.Entry)i.next();
            if ((int) entry.getValue() >= maxVal) {
                maxVal = (int) entry.getValue();
                name = (String) entry.getKey();
            }
        }

        return name;
    }
    public double[] getBaseAttackBonus() {
        double[] bab = new double[4];

        for (Figure figure : this.allFigures) {
            bab[0] += figure.getMaxMelee();
            bab[1] += figure.getMaxRanged();
            bab[2] += figure.getAttacks();
            bab[3] += figure.getBaseAttackBonus();
        }

        for (int i=0; i<bab.length; i++) {
            bab[i] /= this.allFigures.size();
        }

        return bab;
    }

}
