package felixtaylor.gamemaster.equipment;
import java.io.Serializable;
import java.util.ArrayList;
import felixtaylor.gamemaster.figure.misc.Dice;

public class Equipment implements Serializable {
    private static final long serialVersionUID = 23L;

    private ArrayList<Item> backpack;
    private int armor, gold;

    public Equipment() {
        this.backpack = new ArrayList<>();
        this.gold     = 0;
        this.armor    = 0;
    }
    public void calculateArmor(int modifier) {
        this.armor = 10 + modifier;
    }
    public void calculateGold(int charClass) {
        int times = 0;
        switch(charClass){
            case 1: times = 40;  break; // Barbarian
            case 2: times = 40;  break; // Bard
            case 3: times = 50;  break; // Cleric
            case 4: times = 20;  break; // Druid
            case 5: times = 60;  break; // Fighter
            case 6: times = 30;  break; // Mage
            case 7: times = 5; 	 break; // Monk
            case 8: times = 60;  break; // Paladin
            case 9: times = 60;  break; // Ranger
            case 10: times = 50; break; // Rogue
            case 11: times = 30; break; // Sorcerer
        }

        /* Creating the diceSet class */
        Dice diceSet = new Dice(times, 4);
        diceSet.roleDice();
        setGold(diceSet.diceSum());
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }
    public void setGold(int gold) {
        this.gold = gold;
    }
    public void addItem(Item item) {
        this.backpack.add(item);
    }
    public void updateItem(Item item) {
        for (int i=0; i<this.backpack.size(); i++) {
            if (this.backpack.get(i).getId() == item.getId()) {
                this.backpack.remove(i);
                this.backpack.add(i,item);
                break;
            }
        }
    }

    /* GETTER */
    public int getArmor() {
        return this.armor;
    }
    public int getGold() {
        return this.gold;
    }

    public void removeItem(long id) {
        for (int i=0; i<this.backpack.size(); i++) {
            if (this.backpack.get(i).getId() == id) {
                this.backpack.remove(i);
                break;
            }
        }
    }
    public ArrayList<Item> getItems() {
        return this.backpack;
    }
    public Item getItem(long id) {
        Item item = new Item();
        for (int i=0; i<this.backpack.size(); i++) {
            if (this.backpack.get(i).getId() == id) {
                item = this.backpack.get(i);
                break;
            }
        }
        return item;
    }



}
