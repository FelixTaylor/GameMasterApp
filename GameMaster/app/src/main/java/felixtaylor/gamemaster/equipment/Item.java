package felixtaylor.gamemaster.equipment;
import felixtaylor.gamemaster.system.Entity;

public class Item extends Entity {
    private static final long serialVersionUID = 1L;
    private double value, weight;
    private int quantity;

    protected Item() {
        setId(-1);
        setName("ITEM");
        this.quantity = -1;
        this.value    = 0.0;
        this.weight   = 0.0;
    }
    public Item(String name, int quantity, double value, double weight) {
        this();
        setName(name);
        setQuantity(quantity);
        setValue(value);
        setWeight(weight);
    }
    public Item(long id, String name, int quantity, double value, double weight) {
        this(name, quantity, value, weight);
        setId(id);
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public void setValue(double value) {
        this.value = value;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getQuantity() {
        return this.quantity;
    }
    public double getValue() {
        return this.value;
    }
    public double getWeight() {
        return this.weight;
    }

    @Override public boolean equals(Object object) {
        if (object instanceof Item) {
            if (this == object) {
                return true;
            }
        }
        return false;
    }
}