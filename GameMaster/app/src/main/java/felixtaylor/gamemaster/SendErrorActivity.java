package felixtaylor.gamemaster;
import android.content.Context;
import org.acra.*;
import org.acra.BuildConfig;
import org.acra.annotation.*;

import static org.acra.ReportField.ANDROID_VERSION;
import static org.acra.ReportField.APP_VERSION_CODE;
import static org.acra.ReportField.APP_VERSION_NAME;
import static org.acra.ReportField.LOGCAT;
import static org.acra.ReportField.PHONE_MODEL;
import static org.acra.ReportField.STACK_TRACE;

@AcraMailSender(mailTo = StaticValues.SEND_TO_MAIL)
@AcraToast(resText= R.string.dialog_comment)
@AcraCore(
        buildConfigClass = BuildConfig.class,
        reportContent = {PHONE_MODEL, ANDROID_VERSION, APP_VERSION_CODE, APP_VERSION_NAME, LOGCAT, STACK_TRACE}
        )
public class SendErrorActivity extends android.app.Application {
    @Override protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        ACRA.init(this);
    }
}
