package felixtaylor.gamemaster.system;
import java.util.ArrayList;

public class Campaign {

    private long   id;
    private String name;
    private String description;
    private long   creationDate;

    //private ArrayList<Image> images;
    private ArrayList<Group> groups;


    public Campaign() {
        this.id = -1;
        this.name = "NEW CAMPAIGN";
        this.description = "";
        this.creationDate = -1;
        this.groups = new ArrayList<>();
    }

    public Campaign(long id, String name, String description) {
        this();
        setId(id);
        setName(name);
        setDescription(description);
        setCreationDate(System.currentTimeMillis());
    }

    public void addGroup(Group group) {
        this.groups.add(group);
    }
    public void deleteGroup(long id) {
        for (int i=0; i<this.groups.size(); i++) {
            if (this.groups.get(i).getId() == id) {
                this.groups.remove(i);
                break;
            }
        }
    }

    public void setId(long id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    public long getId() {
        return this.id;
    }
    public String getName() {
        return this.name;
    }
    public String getDescription() {
        return this.description;
    }
    public long getCreationDate() {
        return this.creationDate;
    }

    public String toString() {
        return "Campaign: '" + getName() + "', id=" + getId() + ", " + getCreationDate();
    }
}
