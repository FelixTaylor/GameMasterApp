package felixtaylor.gamemaster.system;
@SuppressWarnings("serial")

public abstract class Container extends Entity {
    private String description;
    private long creationDateInMillis;

    @Override public abstract boolean equals(Object object);

    // SETTER
    public void setCreationDate(long millis) {
        this.creationDateInMillis = millis;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    // GETTER
    public long getCreationDateInMillis() {
        return creationDateInMillis;
    }
    public String getDescription() {
        return description;
    }
}