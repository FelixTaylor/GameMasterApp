package felixtaylor.gamemaster.system;
import android.util.Log;
import felixtaylor.gamemaster.database.DBConstants;

public class User extends Entity {
    private static final String LOGTAG = "User";
    private static final long serialVersionUID = 1L;

    private String language;
    private int showUserNamePopUp;
    private String orderBy;

    private int totalCreatedFigures;
    private int totalCreatedGroups;
    private long initialUseTime;

    private User() {
        Log.d(LOGTAG, "New User");
        setId(-1);
        setName("GameMaster");
        this.language = "en";
        this.showUserNamePopUp = 0;
        this.orderBy = "ORDER BY " + DBConstants.FIGURE_NAME + ", " + DBConstants.FIGURE_RACE + ", " + DBConstants.FIGURE_CLASS + ", " + DBConstants.FIGURE_LEVEL;

        this.totalCreatedFigures = 0;
        this.totalCreatedGroups = 0;
        this.initialUseTime = System.currentTimeMillis();
    }
    public User(long id,          String name,           String language,
                String statement, int showUserNamePopUp, int totalFigures,
                int totalGroups,  long initialUseTime) {

        this();
        Log.d(LOGTAG, "User(" + id + ", " + name + ") from DB.");
        setId(id);
        setName(name);
        setLanguage(language);
        setOrderBy(statement);
        setShowUserNamePopUp(showUserNamePopUp);
        setTotalCreatedFigures(totalFigures);
        setTotalCreatedGroups(totalGroups);
        setInitialUseTime(initialUseTime);
    }

    public void setLanguage(String language) {
        if (language != null && !language.trim().isEmpty()) {
            this.language = language;
        }
    }
    public void setOrderBy(String statement) {
        this.orderBy = statement;
    }
    public void setShowUserNamePopUp(int value) {
        this.showUserNamePopUp = value;
    }

    public void addTotalCreatedFigure() {
        Log.d(LOGTAG, "Add total figure.");
        this.totalCreatedFigures += 1;
    }
    private void setTotalCreatedFigures(int amount) {
        Log.d(LOGTAG, "Total figures=" + amount);
        this.totalCreatedFigures = amount;
    }
    public void addTotalCreateGroup() {
        Log.d(LOGTAG, "Add total group.");
        this.totalCreatedGroups += 1;
    }
    private void setTotalCreatedGroups(int amount) {
        Log.d(LOGTAG, "Total groups=" + amount);
        this.totalCreatedGroups = amount;
    }
    public void setInitialUseTime(long time) {
        Log.d(LOGTAG, "Time=" + time);
        this.initialUseTime = time;
    }

    public String getLanguage() {
        return this.language;
    }
    public String getOrderBy() {
        return this.orderBy;
    }
    public int getShowUserNamePopUp() {
        return this.showUserNamePopUp;
    }
    public int getTotalCreatedFigures() {
        return this.totalCreatedFigures;
    }
    public int getTotalCreatedGroups() {
        return this.totalCreatedGroups;
    }
    public long getInitialUseTime() {
        return this.initialUseTime;
    }

    @Override public boolean equals(Object object) {
        if (object instanceof User) {
            if (this == object) {
                return true;
            }
        }
        return false;
    }
}