package felixtaylor.gamemaster.system;
import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class Entity implements Serializable {
    private String name;
    private long id;

    @Override public abstract boolean equals(Object object);
    @Override public String toString() {
        return this.getClass().getSimpleName() + " [name=" + this.name + ", id=" + this.id + ", hash=" + this.hashCode()  + "]";
    }

    // Setter
    public void setId(long id) {
        this.id = id;
    }
    public void setName(String name) {
        if (!name.isEmpty()) {
            this.name = name;
        }
    }

    // Getter
    public long getId() {
        return this.id;
    }
    public String getName() {
        return this.name;
    }
}
