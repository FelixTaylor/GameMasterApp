package felixtaylor.gamemaster.system;

public class Group extends Container {
    private static final long serialVersionUID = 1L;

    private Group() {
        setId(-1);
        setName("DEFAULT");
        setDescription("");
        setCreationDate(0);
    }
    public Group(long dateInMillis, String name, String description) {
        this();
        setName(name);
        setCreationDate(dateInMillis);
        setDescription(description);
    }
    public Group(long id, long dateInMillis, String name, String description) {
        this(dateInMillis, name, description);
        setId(id);
    }
    @Override public boolean equals(Object object) {
        if (object instanceof Group) {
            if (this == object) {
                return this == object;
            }
        }
        return false;
    }
}
