package felixtaylor.gamemaster;
import felixtaylor.gamemaster.database.DBConstants;
public class StaticValues {

    /* The max amount of figures per group */
    public static final int MAX_FIGURES_IN_GROUP = 100;
    public static final int MAX_LEVEL = 30;
    public static final String SEND_TO_MAIL = "ddgmreport@gmail.com";

    /* Intent identifiers
     *
     * are used to pass the correct objects and values
     * to the next activity. */
    public static final String INTENT_FIGURE = "Figure";
    public static final String INTENT_GROUP = "Group";
    public static final String INTENT_USER  = "User";
    public static final String INTENT_LEVEL_MIN  = "LevelMin";
    public static final String INTENT_LEVEL_MAX  = "LevelMax";
    public static final String INTENT_AVAILABLE_RACES = "availableRaces";
    public static final String INTENT_AVAILABLE_CLASSES = "availableClasses";
    public static final String INTENT_FIGURE_AMOUNT = "FigureAmount";
    public static final String INTENT_FIGURES_OF_GROUP = "FiguresOfGroup";
    public static final String INTENT_IS_EXISTING = "ExistingFigure";
    public static final String INTENT_FIGURES_TO_MOVE = "FiguresToMove";
    public static final String INTENT_FIGURES_TO_DELETE = "FiguresToDelete";
    public static final String INTENT_NEW_GROUP_ID = "NewGroup";


    public static final String ORDER_BY_NAME =  "ORDER BY " + DBConstants.FIGURE_NAME + ", " + DBConstants.FIGURE_RACE + ", " + DBConstants.FIGURE_CLASS + ", " + DBConstants.FIGURE_LEVEL;;
    public static final String ORDER_BY_RACE =  "ORDER BY " + DBConstants.FIGURE_RACE + ", " + DBConstants.FIGURE_NAME + ", " + DBConstants.FIGURE_CLASS + ", " + DBConstants.FIGURE_LEVEL;
    public static final String ORDER_BY_CLASS = "ORDER BY " + DBConstants.FIGURE_CLASS + ", " + DBConstants.FIGURE_NAME + ", " + DBConstants.FIGURE_RACE + ", " + DBConstants.FIGURE_LEVEL;
    public static final String ORDER_BY_LEVEL = "ORDER BY " + DBConstants.FIGURE_LEVEL + ", " + DBConstants.FIGURE_NAME + ", " + DBConstants.FIGURE_RACE + ", " + DBConstants.FIGURE_CLASS;
    public static final String ORDER_BY_DATE =  "ORDER BY " + DBConstants.FIGURE_CREATION_DATE + ", " + DBConstants.FIGURE_NAME + ", " + DBConstants.FIGURE_RACE + ", " + DBConstants.FIGURE_CLASS + ", " + DBConstants.FIGURE_LEVEL;

    /* Language values */
    public static final String LANGUAGE_EN = "en";
    public static final String LANGUAGE_DE = "de";
    public static final String LANGUAGE_PL = "pl";
    public static final String LANGUAGE_ENGLISH = "English";
    public static final String LANGUAGE_GERMAN  = "Deutsch";
    public static final String LANGUAGE_POLISH  = "Polski";

    /* Date pattern
     *
     * is used to display ether the date only or the date
     * and the exact time on the group or detail activity. */
    public static final String DATE          = "yyyy/MM/dd";
    public static final String DATE_AND_TIME = DATE + ", HH:mm:ss";

}