package felixtaylor.gamemaster.actions;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import felixtaylor.gamemaster.R;
import felixtaylor.gamemaster.StaticValues;
import felixtaylor.gamemaster.activities.DetailActivity;
import felixtaylor.gamemaster.activities.ListActivity;
import felixtaylor.gamemaster.database.DBHandler;
import felixtaylor.gamemaster.equipment.Item;
import felixtaylor.gamemaster.figure.Figure;
import felixtaylor.gamemaster.system.Group;
import felixtaylor.gamemaster.system.User;

public class DeleteFigureActivity extends AppCompatActivity {
    private static String LOGTAG = DetailActivity.class.getSimpleName();
    private ArrayList<Item> items = new ArrayList<>();

    @Override protected void onResume() {
        super.onResume();
        Intent intent = getIntent();

        ArrayList<Figure> figuresToDelete = (ArrayList<Figure>) intent.getSerializableExtra(StaticValues.INTENT_FIGURES_TO_DELETE);
        Group group = (Group) intent.getSerializableExtra(StaticValues.INTENT_GROUP);
        User user   = (User) intent.getSerializableExtra(StaticValues.INTENT_USER);

        DBHandler handler = new DBHandler(getApplicationContext());
        handler.open();

        for (Figure figure : figuresToDelete) {
            if (!figure.isFavourite()) {

                Log.d(LOGTAG, "Deleted Figure: " + figure.toString());
                items = figure.getItems();
                handler.delete(figure);

            }
        }

        for (Item item : items) {
            Log.d(LOGTAG, "Deleted Item: " + item.toString());
            handler.delete(item);
        }

        handler.close();

        startActivity(new Intent(getApplicationContext(), ListActivity.class)
                .putExtra(StaticValues.INTENT_GROUP, group)
                .putExtra(StaticValues.INTENT_USER, user)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK) );

        Toast.makeText(getApplicationContext(),
                getResources().getQuantityString(
                        R.plurals.message_has_been_deleted,
                        figuresToDelete.size())
                , Toast.LENGTH_SHORT)
                .show();
    }

}