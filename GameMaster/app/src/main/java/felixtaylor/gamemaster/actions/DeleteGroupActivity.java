package felixtaylor.gamemaster.actions;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import felixtaylor.gamemaster.R;
import felixtaylor.gamemaster.StaticValues;
import felixtaylor.gamemaster.activities.DetailActivity;
import felixtaylor.gamemaster.activities.GroupActivity;
import felixtaylor.gamemaster.database.DBHandler;
import felixtaylor.gamemaster.figure.Figure;
import felixtaylor.gamemaster.system.Group;

public class DeleteGroupActivity extends AppCompatActivity {
    private static String LOGTAG = DetailActivity.class.getSimpleName();
    @Override protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        if (intent != null) {

            DBHandler handler = new DBHandler(getApplicationContext());
            Group group = (Group) intent.getSerializableExtra(StaticValues.INTENT_GROUP);
            ArrayList<Figure> figuresOfGroup = (ArrayList<Figure>) intent.getSerializableExtra(StaticValues.INTENT_FIGURES_OF_GROUP);
            Log.d(LOGTAG, "Delete group: " + group.getId() + ", " + group.getName());

            handler.open();
            for (Figure f : figuresOfGroup) {
                handler.delete(f);
            }

            handler.delete(group);
            handler.close();

            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.message_has_been_deleted, group.getName()),
                    Toast.LENGTH_SHORT).show();

            startActivity(new Intent(getApplicationContext(), GroupActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK)
            );
        }
    }
}