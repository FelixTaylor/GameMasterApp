package felixtaylor.gamemaster.actions;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;

import felixtaylor.gamemaster.R;
import felixtaylor.gamemaster.StaticValues;
import felixtaylor.gamemaster.activities.ListActivity;
import felixtaylor.gamemaster.database.DBConstants;
import felixtaylor.gamemaster.database.DBHandler;
import felixtaylor.gamemaster.figure.Figure;
import felixtaylor.gamemaster.system.Group;
import felixtaylor.gamemaster.system.User;

public class MoveFiguresToGroupActivity extends AppCompatActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent   = getIntent();
        Group group     = (Group) intent.getSerializableExtra(StaticValues.INTENT_GROUP);
        User user       = (User) intent.getSerializableExtra(StaticValues.INTENT_USER);
        long newGroupId = intent.getLongExtra(StaticValues.INTENT_NEW_GROUP_ID, -1);
        ArrayList<Figure> figuresToMove = (ArrayList<Figure>) intent.getSerializableExtra(StaticValues.INTENT_FIGURES_TO_MOVE);
        DBHandler handler = new DBHandler(this);
        handler.open();

        Group newGroup = handler.selectGroup(DBConstants.SELECT_GROUP_WITH_ID + newGroupId);
        //int figureCount = newGroup.getFigureCount(getApplicationContext());

        //if ((figuresToMove.size() +figureCount) <= 100) {

            for (int i=0; i<figuresToMove.size(); i++) {
                figuresToMove.get(i).setGroupId(newGroupId);
                handler.update(figuresToMove.get(i));
            }

            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.message_figure_moved),
                    Toast.LENGTH_SHORT).show();
/*
        } else {

            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.message_group_can_hold,
                            (100-figureCount)),
                    Toast.LENGTH_LONG).show();
        }
*/
        handler.close();
        startActivity(new Intent(getApplicationContext(), ListActivity.class)
                .putExtra(StaticValues.INTENT_GROUP, group)
                .putExtra(StaticValues.INTENT_USER, user)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK) );

    }
}
