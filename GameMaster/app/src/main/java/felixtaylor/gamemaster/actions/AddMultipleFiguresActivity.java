package felixtaylor.gamemaster.actions;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.ArrayList;

import felixtaylor.gamemaster.R;
import felixtaylor.gamemaster.StaticValues;
import felixtaylor.gamemaster.activities.ListActivity;
import felixtaylor.gamemaster.database.DBHandler;
import felixtaylor.gamemaster.figure.Figure;
import felixtaylor.gamemaster.system.Group;
import felixtaylor.gamemaster.system.User;

public class AddMultipleFiguresActivity extends AppCompatActivity {
    private DBHandler handler;
    private Group group;

    @Override protected void onResume() {
        super.onResume();

        Intent intent = getIntent();
        if (intent != null) {
            User user = (User) intent.getSerializableExtra(StaticValues.INTENT_USER);
            group = (Group) intent.getSerializableExtra(StaticValues.INTENT_GROUP);
            ArrayList<String> randomAvailableRaces   = (ArrayList<String>) intent.getSerializableExtra(StaticValues.INTENT_AVAILABLE_RACES);
            ArrayList<String> randomAvailableClasses = (ArrayList<String>) intent.getSerializableExtra(StaticValues.INTENT_AVAILABLE_CLASSES);
            int figureAmount = intent.getIntExtra(StaticValues.INTENT_FIGURE_AMOUNT, 0);
            int levelMin     = intent.getIntExtra(StaticValues.INTENT_LEVEL_MIN, 0);
            int levelMax     = intent.getIntExtra(StaticValues.INTENT_LEVEL_MAX, 0);

            handler = new DBHandler(getApplicationContext());

            if (groupCanHoldMoreFigures(figureAmount)) {
                Figure[] figures = new Figure[figureAmount];

                for (int i=0; i<figures.length; i++) {
                    figures[i] = new Figure();
                    figures[i].random(
                            new int[]{levelMin, levelMax},
                            randomAvailableRaces, randomAvailableClasses);

                    figures[i].setGroupId(group.getId());
                    figures[i].setOwner(user.getName());
                    user.addTotalCreatedFigure();
                }

                handler.open();
                for (Figure f : figures) {
                    handler.insert(f);
                }
                handler.update(user);
                handler.close();

            } else {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.message_too_many_figures),
                        Toast.LENGTH_SHORT).show();
            }

            startActivity(new Intent(getApplicationContext(), ListActivity.class)
                    .putExtra(StaticValues.INTENT_GROUP, group)
                    .putExtra(StaticValues.INTENT_USER, user)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
    public boolean groupCanHoldMoreFigures(int newFigures) {
        boolean groupCanHoldMoreFigures = true;

        handler.open();
        int totalFiguresInGroup =  handler.countFiguresInGroup(group.getId());
        handler.close();

        if ((totalFiguresInGroup + newFigures) > StaticValues.MAX_FIGURES_IN_GROUP) {
            groupCanHoldMoreFigures = false;
        }

        return groupCanHoldMoreFigures;
    }
}
